<?php

use Illuminate\Support\Facades\Route;
// Route::get('/', 'HomeController@dashboard')->name('admin.dashboard');

Route::get('/login', 'HomeController@login')->name('admin.login');
Route::post('/authenticate', 'HomeController@authenticate')->name('admin.authenticate');
Route::group(['middleware' => ['admin']], function () {
    Route::get('/', 'HomeController@dashboard')->name('admin.dashboard');
    Route::get('/changepass', 'HomeController@changepass')->name('admin.changepass');
    Route::get('/payment/pending', 'PaymentController@pending')->name('payment.pending');
    Route::get('/payment/paid-form/{id}', 'PaymentController@paid_form')->name('payment.paid.form');
    Route::post('/payment/paid-submit', 'PaymentController@paid_submit')->name('payment.paid.submit');
    Route::post('/payment/processing', 'PaymentController@processing')->name('payment.processing');
    Route::get('/payment/voucher-details/{id}', 'PaymentController@voucher_details')->name('payment.voucher-details');
    Route::get('/payment/voucher-download/{id}', 'PaymentController@download_pdf')->name('payment.voucher-download');
    Route::any('/payment/processing/list', 'PaymentController@processing_list')->name('payment.processing.list');
    Route::post('/checkpass', 'HomeController@checkpass')->name('admin.checkpass');
    Route::Resource('cities', 'CityController');
    Route::Resource('districts', 'DistrictController');
    Route::Resource('areas', 'AreaController');
    Route::resource('cats', 'CategoryController');
    Route::resource('subcats', 'SubcategoryController');
    Route::Resource('rider', 'RiderController');
    Route::get('/rider/assign/{id}/{area_id}', 'RiderController@show_need_assign')->name('admin.rider.assign');
    Route::post('/rider/assigned', 'RiderController@parcel_assigned')->name('admin.rider.assigned');
    // Route::get('/', 'HomeController@dashboard')->name('admin.dashboard');
    Route::get('logout', 'HomeController@logout')->name('admin.logout');
    Route::get('/parcel', 'ParcellController@index')->name('admin.parcel');
    Route::get('/parcel/change-status/{id}/{stage}/{extra}', 'ParcellController@change_status')->name('admin.parcel.change_status');
    Route::get('/parcel/show-status/{id}', 'ParcellController@show_status')->name('admin.parcel.show_status');
});
