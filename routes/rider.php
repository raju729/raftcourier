<?php

use Illuminate\Support\Facades\Route;
// Route::get('/', 'HomeController@dashboard')->name('admin.dashboard');

Route::get('/login', 'HomeController@login')->name('rider.login');
Route::post('/authenticate', 'HomeController@authenticate')->name('rider.authenticate');
Route::group(['middleware' => ['rider']], function () {
    Route::get('/', 'HomeController@dashboard')->name('rider.dashboard');
    Route::get('/parcel/pending', 'ParcelController@pending')->name('rider.parcel.pending');
    Route::get('/parcel/complete', 'ParcelController@complete')->name('rider.parcel.complete');
    Route::get('/parcels/track/{id}', 'ParcelController@track_parcel')->name('rider.track');
    Route::get('/parcel/change-status/{id}/{stage}/{extra}', 'ParcelController@change_status')->name('rider.parcel.change_status');

    Route::get('/notifications', 'HomeController@notifications')->name('rider.notifications');
    Route::get('/logout', 'HomeController@logout')->name('rider.logout');
    Route::get('/changepass', 'HomeController@changepass')->name('rider.changepass');
    Route::post('/changepass/submit', 'HomeController@changepass_submit')->name('rider.changepass.submit');
});
