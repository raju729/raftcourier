<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/home', function () {

//     return view('front.home');
// });

Route::get('/login', 'HomeController@login')->name('user.login');
Route::get('/', 'HomeController@home')->name('user.home');
Route::post('/authenticate', 'HomeController@authenticate')->name('user.authenticate');
Route::post('/signupotp', 'HomeController@signupotp')->name('user.signupotp');
Route::post('/checkotp', 'HomeController@checkotp')->name('user.checkotp');
Route::get('/faq', 'HomeController@faq')->name('user.faq');
Route::get('/policy', 'HomeController@policy')->name('user.policy');
Route::get('/coverage', 'HomeController@coverage')->name('user.coverage');
Route::get('/terms', 'HomeController@terms')->name('user.terms');
Route::group(['middleware' => ['user']], function () {

    Route::get('/logout', 'HomeController@logout')->name('user.logout');
    Route::get('/changepass', 'HomeController@changepass')->name('user.changepass');
    Route::post('/changepass/submit', 'HomeController@changepass_submit')->name('user.changepass.submit');

    Route::get('/notifications', 'HomeController@notifications')->name('user.notifications');
    Route::post('/notifications/update/{id}', 'HomeController@notif_update')->name('user.notification.update');
    Route::get('parcels/view', 'ParcelController@view')->name('user.view');
    Route::get('payments', 'PaymentController@index')->name('payments.index');
    Route::get('payments/details', 'PaymentController@details')->name('payments.details');
    Route::get('payments/pdf', 'PaymentController@pdf')->name('payments.pdf');
    Route::get('qr-code', 'ParcelController@qrcode')->name('user.qrcode');

    Route::get('/shopsetup', 'ShopController@shopsetup')->name('user.shopsetup');
    Route::get('/shopsetup/subcat/{id}', 'ShopController@find_subcat')->name('user.subcat');
    Route::get('/shopsetup/subcity/{id}', 'ShopController@find_subcity')->name('user.subcity');
    Route::get('/shopsetup/subarea/{id}', 'ShopController@find_subarea')->name('user.subarea');
    Route::resource('shops', 'ShopController');
    Route::resource('pickups', 'PickupController');
    Route::resource('paymethods', 'PaymethodController');
    Route::resource('parcels', 'ParcelController');
    Route::get('/parcels/label/{id}', 'ParcelController@label_download')->name('user.label.download');
    Route::get('/parcels/issue/{id}', 'ParcelController@issue')->name('user.issue');
    Route::get('/parcels/track/{id}', 'ParcelController@track_parcel')->name('user.track');
    Route::post('/parcels/track/', 'ParcelController@track_search')->name('user.track.submit');
    Route::get('/parcels/weightcharge/{id}', 'ParcelController@weight_charge')->name('user.weight.charge');
    Route::get('/parcels/areacharge/{id}', 'ParcelController@area_charge')->name('user.area.charge');
    Route::get('shops/activate/{id}', 'ShopController@activate')->name('shops.activate');

    Route::get('/merchants', 'MerchantController@dashboard')->name('merchant.dashboard');

});
