$("document").ready(function () {


  //STICKY MENU On Scroll
  var height = $("#header-icon").height();
  $(window).scroll(function () {
    if ($(this).scrollTop() > height) {
      $(".navbar").addClass("fixed");
    } else {
      $(".navbar").removeClass("fixed");
    }
  });

    //Owl-carousel For Mentor Page

    $(".slider .carousel .owl-carousel").owlCarousel({
      loop: true,
      autoplay: true,
      dots: true,
      responsive: {
        0: {
          items: 1,
        },
        768: {
          items: 2,
        },
        992: {
          items: 4,
        },
      },
    });

     //Owl-carousel For Signin Page

 $(".sign_slider .carousel .owl-carousel").owlCarousel({
  loop: true,
  autoplay: true,
  dots: true,
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 1,
    },
    992: {
      items: 1,
    },
  },
});


});




// BOTTOM TO TOP Button JS CODE
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
      $("#bottom-to-top").css({
        opacity: "1",
        "pointer-events": "auto",
      });
    } else {
      $("#bottom-to-top").css({
        opacity: "0",
        "pointer-events": "none",
      });
    }
  });
  $("#bottom-to-top").click(function () {
    $("html").animate({
      scrollTop: 0
    }, 500);
  });
});



// Form Validation 
$(document).ready(function() {
  $("#basic-form").validate({
    rules: {
      name : {
        required: true,
        minlength: 3
      },
      phone : {
        required: true,
        number: true,
        maxlength: 11,
        minlength: 11
      },
      
      email: {
        required: true,
        email: true
      },
      weight: {
        required: {
          depends: function(elem) {
            return $("#age").val() > 50
          }
        },
        number: true,
        min: 0
      }
    },
    messages : {
      name: {
        minlength: "Name should be at least 3 characters"
      },
        phone: {
        required: "Please enter your number",
        number: "Please enter a numerical value",
        min: "At least 11 digit"
      },
     
      email: {
        email: "The email should be in the format: abc@gmail.com"
      }
    }
  });
});