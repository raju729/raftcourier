<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('district_id');
            $table->foreignId('city_id');
            $table->foreignId('area_id');
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('card_number');
            $table->float('total_income', 8, 2)->nullable();
            $table->float('wallet', 8, 2)->nullable();
            $table->integer('successful_delivery')->nullable();;
            $table->enum('status', [0, 1]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
