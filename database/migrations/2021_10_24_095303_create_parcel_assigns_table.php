<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcel_assigns', function (Blueprint $table) {
            $table->id();
            $table->foreignId('rider_id');
            $table->foreignId('parcel_id');
            $table->set('status', ['success', 'failed', 'returned', 'ongoing']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcel_assigns');
    }
}
