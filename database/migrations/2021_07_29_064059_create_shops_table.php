<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('priority');
            $table->enum('status', [0, 1]);
            $table->string('shop_type');
            $table->string('owner_name, 100');
            $table->string('shop_name');
            $table->string('shop_email');
            $table->string('shop_address');
            $table->string('pickup_address');
            $table->string('pickup_area');
            $table->string('other_pickups');
            $table->string('pickup_phone');
            $table->integer('product_cat');
            $table->integer('product_subcat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
