<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('shop_id');
            $table->string('parcel_ids', 300);
            $table->float('cash_collection', 8, 2);
            $table->float('delivery_charge', 8, 2);
            $table->float('cod_charge', 8, 2)->nullable();
            $table->float('total_payable', 8, 2);
            $table->float('return_charge', 8, 2)->nullable();
            $table->set('status', ['unpaid', 'processing', 'hold', 'paid']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
