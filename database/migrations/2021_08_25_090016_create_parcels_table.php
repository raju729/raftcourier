<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('shop_id');
            $table->enum('assigned', [0, 1, 2]);
            $table->string('track_id')->unique();
            $table->set('overall_status', ['Pending', 'Ontransit', 'Completed', 'Returned']);
            $table->string('pickup_address');
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('customer_address');
            $table->string('merchant_invoice');
            $table->string('delivery_area');
            $table->integer('weight');
            $table->integer('category');
            $table->json('parcel_type');
            $table->float('sell_price', 8, 2);
            $table->string('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
}
