<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcel_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('track_id');
            $table->string('pickup_pending');
            $table->string('pickup_compeleted')->nullable();
            $table->string('send_shub')->nullable();
            $table->string('received_shub')->nullable();
            $table->string('return')->nullable();
            $table->string('return_del_onway')->nullable();
            $table->string('return_received')->nullable();
            $table->string('return_success')->nullable();
            $table->string('send_dhub')->nullable();
            $table->string('received_dhub')->nullable();
            $table->string('delivery_onway')->nullable();
            $table->string('delivery_delay')->nullable();
            $table->string('delivery_again')->nullable();
            $table->string('delivery_done')->nullable();
            $table->string('cash_collect')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcel_statuses');
    }
}
