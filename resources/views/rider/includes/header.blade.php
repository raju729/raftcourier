<header id="header">
            <div class="container-fluid">
                <div class="navbar">
                    <a href="{{route('user.home')}}" id="logo" title="Elegance by TemplateMo">
                        RaftCourier
                    </a>
                    <div class="navigation-row">
                        <nav id="navigation">
                            <button type="button" class="navbar-toggle"> <i class="fa fa-bars"></i> </button>
                            <div class="nav-box navbar-collapse">
                                <ul class="navigation-menu nav navbar-nav navbars" id="nav">
                                    @if(Auth::check() && (Auth::user()->user_type == 'User'))
                                    <li data-menuanchor="slide03"><a href="{{route('merchant.dashboard')}}">Dashboard</a></li>
                                    <li data-menuanchor="slide04"><a href="{{route('parcels.index')}}">Parcels</a></li>
                                    <li data-menuanchor="slide05"><a href="#slide05">Payments</a></li>
                                    <li data-menuanchor="slide06"><a href="#slide06">Coupon</a></li> 
                                    <li data-menuanchor="slide06"><a class="btn-sm btn-secondary" href="{{route('parcels.create')}}">Create parcel</a></li> 
                                     @else   
                                    <li data-menuanchor="slide01" class="active"><a href="#slide01">Home</a></li>
                                    <li data-menuanchor="slide02"><a href="#slide02">Enterprise</a></li>
                                        
                                    @endif




                                    @if(Auth::check() && (Auth::user()->user_type == 'User'))
                                        <li data-menuanchor="slide07">
                                                <div class="text-left" style="">
                                                    <button onclick="$('.menudiv').toggle()" class="btn-sm btn-primary" type="button" >  
                                                        {{check_shop()}}
                                                    </button>
                                                <div class=" menudiv p-3" style="position: absolute; top:45px;right:0;height:200px;width:150px; 
                                                    background:gray;color:black; display:none">
                                                    
                                                    <a class="" href="{{route('shops.index')}}">My shop</a>
                                                    <a class="" href="{{route('paymethods.index')}}">Payment Settings</a>
                                                    <a class="" href="">pickup locations</a>
                                                    <a class="" href="{{route('user.logout')}}">logout</a>
                                                </div>
                                            </div>
                                        </li>
                                       
                                            @else
                                            <li data-menuanchor="slide07">
                                            <a href="{{Route('user.login')}}">login</a>
                                           </li>
                                    @endif
                                    
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>