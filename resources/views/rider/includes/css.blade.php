
  



{{-- 
  prosanra css files --}}



      <!-- ========= Bootstrap CSS File  =========-->
      <link rel="stylesheet" href="{{asset('public/front/css/bootstrap.min.css')}}" />




      <!-- ========= Owl-Carousel CSS File For Gallery Page  =========-->
      <link rel="stylesheet" href="{{asset('public/front/css/owl.carousel.min.css')}}" />
      <link rel="stylesheet" href="{{asset('public/front/css/owl.theme.default.min.css')}}" />
      <!-- ========= Fontawesome CSS File For Icons  =========-->
      <link rel="stylesheet" href="{{asset('public/front/fontawesome/css/all.min.css')}}" />
     
      
    
      
      <!-- ========= Custom CSS File  =========-->
      <link rel="stylesheet" href="{{asset('public/front/css/style.css')}}" />
      <link rel="stylesheet" href="{{asset('public/front/css/responsive.css')}}" />

      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
      <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

