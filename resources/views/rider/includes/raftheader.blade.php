<section class="navbar_sec">

    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{route('user.home')}}">
            <img class="img-fluid" src="{{asset('public/front/images/raft_logo.jpg')}}" alt="logo" />
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="toggler-icon top-bar"></span>
            <span class="toggler-icon middle-bar"></span>
            <span class="toggler-icon bottom-bar"></span>
        </button>
        <div class="collapse navbar-collapse left_part" id="navbarSupportedContent">
           
            <div class="collapse navbar-collapse right_part " id="navbarSupportedContent">
                @if(Auth::check() && (Auth::user()->user_type == 'Rider'))
               

                <ul class="navbar-nav ">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('rider.dashboard')}}">Dashboard</a>
                    </li>               
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Parcel
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('rider.parcel.pending')}}">Pending</a>
                            <a class="dropdown-item" href="{{route('rider.parcel.complete')}}">Completed</a>
                            <a class="dropdown-item" href="{{route('user.logout')}}">On Going</a>
                        </div>
                    </li>


                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('payments.index')}}">Payments</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{check_rider()}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('rider.notifications')}}">Notification Settings</a>
                            <a class="dropdown-item" href="{{route('rider.changepass')}}">Change Password</a>
                            <a class="dropdown-item" href="{{route('rider.logout')}}">Logout</a>
                        </div>
                    </li>

                </ul>
              

              
                @endif

            </div>
        </div>

    </nav>
    @if(Auth::check() && (Auth::user()->user_type == 'User'))
    @if(  shop_count()>1 && count_pay_method()<1)
    <div class="banner_bor">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_item">
                        <div class="left">
                            <p><i class="fas fa-bullhorn"></i> পেমেন্ট পেতে আপনার ব্যাংক অথবা বিকাশ আকাউন্ট এর তথ্য অ্যাড
                                করুন</p>
                        </div>
                        <div class="right">
                            <a href="{{route('paymethods.create')}}">Add Payment Method</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif


    
    

</section>
