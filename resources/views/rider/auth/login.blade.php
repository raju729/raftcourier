@extends('rider.emptymaster')
@section('main')

<div id="rider_login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="main">
                    <div class="row">
                        <div class="col-md-5 col-12 hidden_part">
                            <div class="left">
                                <div class="title">
                                    <h2>Welcome to RaftCourier</h2>
                                </div>
                                <div class="rid_img">
                                    <img src="{{asset('public/front/images/rider-img.png')}}" alt="rider-img" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-12">
                            <div class="right">
                                <h2 class="float-left">Log in to acess</h2>
                                <form action="{{ route('rider.authenticate') }}" class="needs-validation" method="post">
                                    @csrf                            
                                    <div class="form-row justify-content-center">
                                        <div class="form-group col-sm-8 col-11">
                                            <label for="email">Phone Number</label>
                                            <div class="rider_input_part">
                                                <div class="rider_left">
                                                    <span><img
                                                            src="{{asset('public/front/images/bangladesh-flag.png')}}"
                                                            alt="bd" />
                                                        +88</span>
                                                </div>
                                                <div class="right">
                                                    <input type="number" autocomplete="false" required id="phone"
                                                        name="phone" placeholder="ফোন নম্বর" />

                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group col-sm-8 col-11">
                                            <label for="password">Password</label>
                                            <div class="form-label-group input-group eye_design">
                                                <input type="password" id="password" name="password"
                                                    class="form-control" placeholder="Current Password" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i id="eye" class="far fa-eye-slash"
                                                            onclick="showHidePwd();"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-8 col-11">
                                            <div class="form-check pl-0">
                                                <span>
                                                    <label>
                                                        <input type="checkbox" checked="checked" name="remember">
                                                        <small>Remember me</small>
                                                        <span class="float-right"><a href="#">Forgot Password</a></span>
                                                    </label>


                                                </span>

                                            </div>
                                        </div>
                                        <div class="form-group col-sm-8 col-11">
                                            <button type="submit">LOGIN</button>
                                        </div>

                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('custom-js')
<script>
    function showHidePwd() {
        var input = document.getElementById("password");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye").className = "far fa-eye-slash";
        }
    }

</script>
@endsection
