@extends('rider.master')
@section('main')
@php
!empty($parcel_status->pickup_pending)?$pickup_pending=json_decode($parcel_status->pickup_pending,true):"";
!empty($parcel_status->pickup_compeleted)?$pickup_compeleted=json_decode($parcel_status->pickup_compeleted,true):"";
!empty($parcel_status->send_shub)?$send_shub=json_decode($parcel_status->send_shub,true):"";
!empty($parcel_status->received_shub)?$received_shub=json_decode($parcel_status->received_shub,true):"";
!empty($parcel_status->send_dhub)?$send_dhub=json_decode($parcel_status->send_dhub,true):"";
!empty($parcel_status->received_dhub)?$received_dhub=json_decode($parcel_status->received_dhub,true):"";
!empty($parcel_status->delivery_onway)?$delivery_onway=json_decode($parcel_status->delivery_onway,true):"";
!empty($parcel_status->return)?$return=json_decode($parcel_status->return,true):"";
!empty($parcel_status->delivery_delay)?$delivery_delay=json_decode($parcel_status->delivery_delay,true):"";
!empty($parcel_status->delivery_done)?$delivery_done=json_decode($parcel_status->delivery_done,true):"";
!empty($parcel_status->cash_collect)?$cash_collect=json_decode($parcel_status->cash_collect,true):"";




@endphp


<div id="rider_track">

    <div class="track_title">
        <h1>Parcel Status</h1>
    </div>

    <div class="track_body">

        <div class="merchand small_two">
            <div class="marchant_title">
                <h2>MarchantDetails</h2>
            </div>
            <div class="marchant_item">
                <p>Name</p>
                <h4> Raju Das{{find_shop_details($parcel->shop_id)->name}}</h4>
            </div>
            <div class="marchant_item">
                <p>Shop Name</p>
                <h4>{{find_shop_details($parcel->shop_id)->shop_name}}</h4>
            </div>
            <div class="marchant_item">
                <p>Phone Number</p>
                <h4>{{find_shop_details($parcel->shop_id)->pickup_phone}}</h4>
            </div>
            <div class="marchant_item">
                <p>Address</p>
                <h4>{{find_shop_details($parcel->shop_id)->shop_address}}</h4>
            </div>
        </div>

        <div class="stages small_top" style="overflow-x:auto;">
            <div class="track_table">
                <table>
                    <thead>
                        <tr>
                            <th scope="col">Stage</th>
                            <th scope="col">Status</th>
                            <th scope="col">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($return['status']==2)
                        <tr>
                            <td>
                                <div class="text">
                                    
                                        <input type="text" name="returnotp" id="returnotp" class="form-control">
                                        <select class="form-control" id="cause_change" aria-label="Default select example">
                                          <option disabled selected>Open this select menu</option>
                                          
                                          <option value="wrong product">wrong product</option>
                                          <option value="wrong color">wrong color</option>
                                          <option value="wrong size">wrong size</option>
                                      
                                        </select>
                                       
                                    
                                    <p> October 28, 2021, 12:34 pm</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'return_checkotp',''])}}">N/A</a>
                            </td>
                        </tr>
                        @endif
                        @if($delivery_done['status']==1 && $parcel_status->cash_collect != null)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলের লেনদেনটি সম্পূর্ণ হয়েছে 
                                    </p>
                                    <p> {{$cash_collect['time']}}</p>
                                </div>
                            </td>
                            <td>{{$cash_collect['status'] == 1 ?'completed':'incompleted'}}</td>
                            <td>
                                @if($cash_collect['status'] == 0)
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'cash_collect','noextra'])}}">Change</a>
                                @else
                                N/A
                                @endif
                            </td>
                        </tr>
                        @endif
                        @if($delivery_done['status']==1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেল ডেলিভারি সম্পন্ন হয়েছে
                                        
                                    </p>
                                    <p>{{$delivery_done['time']}}</p>
                                </div>
                            </td>
                            <td>{{$delivery_done['status'] == 1 ?'completed':'incompleted'}}</td>
                            <td>
                                @if($delivery_done['status'] == 0)
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}">Change</a>
                                @else
                                N/A
                                @endif
                            </td>
                        </tr>
                        @endif
                        @if($delivery_onway['status']==1 && $delivery_delay['status']==0 && $return['status']==0 && $delivery_done['status']==0)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        if the parcel is return, press return
                                    </p>
                                    <p>
                                        if the parcel delivery success, press success
                                    </p>
                                    <p>
                                        if the parcel delivery delay, press delay
                                    </p>
                                    <p> </p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                                @if($delivery_delay['status']==0 && $return['status']==0 && $delivery_done['status']==0)
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'return_sendotp','noextra'])}}">Return</a>
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}">Success</a>
                                <a href="{{route('rider.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}">Delay</a>
                                @endif
                            </td>
                        </tr>
                        @endif
                        @if($delivery_onway['status']== 1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        ডেলিভারি এজেন্ট {{!empty($delivery_onway['rider'])? find_rider_details($delivery_onway['rider'])->name :'--'}}  ডেলিভারির জন্যে বের হয়েছে   </span><br>

                                        
                                    </p>
                                    <p> {{$delivery_onway['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                                N/A
                            </td>
                        </tr>
                        @endif
                        @if($received_dhub['status'] == 1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলটি {{isset($received_dhub['hub'])?$received_dhub['hub']:'--'}} এ রিসিভ করা হয়েছে
                                        
                                    </p>
                                    <p> {{$received_dhub['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                              N/A
                            </td>
                        </tr>
                        @endif
                      
                        @if($send_dhub['status']== 1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলটি {{isset($send_dhub['hub'])?$send_dhub['hub']:'--'}}  এ পাঠানো হচ্ছে
                                        
                                    </p>
                                    <p> {{$send_dhub['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                                N/A
                            </td>
                        </tr>
                        @endif
                        @if($received_shub['status'] == 1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলটি {{isset($received_shub['hub'])?$received_shub['hub']:'--'}} এ রিসিভ করা হয়েছে
                                        
                                    </p>
                                    <p> {{$received_shub['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                              N/A
                            </td>
                        </tr>
                        @endif
                      
                        @if($send_shub['status']== 1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলটি {{isset($send_shub['hub'])?$send_shub['hub']:'--'}}  এ পাঠানো হচ্ছে
                                        
                                    </p>
                                    <p> {{$send_shub['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                                N/A
                            </td>
                        </tr>
                        @endif
                        @if($pickup_compeleted['status'] ==1)
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেল পিকাপ সম্পন্ন হয়েছে
                                        
                                    </p>
                                    <p> {{$pickup_compeleted['time']}}</p>
                                </div>
                            </td>
                            <td>{{$pickup_compeleted['status'] ==1 ?'completed':'incompleted'}}</td>
                            <td>
                               N/A
                            </td>
                        </tr>
                        @endif
                        @if($pickup_pending['status']==1 && !empty($parcel_status->pickup_pending))
                        <tr>
                            <td>
                                <div class="text">
                                    <p>
                                        পার্সেলটি পিকআপের জন্য মার্চেন্ট অনুরোধ করেছেন
                                        
                                    </p>
                                    <p> {{$pickup_pending['time']}}</p>
                                </div>
                            </td>
                            <td>completed</td>
                            <td>
                               N/A
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="customer small_three">
            <div class="customer_title">
                <h2>কাস্টমার ও অর্ডারের বিস্তারিত তথ্য</h2>
            </div>
            <div class="customer_item">
                <p>Parcel ID</p>
                <h4>{{$parcel->track_id}}</h4>
            </div>
            <div class="customer_item">
                <p>Customer Address</p>
                <h4>{{$parcel->customer_address}}</h4>
            </div>
            <div class="customer_item">
                <p>Customer Phone</p>
                <h4>{{$parcel->customer_phone}}</h4>
            </div>
            <div class="customer_item">
                <p>Customer Name</p>
                <h4>{{$parcel->customer_name}}</h4>
            </div>
            <div class="customer_item">
                <p>Area</p>
                <h4>{{get_area_name($parcel->delivery_area)}}</h4>
            </div>
            <div class="customer_item">
                <p>Weight</p>
                <h4>500</h4>
            </div>
            <div class="customer_item">
                <p>Price</p>
                <h4>{{$parcel->sell_price}}</h4>
            </div>
            <div class="customer_item">
                <p>Delivery Charge</p>
                <h4>{{$delivery_details->delivery_charge}}</h4>
            </div>
            <div class="customer_item">
                <p>COD Charge</p>
                <h4>{{$delivery_details->cod_charge}}</h4>
            </div>

        </div>

    </div>
</div>






</div>
</div>

@endsection
