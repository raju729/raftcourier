@extends('rider.master')
@section('main')

<div id="dashboard">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="dash_top">
                    <div class="dash_left">
                        <h1>Welcome, RaftCourier</h1>
                        <p>Overview of your mission summary</p>
                    </div>
                    <div class="dash_right">
                        <div class="right_select">
                            <select name="" id=""> 
                                <option value="location">All Pickup Location</option>
                                <option value="dhaka">Dhaka</option>
                                <option value="Dhanmondi">Dhanmondi</option>
                            </select>
                        </div>
                        <div class="right_date">
                        
                            <label for="date"> Start Date ~ End Date :
                            <input type="text" name="daterange"  placeholder="start Date - End Date" id="date" />
                            <i class="far fa-calendar-alt"></i>
                            </label>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dash_item">

                <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_placed.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$data['complete']}}</h6>
                                <p>Mission Complete</p>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_placed.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$data['pending']}}</h6>
                                <p>Mission Pending</p>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_delivered.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>0</h6>
                                <p>Mission on Processing</p>
                            </div>
                        </div>                      
                    </div>

                   

                </div>
            </div>
        </div>
        

        <div class="row justify-content-center">
            <div class="col-12 dash_top">
                <p>Overview of your payment summary</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="dash_item">

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/total_sales.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$data['income']}}</h6>
                                <p>Total income using RaftCourier</p>
                                <small>128 successful delivery</small>
                            </div>
                        </div>
                        
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/delivery_fees_collected.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$data['wallet']}}</h6>
                                <p>Your Wallet</p>
                                <small></small>
                            </div>
                        </div>                      
                    </div>

                


                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/paid_amount.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$data['income']-$data['wallet']}}</h6>
                                <p>Paid amount</p>
                                <small>2 invoices created</small>
                            </div>
                        </div>                      
                    </div>                  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection