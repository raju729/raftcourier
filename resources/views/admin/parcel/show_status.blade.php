
@php
   !empty($parcel_status->pickup_pending)?$pickup_pending=json_decode($parcel_status->pickup_pending,true):"";
   !empty($parcel_status->pickup_compeleted)?$pickup_compeleted=json_decode($parcel_status->pickup_compeleted,true):"";
   !empty($parcel_status->send_shub)?$send_shub=json_decode($parcel_status->send_shub,true):"";
   !empty($parcel_status->received_shub)?$received_shub=json_decode($parcel_status->received_shub,true):"";
   !empty($parcel_status->send_dhub)?$send_dhub=json_decode($parcel_status->send_dhub,true):"";
   !empty($parcel_status->received_dhub)?$received_dhub=json_decode($parcel_status->received_dhub,true):"";
   !empty($parcel_status->delivery_onway)?$delivery_onway=json_decode($parcel_status->delivery_onway,true):"";
   !empty($parcel_status->return)?$return=json_decode($parcel_status->return,true):"";
   !empty($parcel_status->delivery_delay)?$delivery_delay=json_decode($parcel_status->delivery_delay,true):"";
   !empty($parcel_status->delivery_done)?$delivery_done=json_decode($parcel_status->delivery_done,true):"";
   !empty($parcel_status->cash_collect)?$cash_collect=json_decode($parcel_status->cash_collect,true):"";

   

@endphp
<div class="row">
  <div class="col-12">
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="20%" >Track Id</th>
            <th width="20%" >Stage</th>
            <th width="20%" >Status</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @if($pickup_pending['status']==1 && !empty($parcel_status->pickup_pending))
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header">পার্সেলটি পিকআপের জন্য মার্চেন্ট অনুরোধ করেছেন <br>
                            <span>{{$pickup_pending['time']}}</span>
                        </div>
                      </div>
                    
                  </td>
                  <td>
                    {{$pickup_pending['status']==1?'completed':""}}
                  </td>
                  <td>
                  </td>
              </tr>
              @endif
              @if($pickup_pending['status']==1 && !empty($parcel_status->pickup_pending))
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header">পার্সেল পিকাপ সম্পন্ন হয়েছে <br>
                            <span>{{$pickup_compeleted['time']}}</span>
                        </div>
                      </div>
                    
                  </td>
                  <td>
                    {{$pickup_compeleted['status'] ==1 ?'completed':'incompleted'}}
                  </td> 
                  <td>
                    @if($pickup_compeleted['status'] == 0)
                    
                      <a class="btn btn-info"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'pickup_compeleted','noextra'])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif
              @if($pickup_compeleted['status']==1 && $parcel_status->send_shub != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="ss_hub"> পার্সেলটি {{isset($send_shub['hub'])?$send_shub['hub']:'--'}}  এ পাঠানো হচ্ছে</span><br>
                            <span>{{$send_shub['time']}}</span>
                        </div>
                      </div>
                      @if($send_shub['status']== 0)
                      <div>
                       
                        <select class="form-control" id="hub_change" aria-label="Default select example">
                          <option disabled selected>Open this select menu</option>
                          @foreach($area as $key=> $area)
                          <option value="{{$area->name}}">{{$area->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      @endif
                  </td>
                  <td>
                    {{$send_shub['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($send_shub['status'] == 0)
                    
                      <a class="btn btn-info"onclick="hub_change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'send_shub',''])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif
              @if($send_shub['status']==1 && $parcel_status->received_shub != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="sr_hub">পার্সেলটি {{isset($received_shub['hub'])?$received_shub['hub']:'--'}} এ রিসিভ করা হয়েছে </span><br>
                            <span>{{$received_shub['time']}}</span>
                        </div>
                      </div>
                     
                  </td>
                  <td>
                    {{$received_shub['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($received_shub['status'] == 0)
                    
                      <a class="btn btn-info"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'received_shub','noextra'])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif

              @if($received_shub['status']==1 && $parcel_status->send_dhub != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="ds_hub"> পার্সেলটি {{isset($send_dhub['hub'])?$send_dhub['hub']:'--'}}  এ পাঠানো হচ্ছে</span><br>
                            <span>{{$send_dhub['time']}}</span>
                        </div>
                      </div>
                      @if($send_dhub['status']== 0)
                      <div>
                       
                        <select class="form-control" id="dhub_change" aria-label="Default select example">
                          <option disabled selected>Open this select menu</option>
                          @foreach($area as $key=> $area)
                          <option value="{{$area->name}}">{{$area->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      @endif
                  </td>
                  <td>
                    {{$send_dhub['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($send_dhub['status'] == 0)
                    
                      <a class="btn btn-info"onclick="dhub_change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'send_dhub',''])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif
              @if($send_dhub['status']==1 && $parcel_status->received_dhub != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="sr_hub">পার্সেলটি {{isset($received_dhub['hub'])?$received_dhub['hub']:'--'}} এ রিসিভ করা হয়েছে </span><br>
                            <span>{{$received_dhub['time']}}</span>
                        </div>
                      </div>
                     
                  </td>
                  <td>
                    {{$received_dhub['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($received_dhub['status'] == 0)
                    
                      <a class="btn btn-info"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'received_dhub','noextra'])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif
              @if($received_dhub['status']==1 && $parcel_status->delivery_onway != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="dl_rider">
                          ডেলিভারি এজেন্ট {{!empty($delivery_onway['rider'])? find_rider_details($delivery_onway['rider'])->name :'--'}}  ডেলিভারির জন্যে বের হয়েছে   </span><br>
                            <span>{{$delivery_onway['time']}}</span>
                        </div>
                      </div>
                      @if($delivery_onway['status']== 0)
                      <div>
                       
                        <select class="form-control" id="rider_change" aria-label="Default select example">
                          <option disabled selected>Open this select menu</option>
                          @foreach (find_riders($parcel_status->track_id) as $item)
                             <option value="{{$item->id}}">{{$item->name.'('.$item->phone.')'}}</option> 
                          @endforeach
                         
                      
                        </select>
                      </div>
                      @endif
                  </td>
                  <td>
                    {{$delivery_onway['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($delivery_onway['status'] == 0)
                    
                      <a class="btn btn-info"onclick="dman_change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'delivery_onway',''])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif

              @if($delivery_onway['status']==1 && $delivery_delay['status']==0 && $return['status']==0 && $delivery_done['status']==0)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:30rem;">
                        <div class="card-header"><span id="sr_hub">if the parcel is return, press return</span><br>
                            <span></span>
                        </div>
                    </div>
                    <div class="card text-white bg-success mb-3" style="max-width:30rem;">
                      <div class="card-header"><span id="sr_hub">if the parcel delivery success, press success</span><br>
                          <span></span>
                      </div>
                  </div>
                  <div class="card text-white bg-warning mb-3" style="max-width:30rem;">
                    <div class="card-header"><span id="sr_hub">if the parcel delivery delay, press delay </span><br>
                        <span></span>
                    </div>
                </div>
                     
                  </td>
                  <td>
                    {{$received_dhub['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($delivery_delay['status']==0 && $return['status']==0 && $delivery_done['status']==0)
                    
                      <a class="btn btn-info mb-1"onclick="change_status_return('{{route('admin.parcel.change_status',[$parcel_status->track_id,'return_sendotp','noextra'])}}')" type="submit">Return</a><br>
                      <a class="btn btn-success mb-1"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}')" type="submit">Success</a><br>
                      <a class="btn btn-warning"onclick="change_status_delay('{{route('admin.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}')" type="submit">Delay</a>

                    @endif

                   
                  </td>
              </tr>
              @endif

              @if($return['status']==2)
              <tr>
                <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <input type="text" name="returnotp" id="returnotp" class="form-control">
                    <select class="form-control" id="cause_change" aria-label="Default select example">
                      <option disabled selected>Open this select menu</option>
                      
                      <option value="wrong product">wrong product</option>
                      <option value="wrong color">wrong color</option>
                      <option value="wrong size">wrong size</option>
                  
                    </select>
                  </td>
                  <td>
                    <a class="btn btn-info mb-1"onclick="check_return_otp('{{route('admin.parcel.change_status',[$parcel_status->track_id,'return_checkotp',''])}}')" type="submit">Submit</a><br>

                  </td>

              </tr>
                    
              @endif

              @if($delivery_done['status']==1)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="sr_hub">পার্সেল ডেলিভারি সম্পন্ন হয়েছে</span><br>
                            <span>{{$delivery_done['time']}}</span>
                        </div>
                      </div>   
                  </td>
                  <td>
                    {{$delivery_done['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($delivery_done['status'] == 0)
                    
                      <a class="btn btn-info"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'delivery_done','noextra'])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif
              
              @if($delivery_done['status']==1 && $parcel_status->cash_collect != null)
              <tr>
                  <td></td>
                  <td>{{$parcel_status->track_id}}</td>
                  <td>
                    <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                        <div class="card-header"><span id="sr_hub">পার্সেলের লেনদেনটি সম্পূর্ণ হয়েছে</span><br>
                            <span>{{$cash_collect['time']}}</span>
                        </div>
                      </div>   
                  </td>
                  <td>
                    {{$cash_collect['status'] == 1 ?'completed':'incompleted'}}
                  </td>
                  <td>
                    @if($cash_collect['status'] == 0)
                    
                      <a class="btn btn-info"onclick="change_status('{{route('admin.parcel.change_status',[$parcel_status->track_id,'cash_collect','noextra'])}}')" type="submit">change</a>
                    
                    @endif
                  </td>
              </tr>
              @endif

              
            
          </tbody>
        </table>
      </div>
  </div>
</div>

<script type="text/javascript">

       $('#hub_change').on('change', function() {
          $id=this.value;
          $('#ss_hub').text('পার্সেলটি  '  +$id+ ' hub এ পাঠানো হচ্ছে')
          
        });
        $('#dhub_change').on('change', function() {
          $id=this.value;
          $('#ds_hub').text('পার্সেলটি  '  +$id+ ' hub এ পাঠানো হচ্ছে')
          
        });

        $('#rider_change').on('change', function() {
          $id= $( "#rider_change option:selected" ).text();
          $('#dl_rider').text('ডেলিভারি এজেন্ট '  +$id+ ' ডেলিভারির জন্যে বের হয়েছে ')
          
        });


        
</script>
