@extends('admin.master')
@section('main')
<div class="row">
  <div class="col-12">
     <!-- jquery validation -->
 
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Parcels List</h3>

        <div class="card-tools">
          
        <a href="javascript:void(0)" onclick="show_agent_modal('')" class="btn btn-md btn-primary">Add New Parcel</a>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="20%" >Track Id</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @foreach($parcels as $key=> $parcel)
            <tr>
              <td>{{$key+1}}</td>
            <td>{{$parcel->track_id}}</td>
              <td>
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="show_agent_modal('{{route('admin.parcel.show_status',$parcel->track_id)}}')" class=" btn btn-outline btn-info ">Change Status</a>
                  <a href="javascript:void(0)" onclick="confirm_modal('')" class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a>

              
              </div>
              </td>
             
              
            </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          {!! $parcels->links() !!}
        </ul>
      </div>
    </div>
    <!-- /.card -->
  </div>
</div>
<div class="modal fade" id="category_modal">
  <div class="modal-dialog modal-lg">
    <div id="modal_content" class="modal-content ">
    
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('customjs')
<script type="text/javascript">



  function show_agent_modal(dataUrl){
    
      $.get(dataUrl, function(data){
          $('#modal_content').html(data);
          $('#category_modal').modal('show', {backdrop: 'static'});
         
      });
  }

  function hub_change_status(dataUrl){
   var hubs = document.getElementById('hub_change').value;
   var f_url = dataUrl+'/'+hubs;

    console.log(f_url);
    $.get(f_url, function(data){
        console.log(data);
        $('#category_modal').modal('hide');
    });
}

function dhub_change_status(dataUrl){
   var hubs = document.getElementById('dhub_change').value;
   var f_url = dataUrl+'/'+hubs;

    console.log(f_url);
    $.get(f_url, function(data){
        console.log(data);
        $('#category_modal').modal('hide');
    });
}

function dman_change_status(dataUrl){
   var riders = document.getElementById('rider_change').value;
   var f_url = dataUrl+'/'+riders;
    $.get(f_url, function(data){
        console.log(data);
        $('#category_modal').modal('hide');
    });
}

function change_status(dataUrl){
  
  console.log(dataUrl);
    $.get(dataUrl, function(data){
        console.log(data);
        $('#category_modal').modal('hide');
    });
}

function check_return_otp(dataUrl){
   var cause = document.getElementById('cause_change').value;
   var otp = document.getElementById('returnotp').value;
   var  cause_otp = new Array(cause,otp);
   var  cause_otps=cause_otp.toString();;
  //  var cause_otp =json_encode(cause_otp);
   console.log(cause_otps);



   var f_url = dataUrl+'/'+cause_otp;

    console.log(f_url);
    // $.get(f_url, function(data){
    //     console.log(data);
    // });
}




function change_status_return(dataUrl){
  if (confirm("Do you want to return parcel?") == true) {
		   console.log(dataUrl);
    $.get(dataUrl, function(data){
        console.log(data);
        $('#category_modal').modal('hide');
    });
		} else {
			
		}

  
}




 function checkValidate(){
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
 }
 

</script>
@endsection