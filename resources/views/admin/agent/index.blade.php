@extends('admin.master')
@section('main')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Agent Details</h3>

        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="30%" >Name</th>
            <th>Email</th>
            <th>phone</th>
            <th style="display: table-cell;" class="footable-last-visible">Age</th>
            <th>Address</th>
            <th>Status</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @foreach($seller as $key =>$seller)
            <tr>
              <td>{{$key+1}}</td>
            <td>{{$seller->name}}</td>
              <td>{{$seller->email }} </td>
              <td>{{$seller->phone}}</td>
            <td>{{$seller->age}}</td>
              <td>{{$seller->district}}</td>
              <td> @if($seller->status=='1')
                <span style="color: green">Active</span>
            @else
                <span style="color: red">Block</span>
            @endif
              </td>
              <td>
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="show_agent_modal('{{route('agents.show', $seller->id)}}')" class=" btn btn-outline btn-warning btn-md "><i class="fa fa-eye"></i></a>

                  <a href="javascript:void(0)" onclick="show_agent_modal('{{route('agents.edit',$seller->id)}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
                  
              
              </div>
              </td>
            </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<div class="modal fade" id="category_modal">
  <div class="modal-dialog">
    <div id="modal_content" class="modal-content">
    
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection
@section('customjs')
<script type="text/javascript">


  function show_agent_modal(dataUrl){
   
      $.get(dataUrl, function(data){
          $('#modal_content').html(data);
          $('#category_modal').modal('show', {backdrop: 'static'});
      });
  }
</script>
@endsection