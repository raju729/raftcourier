<div class="row">
    <div class="col-md-12">

      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="http://deskbd.com/img/saiful.jpg"
                 alt="User profile picture">
          </div>

          <h3 class="profile-username text-center">{{$seller_details->name}}</h3>

          <p class="text-muted text-center">AGENT Deskbd</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Balance</b> <a class="float-right">{{$seller_details->total_balance}}  BDT</a>
            </li>
            <li class="list-group-item">
            <b>Email</b> <a class="float-right">{{$seller->email}}</a>
            </li>
            <li class="list-group-item">
            <b>Phone</b> <a class="float-right">{{$seller_details->phone}}</a>
            </li>
            <li class="list-group-item">
              <b>Age</b> <a class="float-right">{{$seller_details->age}} years</a>
            </li>
          </ul>

        </div>
        <!-- /.card-body -->
      </div>
     
    </div>
   <div class="col-md-12">
   <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">About Me</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <strong><i class="fas fa-book mr-1"></i> Education</strong>

          <p class="text-muted">
            deskbd University
          </p>

          <hr>

          <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

          <p class="text-muted">Dhaka, Bangladesh</p>

          <hr>

          <strong><i class="fas fa-pencil-alt mr-1"></i> Job History</strong>

          <p class="text-muted">
            <span class="tag tag-danger">{{$seller_details->job_description}} ,</span>
            <span class="tag tag-success">designer,</span>
            <span class="tag tag-info">Shopkeeper</span>
          
          </p>

          <hr>

          <strong><i class="far fa-file-alt mr-1"></i> Shop Details</strong>

          <p class="text-muted">i have a very niche and organised shop.</p>
        </div>
        <a  href="javascript:void(0)" onclick="show_agent_modal('{{route('agents.edit',$seller->id)}}')" class="btn btn-primary btn-md"><b>Edit</b></a>
        <!-- /.card-body -->
      </div>
      
    </div>    
    <!-- /.col -->
   
   
    <!-- /.col -->
  </div>