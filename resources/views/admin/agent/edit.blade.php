<form method="post" action="{{ route('agents.update',$seller->id) }}">
  <input name="_method" type="hidden" value="PATCH">
    @csrf
    {{-- <div class="overlay d-flex justify-content-center align-items-center">
        <i class="fas fa-2x fa-sync fa-spin"></i>
    </div> --}}
    <div class="modal-header">
        <h5 class="modal-title">Change Status Type</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>


        <div class="modal-body">
            <div class="input-group mb-3">
                <input type="text" readonly name="name" required class="form-control" placeholder="Email" value={{$seller_details->name}}>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              <div class="input-group mb-3">
                <input type="email" name="email" required class="form-control" placeholder="Email" value={{$seller->email}} readonly>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Change Agent Status</label>
                <select name="status" class="form-control">
                  <option>-->status type<--</option>
                  <option value=1>Active</option>
                  <option value=0>Block</option>
                 
                </select>
              </div>

        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
</form>