 <!-- jquery validation -->
 <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Setup Payment Details<small></small></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
  <form role="form" id="quickForm" method="post" action="{{route('payment.paid.submit')}}">
    @csrf
      <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Total Payable</label>
            <input type="text" name="total_payable" readonly class="form-control" required id="exampleInputEmail1" value="{{$paid->total_payable}}" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Transaction Id</label>
            <input type="text" name="transaction" required class="form-control" required id="exampleInputEmail1" placeholder="Enter Transaction Id">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Account Number</label>
            <input type="text" name="sent_account" required class="form-control" required id="exampleInputEmail1" placeholder="Enter Account Number">
          </div>
        <div class="form-group">
          <label>Payemnt Method</label>
          <select name="method" class="form-control " style="width: 100%;">
            <option value="Bkash">Bkash</option>
            <option value="Rocket">Rocket</option>
            <option value="Bank">Bank Acc</option>
        
          </select>
        </div>
      
          <input type="hidden" name="voucher_id" value="{{$paid->id}}" class="form-control">

      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit"  class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <script type="text/javascript">
  function checkValidate(){
    $('#quickForm').validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 5
        },
        terms: {
          required: true
        },
      },
      messages: {
        name: {
            required: "Please enter a name",
           
          },
          category: {
            required: "Please select a category",
           
          },
        terms: "Please accept our terms"
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  }
  
  </script>