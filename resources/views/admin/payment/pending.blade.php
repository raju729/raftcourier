@extends('admin.master')
@section('main')
<div class="row">
  <div class="col-12">
     <!-- jquery validation -->
 
    <div class="card">
      <div class="card-header">
        <h3 class="card-title mb-4">Pending Payments</h3>
        <form role="form" id="quickForm" method="get" action="{{route('payment.pending')}}">
            @csrf
            <div class="card-tools">
                <div class="form-group w-25">
                    <select  class="form-control js-example-basic-single" id="p_area_crg" name="shop_id" required>
                    <option disabled selected value> সিলেক্ট করুন</option>
                    @foreach ($all_shop as $all_shop)
                        <option value="{{$all_shop->id}}">{{$all_shop->shop_name}}</option>  
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-sm btn-warning d-inline"> find</button>
            {{-- <a href="javascript:void(0)" onclick="show_agent_modal('{{route('payment.pending')}}')" class="btn btn-md btn-primary">Add New City</a> --}}
            </div>
        </form>
      </div>
      <!-- /.card-header -->
      <form role="form" id="quickForm" method="post" action="{{route('payment.processing')}}">
        @csrf
        <input type="hidden" name="selected_shop" value="{{$selected_shop_id}}">
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap " id="pendingtable">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="20%">Track Id</th>
            <th width="20%">Delivery Charge</th>
            <th width="20%">Cash Collection (Tk)</th>
            <th width="20%">Total Payable (Tk)</th>
            <th width="20%">Created time</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @foreach($pending as $key=> $pending)
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$pending->track_id}}</td>
            <td>{{$pending->delivery_charge}}</td>
            <td>{{$pending->cash_collection}}</td>
            <td>{{$pending->total_payable}}</td>
            <td>{{$pending->created_at}}</td>
              <td>
                <div class="btn-group">
                  {{-- <a href="javascript:void(0)" onclick="show_agent_modal('{{route('cities.edit',$city->c_id)}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
                  <a href="javascript:void(0)" onclick="confirm_modal('{{route('cities.destroy',$city->c_id)}}')" class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a> --}}

                  <div class="form-check">
                    <input class="form-check-input" name="{{$pending->track_id}}" type="checkbox" value="{{$pending->track_id}}" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                      
                    </label>
                  </div>
              </div>
              </td>
             
              
            </tr>
            @endforeach
            
          </tbody>
        </table>
        
      </div>
      <div class="my-2" style="border:1px solid gainsboro; "></div>
      <button class="btn btn-md btn-success m-auto w-25">Submit</button>
      </form> 
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          {{-- {!! $city->links() !!} --}}
        </ul>
      </div>
    </div>
    <!-- /.card -->
  </div>
</div>
<div class="modal fade" id="category_modal">
  <div class="modal-dialog">
    <div id="modal_content" class="modal-content ">
    
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('customjs')
<script type="text/javascript">

$('#pendingtable').on('click', 'tr', function () {
  var currentRow=$(this).closest("tr").fadeOut('slow','swing')
         // get current row 3rd TD
         
         alert(data);
});

  $('#p_area_crg').on('change', function() {
          $id=this.value;
          
        });
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });

  function show_agent_modal(dataUrl){
   
      $.get(dataUrl, function(data){
        
          $('#modal_content').html(data);
          $('#category_modal').modal('show', {backdrop: 'static'});
          setTimeout(function(){ 
            $('.select2').select2();
              $('.select2bs4').select2({
                theme: 'bootstrap4'
              })
           }, 1000);
      });
  }

 function checkValidate(){
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
 }
 

</script>
@endsection