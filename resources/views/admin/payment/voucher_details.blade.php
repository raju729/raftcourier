<<<<<<< HEAD

<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap " id="pendingtable">
      <thead>
        <tr class="footable-header">
        <th class="footable-first-visible" style="display: table-cell;">#</th>
        <th width="20%">Track Id</th>
        <th width="20%">Delivery Charge</th>
        <th width="20%">Cash Collection (Tk)</th>
        <th width="20%">Total Payable (Tk)</th>
        <th width="20%">Cod Charges (Tk)</th>
        <th>Options</th>
      </tr>
    </thead>
      <tbody>
        @foreach($parcel_details as $key=> $voucher)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$voucher->track_id}}</td>
        <td>{{$voucher->delivery_charge}}</td>
        <td>{{$voucher->cash_collection}}</td>
        {{-- <td>{{$voucher->return_charge}}</td> --}}
        <td>{{$voucher->total_payable}}</td>
        <td>{{$voucher->cod_charge}}</td>
          <td>
            <div class="btn-group">
              {{-- <a href="javascript:void(0)" onclick="show_agent_modal('{{route('cities.edit',$city->c_id)}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
              <a href="javascript:void(0)" onclick="confirm_modal('{{route('cities.destroy',$city->c_id)}}')" class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a> --}}

              <div class="form-check">
                {{-- <input class="form-check-input" name="{{$processing->track_id}}" type="checkbox" value="{{$processing->track_id}}" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault"> --}}
                  
                </label>
              </div>
          </div>
          </td>
         
          
        </tr>
        @endforeach
=======
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      
      <style> 

#voucher{
  padding: 0px 0px 100px 0px;
}
#category_modal #modal_content{
  width: 120% !important;
}
.table_top{
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
}
.table_top .logo img{
  width: 80px;
  float: left;
}
.table_top .logo{
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: 100%;
  margin-bottom: 0px;
}
.table_top p{
  font-size: 14px;
  margin-bottom: 0px;
}
.table_top .bd{
  margin-bottom: 30px;
}
.table_top .bd p{
  text-align: center;
}
.table_top .company{
  width: 100%;
  margin-bottom: 30px;
}
.table_top .company .left{
    width: 70%;
    margin: 0 auto;
    padding-left: 142px;
}
.table_top .company .left p{
  width: 100%;
  display: flex;
  -webkit-box-pack: start;
  justify-content: start;
}
.table_top .company .left .first{
    flex: 1 1 0%;
    font-size: 14px;
   
}
.table_top .company .left .second{
    flex: 1.7 1 0%;
    font-size: 14px;
    font-weight: 700;
}
.table_top .customer{
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin-bottom: 40px;
}
.table_top .customer .left{
    width: 50%;
    margin: 0 auto;
}
.table_top .customer .left p{
  width: 100%;
  display: flex;
  -webkit-box-pack: start;
  justify-content: start;
}
.table_top .customer .left .first{
    flex: 1 1 0%;
    font-size: 14px;
   
}
.table_top .customer .left .second{
    flex: 1.7 1 0%;
    font-size: 14px;
    font-weight: 700;
}
.table_top .customer .right{
    width: 35%;
    margin: 0 auto;
}
.table_top .customer .right p{
  width: 100%;
  display: flex;
  -webkit-box-pack: start;
  justify-content: start;
}
.table_top .customer .right .first{
    flex: 1 1 0%;
    font-size: 14px;
   
}
.table_top .customer .right .second{
    flex: 1.7 1 0%;
    font-size: 14px;
    font-weight: 700;
}

</style>
   </head>
   <body>
      
>>>>>>> d2492f8d139847127f6d3ae44d729bfbdd73f62d
        
<div class="card-body table-responsive" id="voucher">

    <div class="table_top">

      <div class="logo">
          <img class="img-fluid" src="{{asset('public/front/images/raft_logo.jpg')}}" alt="logo" /> 
      </div>

      <div class="bd">
        <h1>RaftCourier</h1>
      </div>

      <div class="company">
        <div class="left">
            <p>
              <span class="first">Registered Person Name </span>
              <span class="second">: REDX Logistics Limited </span>
            </p>
            <p>
              <span class="first">Registered Person BIN </span>
              <span class="second">: 003255741-0402  </span>
            </p>
            <p>
              <span class="first">Address </span>
              <span class="second">: 50, Lake Circus, Kalabagan, Dhaka, PO : 1205, Bangladesh</span>
            </p>
        </div>
      </div>

      <div class="customer">
        <div class="left">
            <p>
              <span class="first">Customer Name </span>
              <span class="second">: E-Raft </span>
            </p>
            <p>
              <span class="first">Customer BIN </span>
              <span class="second">: +8801841337523</span>
            </p>
            <p>
              <span class="first">Customer Address </span>
              <span class="second">: Mahtab Center (6th Flr), 177 Shahid Syed Nazrul Islam Sarani, Bijoy Nagar, Dhaka.</span>
            </p>
            <p>
              <span class="first">Destination</span>
              <span class="second">: N/A </span>
            </p>
            
        </div>
        <div class="right">
            <p>
              <span class="first">Voucher Number</span>
              <span class="second">: 1</span>
            </p>
            <p>
              <span class="first">Issue Date</span>
              <span class="second">: November 3rd 2021</span>
            </p>
            <p>
              <span class="first">Issue Time </span>
              <span class="second">: 12:21 pm</span>
            </p>
        </div>
      </div>

    </div>
 


    <table class="table table-hover text-nowrap " id="pendingtable">
        <thead>
            <tr class="footable-header">
                <th class="footable-first-visible" style="display: table-cell;">#</th>
                <th width="20%">Track Id</th>
                <th width="20%">Delivery Charge</th>
                <th width="20%">Cash Collection (Tk)</th>
                <th width="20%">Total Payable (Tk)</th>
                <th width="20%">Cod Charges (Tk)</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach($parcel_details as $key=> $voucher)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$voucher->track_id}}</td>
                <td>{{$voucher->delivery_charge}}</td>
                <td>{{$voucher->cash_collection}}</td>
                {{-- <td>{{$voucher->return_charge}}</td> --}}
                <td>{{$voucher->total_payable}}</td>
                <td>{{$voucher->cod_charge}}</td>
                <td>
                    <div class="btn-group">
                        {{-- <a href="javascript:void(0)" onclick="show_agent_modal('{{route('cities.edit',$city->c_id)}}')"
                        class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" onclick="confirm_modal('{{route('cities.destroy',$city->c_id)}}')"
                            class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a> --}}

                        <div class="form-check">
                            {{-- <input class="form-check-input" name="{{$processing->track_id}}" type="checkbox"
                            value="{{$processing->track_id}}" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault"> --}}

                            </label>
                        </div>
                    </div>
                </td>


            </tr>
            @endforeach

        </tbody>
    </table>
<<<<<<< HEAD
    
  </div>
=======

</div>
       
   </body>
</html>



>>>>>>> d2492f8d139847127f6d3ae44d729bfbdd73f62d
