 <!-- jquery validation -->

 <div class="card card-primary">

    <div class="card-header">

      <h3 class="card-title">Edit Area Details<small></small></h3>

    </div>

    <!-- /.card-header -->

    <!-- form start -->

  <form role="form" id="quickForm" method="post" action="{{route('areas.update',$area->id)}}">

    <input name="_method" type="hidden" value="PATCH">

    @csrf

      <div class="card-body">
        
        <div class="form-group">
          <label>City</label>
          <select name="city_id" class="form-control " style="width: 100%;">
            @foreach($city as $key=> $city)
          <option {{$city->id == $area->city_id ? "selected" : ""}} value="{{$city->id}}">{{$city->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Area Name</label>
          <input type="text" required name="name" value="{{$area->name}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Category Name">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Delivery Charge</label>
          <input type="number" name="charge" class="form-control" value="{{$area->charge}}" required id="exampleInputEmail1" placeholder="Enter Delivery Charge">
        </div>
        <div class="form-check">
          <input type="checkbox" {{$area->pick_up ==1 ? "checked":""}} checked class="form-check-input" id="exampleCheck1" name="pickup">
          <label class=""  for="exampleCheck1"> Pick Up Point</label>
        </div>
        <div class="form-check">
          <input type="checkbox" {{$area->drop_off == 1 ? "checked": ""}} class="form-check-input" id="exampleCheck1" name="dropoff">
          <label class="" for="exampleCheck1"> Drop off Point</label>
        </div>



      </div>

  

     

      <!-- /.card-body -->

      <div class="card-footer">

        <button type="submit" onclick="checkValidate()" class="btn btn-primary">Submit</button>

      </div>

    </form>

  </div>

  

  <script type="text/javascript">

  function checkValidate(){

    $('#quickForm').validate({

      rules: {

        email: {

          required: true,

          email: true,

        },

        password: {

          required: true,

          minlength: 5

        },

        terms: {

          required: true

        },

      },

      messages: {

        name: {

          required: "Please enter a name",

         

        },

        category: {

          required: "Please select a category",

         

        },

        terms: "Please accept our terms"

      },

      errorElement: 'span',

      errorPlacement: function (error, element) {

        error.addClass('invalid-feedback');

        element.closest('.form-group').append(error);

      },

      highlight: function (element, errorClass, validClass) {

        $(element).addClass('is-invalid');

      },

      unhighlight: function (element, errorClass, validClass) {

        $(element).removeClass('is-invalid');

      }

    });

  }

  

  </script>