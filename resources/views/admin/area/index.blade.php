@extends('admin.master')
@section('main')
<div class="row">
  <div class="col-12">
     <!-- jquery validation -->
 
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Area List</h3>

        <div class="card-tools">
          
        <a href="javascript:void(0)" onclick="show_agent_modal('{{route('areas.create')}}')" class="btn btn-md btn-primary">Add New Area</a>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="20%">City Name</th>
            <th width="20%">Area</th>
            <th width="20%">Delivery Charge</th>
            <th width="20%">pick up</th>
            <th width="10%">Drop off</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @foreach($area as $key=> $area)
            <tr>
              <td>{{$key+1}}</td>
            <td>{{$area->c_name}}</td>
            <td>{{$area->ar_name}}</td>
            <td>{{$area->charge}}.00 tk</td>
            <td>Available</td>
            <td>N/A</td>
              <td>
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="show_agent_modal('{{route('areas.edit',$area->ar_id)}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
                  <a href="javascript:void(0)" onclick="confirm_modal('{{route('areas.destroy',$area->ar_id)}}')" class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a>

              
              </div>
              </td>
             
             
              
            </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          {{-- {!! $city->links() !!} --}}
        </ul>
      </div>
    </div>
    <!-- /.card -->
  </div>
</div>
<div class="modal fade" id="category_modal">
  <div class="modal-dialog">
    <div id="modal_content" class="modal-content ">
    
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('customjs')
<script type="text/javascript">

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });

  function show_agent_modal(dataUrl){
   
      $.get(dataUrl, function(data){
        
          $('#modal_content').html(data);
          $('#category_modal').modal('show', {backdrop: 'static'});
          setTimeout(function(){ 
            $('.select2').select2();
              $('.select2bs4').select2({
                theme: 'bootstrap4'
              })
           }, 1000);
      });
  }

 function checkValidate(){
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
 }
 

</script>
@endsection