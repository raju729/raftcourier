@extends('admin.master')
@section('main')

<div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>150</h3>

          <p>Total Shipment Count</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>53</h3>

          <p>Pending Shipment Count</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>44 <sup style="font-size: 20px"></sup></h3>

          <p>Delivered Shipment Count</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>65</h3>

          <p>Return Shipment</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    
  </div>

  <div class="row mt-3">
  

    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6">
              <h3 class="card-title"> Latest Shipments</h3>
            </div>
            <div class="col-md-6 ">
              <button type="button" class=" float-right btn  bg-gradient-warning">Add New Shipment</button>
            </div>
          </div>
          
        </div>
        
        <!-- /.card-header -->
        <div class="card-body p-0">
          <table class="table">
            <thead>
              <tr>
                <th style="width: 10px">Code</th>
                <th>Status</th>
                <th>Type</th>
                <th>Products</th>
                <th>Customer</th>
                <th>Shipping Cost</th>
                <th>Payment Method</th>
                <th>Shipping Date</th>
                
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>SH001</td>
                <td>Approved</td>
                <td>Pickup</td>
                <td>Bag</td>
                <td>Raju</td>
                <td>21.50 Tk</td>
                <td>COD</td>
                <td>12/11/2021</td>
              </tr>
              <tr>
                <td>SH001</td>
                <td>Approved</td>
                <td>Pickup</td>
                <td>Bag</td>
                <td>Raju</td>
                <td>21.50 Tk</td>
                <td>COD</td>
                <td>12/11/2021</td>
              </tr>
              <tr>
                <td>SH001</td>
                <td>Approved</td>
                <td>Pickup</td>
                <td>Bag</td>
                <td>Raju</td>
                <td>21.50 Tk</td>
                <td>COD</td>
                <td>12/11/2021</td>
              </tr>
              <tr>
                <td>SH001</td>
                <td>Approved</td>
                <td>Pickup</td>
                <td>Bag</td>
                <td>Raju</td>
                <td>21.50 Tk</td>
                <td>COD</td>
                <td>12/11/2021</td>
              </tr>
         
            
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-4">
      <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-bullhorn"></i>
            SHOP
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body text-center">
          <div class="callout callout-danger">
            <h5>Manage Shop</h5>
            <button type="button" class=" btn  bg-gradient-warning">Go to Settings</button>

          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <div class="col-md-4">
      <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-bullhorn"></i>
            SOLD AMMOUNT
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body text-center">
          <div class="callout callout-danger">
            <h5>Your Sold Ammount(curr)</h5>
            <button type="button" class=" btn  bg-gradient-danger">3000tk</button>
            <p>Total Sold: 20000tk</p>
            <p>LAst month Sold :30000tk</p>

          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <div class="col-md-4">
      <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-bullhorn"></i>
            PAYMENT
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body text-center">
          <div class="callout callout-danger">
            <h5>Configure Your Payment</h5>
            <button type="button" class="  btn  bg-gradient-warning">Configure Payment Method</button>

          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card bg-gradient-success">
        <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">

          <h3 class="card-title">
            <i class="far fa-calendar-alt"></i>
            Calendar
          </h3>
          <!-- tools card -->
          <div class="card-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                <i class="fas fa-bars"></i></button>
              <div class="dropdown-menu" role="menu">
                <a href="#" class="dropdown-item">Add new event</a>
                <a href="#" class="dropdown-item">Clear events</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">View calendar</a>
              </div>
            </div>
            <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0">
          <!--The calendar -->
          <div id="calendar" style="width: 100%"><div class="bootstrap-datetimepicker-widget usetwentyfour"><ul class="list-unstyled"><li class="show"><div class="datepicker"><div class="datepicker-days" style=""><table class="table table-sm"><thead><tr><th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Month"></span></th><th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Month">February 2021</th><th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Month"></span></th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td data-action="selectDay" data-day="01/31/2021" class="day old weekend">31</td><td data-action="selectDay" data-day="02/01/2021" class="day">1</td><td data-action="selectDay" data-day="02/02/2021" class="day">2</td><td data-action="selectDay" data-day="02/03/2021" class="day">3</td><td data-action="selectDay" data-day="02/04/2021" class="day">4</td><td data-action="selectDay" data-day="02/05/2021" class="day">5</td><td data-action="selectDay" data-day="02/06/2021" class="day weekend">6</td></tr><tr><td data-action="selectDay" data-day="02/07/2021" class="day active today weekend">7</td><td data-action="selectDay" data-day="02/08/2021" class="day">8</td><td data-action="selectDay" data-day="02/09/2021" class="day">9</td><td data-action="selectDay" data-day="02/10/2021" class="day">10</td><td data-action="selectDay" data-day="02/11/2021" class="day">11</td><td data-action="selectDay" data-day="02/12/2021" class="day">12</td><td data-action="selectDay" data-day="02/13/2021" class="day weekend">13</td></tr><tr><td data-action="selectDay" data-day="02/14/2021" class="day weekend">14</td><td data-action="selectDay" data-day="02/15/2021" class="day">15</td><td data-action="selectDay" data-day="02/16/2021" class="day">16</td><td data-action="selectDay" data-day="02/17/2021" class="day">17</td><td data-action="selectDay" data-day="02/18/2021" class="day">18</td><td data-action="selectDay" data-day="02/19/2021" class="day">19</td><td data-action="selectDay" data-day="02/20/2021" class="day weekend">20</td></tr><tr><td data-action="selectDay" data-day="02/21/2021" class="day weekend">21</td><td data-action="selectDay" data-day="02/22/2021" class="day">22</td><td data-action="selectDay" data-day="02/23/2021" class="day">23</td><td data-action="selectDay" data-day="02/24/2021" class="day">24</td><td data-action="selectDay" data-day="02/25/2021" class="day">25</td><td data-action="selectDay" data-day="02/26/2021" class="day">26</td><td data-action="selectDay" data-day="02/27/2021" class="day weekend">27</td></tr><tr><td data-action="selectDay" data-day="02/28/2021" class="day weekend">28</td><td data-action="selectDay" data-day="03/01/2021" class="day new">1</td><td data-action="selectDay" data-day="03/02/2021" class="day new">2</td><td data-action="selectDay" data-day="03/03/2021" class="day new">3</td><td data-action="selectDay" data-day="03/04/2021" class="day new">4</td><td data-action="selectDay" data-day="03/05/2021" class="day new">5</td><td data-action="selectDay" data-day="03/06/2021" class="day new weekend">6</td></tr><tr><td data-action="selectDay" data-day="03/07/2021" class="day new weekend">7</td><td data-action="selectDay" data-day="03/08/2021" class="day new">8</td><td data-action="selectDay" data-day="03/09/2021" class="day new">9</td><td data-action="selectDay" data-day="03/10/2021" class="day new">10</td><td data-action="selectDay" data-day="03/11/2021" class="day new">11</td><td data-action="selectDay" data-day="03/12/2021" class="day new">12</td><td data-action="selectDay" data-day="03/13/2021" class="day new weekend">13</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Year"></span></th><th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Year">2021</th><th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Year"></span></th></tr></thead><tbody><tr><td colspan="7"><span data-action="selectMonth" class="month">Jan</span><span data-action="selectMonth" class="month active">Feb</span><span data-action="selectMonth" class="month">Mar</span><span data-action="selectMonth" class="month">Apr</span><span data-action="selectMonth" class="month">May</span><span data-action="selectMonth" class="month">Jun</span><span data-action="selectMonth" class="month">Jul</span><span data-action="selectMonth" class="month">Aug</span><span data-action="selectMonth" class="month">Sep</span><span data-action="selectMonth" class="month">Oct</span><span data-action="selectMonth" class="month">Nov</span><span data-action="selectMonth" class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Decade"></span></th><th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Decade">2020-2029</th><th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Decade"></span></th></tr></thead><tbody><tr><td colspan="7"><span data-action="selectYear" class="year old">2019</span><span data-action="selectYear" class="year">2020</span><span data-action="selectYear" class="year active">2021</span><span data-action="selectYear" class="year">2022</span><span data-action="selectYear" class="year">2023</span><span data-action="selectYear" class="year">2024</span><span data-action="selectYear" class="year">2025</span><span data-action="selectYear" class="year">2026</span><span data-action="selectYear" class="year">2027</span><span data-action="selectYear" class="year">2028</span><span data-action="selectYear" class="year">2029</span><span data-action="selectYear" class="year old">2030</span></td></tr></tbody></table></div><div class="datepicker-decades" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Century"></span></th><th class="picker-switch" data-action="pickerSwitch" colspan="5">2000-2090</th><th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Century"></span></th></tr></thead><tbody><tr><td colspan="7"><span data-action="selectDecade" class="decade old" data-selection="2006">1990</span><span data-action="selectDecade" class="decade" data-selection="2006">2000</span><span data-action="selectDecade" class="decade active" data-selection="2016">2010</span><span data-action="selectDecade" class="decade active" data-selection="2026">2020</span><span data-action="selectDecade" class="decade" data-selection="2036">2030</span><span data-action="selectDecade" class="decade" data-selection="2046">2040</span><span data-action="selectDecade" class="decade" data-selection="2056">2050</span><span data-action="selectDecade" class="decade" data-selection="2066">2060</span><span data-action="selectDecade" class="decade" data-selection="2076">2070</span><span data-action="selectDecade" class="decade" data-selection="2086">2080</span><span data-action="selectDecade" class="decade" data-selection="2096">2090</span><span data-action="selectDecade" class="decade old" data-selection="2106">2100</span></td></tr></tbody></table></div></div></li><li class="picker-switch accordion-toggle"></li></ul></div></div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endsection