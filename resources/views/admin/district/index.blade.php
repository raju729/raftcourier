@extends('admin.master')
@section('main')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Manage Districts</h3>

        <div class="card-tools">
         
            <div class="">
              <a href="javascript:void(0)" onclick="show_add_category_modal()" class="btn btn-md btn-primary">Add New District</a>
            </div>
          
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr class="footable-header">
            <th class="footable-first-visible" style="display: table-cell;">#</th>
            <th width="30%" >District Name</th>
            <th>Options</th>
          </tr>
        </thead>
          <tbody>
            @foreach($dist as $key =>$dist)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$dist->name}}</td>
                                   
                                    <td>
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" onclick="show_edit_category_modal('{{route('districts.edit', base64_encode($dist->id))}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
                                            
                                            <a href="javascript:void(0)" onclick="confirm_modal('{{route('districts.destroy',base64_encode($dist->id))}}')" class=" btn btn-outline btn-danger "> <i class="fas fa-trash"></i> </a>
                                        
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<div class="modal fade" id="raft_modal">
    <div class="modal-dialog">
      <div id="modal_content" class="modal-content">
       
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection
@section('customjs')
<script type="text/javascript">
     function show_add_category_modal(){
        // $('#raft_modal').modal('show',{backdrop:'static'});
            $.get('{{ route('districts.create') }}', function(data){
             
                $('#modal_content').html(data);
                $('#raft_modal').modal('show',{backdrop:'static'});
            });
        }
        function show_edit_category_modal($url){
        // $('#raft_modal').modal('show',{backdrop:'static'});
            $.get($url, function(data){
             
                $('#modal_content').html(data);
                $('#raft_modal').modal('show',{backdrop:'static'});
            });
        }
</script>
@endsection