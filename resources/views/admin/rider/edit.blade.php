 <!-- jquery validation -->

 <div class="card card-primary">

    <div class="card-header">

      <h3 class="card-title">Edit Question Details<small></small></h3>

    </div>

    <!-- /.card-header -->

    <!-- form start -->

  <form role="form" id="quickForm" method="post" action="{{route('questions.update',$question->question_id)}}" enctype="multipart/form-data">
    @csrf
    <input name="_method" type="hidden" value="PATCH">
      <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Question</label>
            <input type="text" required name="question" value="{{$question->question_question}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Question">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Option A</label>
            <input type="text" required  name="opta"  value="{{$question->question_opt_a}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Option A">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Option B</label>
            <input type="text" required  name="optb"  value="{{$question->question_opt_b}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Option B">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Option C</label>
            <input type="text" required  name="optc"  value="{{$question->question_opt_c}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Option C">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Option D</label>
            <input type="text" required  name="optd"  value="{{$question->question_opt_d}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Option D">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Answer</label>
            <input type="text" required name="answer"  value="{{$question->question_ans}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Answer">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Choose Question Image</label>
            <input type="file" name="image" required class="form-control-file" id="exampleFormControlFile1">
          </div>
        
        

        



      </div>

  

     

      <!-- /.card-body -->

      <div class="card-footer">

        <button type="submit" onclick="checkValidate()" class="btn btn-primary">Submit</button>

      </div>

    </form>

  </div>

  

  <script type="text/javascript">

  function checkValidate(){

    $('#quickForm').validate({

      rules: {

        email: {

          required: true,

          email: true,

        },

        password: {

          required: true,

          minlength: 5

        },

        terms: {

          required: true

        },

      },

      messages: {

        name: {

          required: "Please enter a name",

         

        },

        category: {

          required: "Please select a category",

         

        },

        terms: "Please accept our terms"

      },

      errorElement: 'span',

      errorPlacement: function (error, element) {

        error.addClass('invalid-feedback');

        element.closest('.form-group').append(error);

      },

      highlight: function (element, errorClass, validClass) {

        $(element).addClass('is-invalid');

      },

      unhighlight: function (element, errorClass, validClass) {

        $(element).removeClass('is-invalid');

      }

    });

  }

  

  </script>