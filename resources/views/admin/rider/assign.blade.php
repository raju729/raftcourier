<form action="{{route('admin.rider.assigned')}}" method="post">
  @csrf
  <input type="hidden" name="rider_id" id="" value="{{$rider_id}}" >
<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap " id="pendingtable">
      <thead>
        <tr class="footable-header">
        <th class="footable-first-visible" style="display: table-cell;">#</th>
        <th width="20%">Track Id</th>
        <th width="20%">Customer name</th>
        <th width="20%">Status</th>
        <th width="20%">Sell Price</th>
        <th width="20%">Area</th>
        <th>Options</th>
      </tr>
    </thead>
      <tbody>
        
        @foreach($parcel as $key=> $voucher)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$voucher->track_id}}</td>
        <td>{{$voucher->customer_name}}</td>
        <td>{{$voucher->overall_status}}</td>
        {{-- <td>{{$voucher->return_charge}}</td> --}}
        <td>{{$voucher->sell_price}}</td>
        <td>{{get_area_name($voucher->delivery_area)}}</td>
          <td>
            <div class="btn-group">
              {{-- <a href="javascript:void(0)" onclick="show_agent_modal('{{route('cities.edit',$city->c_id)}}')" class=" btn btn-outline btn-info "><i class="fas fa-edit"></i></a>
              <a href="javascript:void(0)" onclick="confirm_modal('{{route('cities.destroy',$city->c_id)}}')" class=" btn btn-outline btn-danger btn-md "><i class="fa fa-trash"></i></a> --}}

              <div class="form-check">
                <input class="form-check-input" name="{{$voucher->track_id}}" type="checkbox" value="{{$voucher->id}}" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                  
                </label>
              </div>
          </div>
          </td>
         
          
        </tr>
        @endforeach
       
  
      </tbody>
    </table>
    
  </div>
  <button  class="btn btn-success float-right m-2">Submit </button>
</form>