 <!-- jquery validation -->

 <div class="card card-primary">

    <div class="card-header">

      <h3 class="card-title">Add Rider<small></small></h3>

    </div>

    <!-- /.card-header -->

    <!-- form start -->

  <form role="form" id="quickForm" method="post" action="{{route('rider.store')}}" enctype="multipart/form-data">
    @csrf
      <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" required name="name" value="" class="form-control" id="exampleInputEmail1" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            <input type="text" required  name="phone"  value="" class="form-control" id="exampleInputEmail1" placeholder="Enter phone">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            <input type="text" required  name="address"  value="" class="form-control" id="exampleInputEmail1" placeholder="Enter address">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Id Card Number</label>
            <input type="text" required  name="card_number" autocomplete="off"  value="" class="form-control" id="exampleInputEmail1" placeholder="Enter Card number">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">District</label>
          <select id="area" name="district_id" required  class="form-control">
              <option selected disabled>Select District</option>
              @foreach ($dist as $item)
              <option value="{{$item->id}}">{{$item->name}}</option>
              @endforeach
          </select>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">City</label>
        <select id="area" name="city_id" required  class="form-control">
            <option selected disabled>Select City</option>
            @foreach ($city as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Area</label>
            <select id="area" name="area_id" required  class="form-control">
                <option selected disabled> Select Area</option>
                @foreach ($area as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
       
        <div class="form-group">
            <label for="exampleFormControlFile1">Upload Your Image</label>
            <input type="file" name="image" name class="form-control-file" id="exampleFormControlFile1">
          </div>
      </div>

  

     

      <!-- /.card-body -->

      <div class="card-footer">

        <button type="submit" onclick="checkValidate()" class="btn btn-primary">Submit</button>

      </div>

    </form>

  </div>

  

  <script type="text/javascript">

  function checkValidate(){

    $('#quickForm').validate({

      rules: {

        email: {

          required: true,

          email: true,

        },

        password: {

          required: true,

          minlength: 5

        },

        terms: {

          required: true

        },

      },

      messages: {

        name: {

          required: "Please enter a name",

         

        },

        category: {

          required: "Please select a category",

         

        },

        terms: "Please accept our terms"

      },

      errorElement: 'span',

      errorPlacement: function (error, element) {

        error.addClass('invalid-feedback');

        element.closest('.form-group').append(error);

      },

      highlight: function (element, errorClass, validClass) {

        $(element).addClass('is-invalid');

      },

      unhighlight: function (element, errorClass, validClass) {

        $(element).removeClass('is-invalid');

      }

    });

  }

  

  </script>