 <!-- jquery validation -->
 <div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Setup Category Details<small></small></h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
<form role="form" id="quickForm" method="post" action="{{route('subcats.store')}}">
  @csrf
    <div class="card-body">
      <div class="form-group">
        <label>Category</label>
        <select name="cat_id" class="form-control select2" style="width: 100%;">
          @foreach($cats as $key=> $cat)
        <option value="{{$cat->id}}">{{$cat->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Sub-category Name</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Category Name">
      </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" onclick="checkValidate()" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<script type="text/javascript">
function checkValidate(){
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      name: {
          required: "Please enter a name",
         
        },
        category: {
          required: "Please select a category",
         
        },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
}

</script>