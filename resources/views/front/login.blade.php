@extends('front.master')
@section('main')
<div class="row mt-3 p-5" >
    <div class="col-md-4 offset-md-4 mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <h3> Raft Courier</h3>
        <p> Log in to see your details</p>

    <form action="{{ route('user.authenticate') }}" class="needs-validation" method="post">
        @csrf
        
        <div class="form-group">
            
            <input type="text" class="form-control" id="uname" placeholder="mobile #EX:01710000000" name="phone" required>
            <div class="valid-feedback">Valid.</div>
            <div class="invalid-feedback">Please fill out this field.</div>
        </div>
        <div class="form-group">
            
            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password" required>
            
            <div class="valid-feedback">Valid.</div>
            <div class="invalid-feedback">Please fill out this field.</div>
        </div>
        <div class="form-group form-check">
            <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="remember" required> I agree on RAftcourier policy.
            <div class="valid-feedback">Valid.</div>
            <div class="invalid-feedback">Check this checkbox to continue.</div>
            </label>
        </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    </div>
</div>

@endsection