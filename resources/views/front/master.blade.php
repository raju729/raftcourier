<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">

    <title>RaftCourier</title>
<!--
Elegance Template
https://templatemo.com/tm-528-elegance
-->
    <!-- Additional CSS Files -->

    @include('front.includes.css')
   
    </head>
    
    <body>
    
    <div id="">
        <div class="preloader">
            <div class="preloader-bounce">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        @include('front.includes.header')

   

        <!-- <video autoplay muted loop id="myVideo">
          <source src="images/delivery_concept.mp4" type="video/mp4">
        </video> -->

        <div id="fullpage" class="">

        @yield('main')
        </div>
{{-- 
        <div id="social-icons">
            <div class="text-right">
                <ul class="social-icons">
                    <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" title="Instagram"><i class="fa fa-behance"></i></a></li>
                </ul>
            </div>
        </div> --}}
    </div>  
    @include('front.includes.js')
    <script>
       
       jQuery(document).ready(function($){
            $("#btn-continue").click(function (e) {
               
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                var formData = {
            phone: jQuery('#phone_no').val(),
            
        };
        var state = jQuery('#btn-save').val();
        var type = "POST";
        var todo_id = jQuery('#todo_id').val();
        var ajaxurl = 'signupotp';
        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data.msg);

                if(data.msg == 'ok'){
                    $('#signupotp').removeClass('d-none');
                    $('#password').removeClass('d-none');
                    $("#btn-continue").addClass('d-none');
                    $("#btn-submit").removeClass('d-none');
                }
                else if(data.msg == 'taken'){
                    showFrontendAlert('error', 'Number already Used');
                }
                else{

                }
                   
                
                // jQuery('#myForm').trigger("reset");
                // jQuery('#formModal').modal('hide')
            },
            error: function (data) {
                console.log(data.msg);
            }
        });
            });

//button submit/////////////////////////////////////////////////////////////////////////

            $("#btn-submit").click(function (e) {
               
               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                   }
               });
               e.preventDefault();
               var formData = {
           phone: jQuery('#phone_no').val(),
           otp: jQuery('#signupotp').val(),
           password: jQuery('#password').val(),
           
       };
       var state = jQuery('#btn-save').val();
       var type = "POST";
       var todo_id = jQuery('#todo_id').val();
       var ajaxurl = 'checkotp';
       $.ajax({
           type: type,
           url: ajaxurl,
           data: formData,
           dataType: 'json',
           success: function (data) {
               console.log(data.msg);

               if(data.msg == 'required'){
                showFrontendAlert('error', 'all fields are required');
               }
               else if(data.msg == 'otpproblem'){
                   showFrontendAlert('error', 'OTP Not Matched');
               }
               else if(data.msg == 'saved'){
                window.location.href = "{{ route('user.login')}}";
                   showFrontendAlert('success', 'Id Created Successfully');
               }
               else if(data.msg == 'unsaved'){
                   showFrontendAlert('error', 'password not saved.please recreate');
               }
               else{

               }
                  
               
               // jQuery('#myForm').trigger("reset");
               // jQuery('#formModal').modal('hide')
           },
           error: function (data) {
               console.log(data.msg);
           }
       });
           });

        
        });


        
    </script>
    <script type="text/javascript">
        function showFrontendAlert(type, message){
            // if(type == 'danger'){
            //     type = 'error';
            // }
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: type,
            title: message
            })
        }

</script>
@yield('custom-js')
    @include('sweetalert::alert')


  </body>
</html>