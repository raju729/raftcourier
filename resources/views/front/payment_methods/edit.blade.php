@extends('front.raftmaster')
@section('main')
@php
!empty($paymethod->details)? $details = json_decode($paymethod->details,true):"";

@endphp
<div id="payment_create">
    <div class="container">
        <div class="row justify-content-center" id="payment_hide">
            <div class="col-12">
                <div class="top text-center">
                    <h1>পেমেন্ট ডিটেইলস আপডেট করুন</h1>
                    <p>Raft courier থেকে পেমেন্টের জন্য আপনার পছন্দসই ব্যাঙ্ক/বিকাশ অ্যাকাউন্ট যুক্ত করুন</p>
                </div>
            </div>
        </div>
        <div class="row" id="payment_hide1">
            <div class="col-12">
                <div class="create_body">
                    <div class="top"></div>
                    <div class="bottom">

                        <div class="row justify-content-center">
                            <div class="col-12 col-sm-10 ">
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-12 text-center mb-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="bank" value="Bank" {{$paymethod->pay_method =="Bank"?'checked':''}}>
                                            <label class="form-check-label" for="bank">
                                                Bank Account
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="bkash" value="Bkash"
                                                {{$paymethod->pay_method =="Bkash"?'checked':''}}>
                                            <label class="form-check-label" for="bkash">
                                                Bkash
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="nagad" value="Nagad"
                                                {{$paymethod->pay_method =="Nagad"?'checked':''}}>
                                            <label class="form-check-label" for="nagad">
                                                Nagad
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row justify-content-center {{$paymethod->pay_method =="Bank"?'':'d-none'}}"
                                    id="bank_show">
                                    <div class="col-sm-10 col-12">
                                        <form id="bank_prevent" method="post" action="{{route('paymethods.update',$paymethod->id)}}">

                                            <input name="_method" type="hidden" value="PATCH">
                                            @csrf
                                            <input type="hidden" value="Bank" name="paymethod">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-lg-6">
                                                    <label for="bank">Bank Name</label>
                                                    <input class="form-control" id='bank' list="browsers"
                                                        name="bank_name" value="@if($paymethod->pay_method =="
                                                        Bank"){{$details['bn']}} @endif" placeholder="Select Bank Name">
                                                    <datalist id="browsers">
                                                        <option value="Internet Explorer">
                                                        <option value="Firefox">
                                                        <option value="Chrome">
                                                        <option value="Opera">
                                                        <option value="Safari">
                                                    </datalist><br>



                                                    <label for="acc_name">Account Name</label>
                                                    <input class="form-control" name="acc_name" autocomplete="off"
                                                        value="@if($paymethod->pay_method ==" Bank"){{$details['an']}}
                                                        @endif" type="text" id="acc_name"> <br>

                                                    <label for="acc_num">Account Number</label>
                                                    <input class="form-control" name="acc_num" autocomplete="off"
                                                        value="@if($paymethod->pay_method ==" Bank"){{$details['a_nm']}}
                                                        @endif" type="text" id="acc_num"> <br>
                                                </div>

                                                <div class="col-12 col-lg-6">
                                                    <label for="branch_name">Branch Name</label>
                                                    <input type="text" class="form-control" id="branch_name"
                                                        placeholder="Branch Name" name="branch_name" required
                                                        value="@if($paymethod->pay_method ==" Bank"){{$details['br_n']}}
                                                        @endif"> <br>

                                                    <label for="acc_type">Account Type</label>


                                                    <input class="form-control" id='acc_type' name="acc_type"
                                                        value="@if($paymethod->pay_method ==" Bank"){{$details['at']}}
                                                        @endif" list="browsers1" placeholder="-- Current --">
                                                    <datalist id="browsers1">
                                                        <option value="Internet Explorer">
                                                        <option value="Firefox">
                                                        <option value="Chrome">
                                                        <option value="Opera">
                                                        <option value="Safari">
                                                    </datalist>
                                                    <br>



                                                    <label for="rot_num">Routing Number</label>
                                                    <input type="text" autocomplete="off" name="routing"
                                                        class="form-control" id="rot_num"
                                                        value="@if($paymethod->pay_method ==" Bank"){{$details['rt']}}
                                                        @endif" required> <br>
                                                </div>
                                                <div class="col-12 text-center">
                                                    <button onclick="otp_show()" type="submit">Confirm <span class="spinner-border spinner-border-sm"></span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>


                                <div class="row justify-content-center {{$paymethod->pay_method =="Bkash"?'':'d-none'}}" id="bkash_show">
                                    <div class="col-sm-10 col-12">
                                    <form id="bkash_prevent" method="post" action="{{route('paymethods.update',$paymethod->id)}}">

                                        <input name="_method" type="hidden" value="PATCH">
                                        @csrf
                                        <input type="hidden" value="Bkash" name="paymethod">
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-lg-6">

                                                <label for="bkash_num">Bkash Number</label>
                                                <input class="form-control" autocomplete="off"
                                                    value="@if($paymethod->pay_method =="Bkash"){{$details['bkash_no']}} @endif" name="bkash_number"
                                                    placeholder="Bkash Number" type="text" id="bkash_num"> <br>
                                            </div>

                                            <div class="col-12 col-lg-6">
                                                <label for="bkash_con">Confirm Bkash Number</label>
                                                <input type="text" class="form-control"
                                                    value="@if($paymethod->pay_method =="Bkash"){{$details['bkash_no']}} @endif" id="bkash_con"
                                                    placeholder="Confirm Bkash Number" required> <br>
                                            </div>

                                            <div class="col-12 text-center">
                                            <button onclick="otp_show()" type="submit">Confirm <span class="spinner-border spinner-border-sm"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>

                                <div class="row justify-content-center {{$paymethod->pay_method =="Nagad"?'':'d-none'}}" id="nagad_show">
                                    <div class="col-sm-10 col-12">
                                        <form id="nagad_prevent" method="post" action="{{route('paymethods.update',$paymethod->id)}}">

                                            <input name="_method" type="hidden" value="PATCH">
                                            @csrf
                                            <input type="hidden" value="Nagad" name="paymethod">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-lg-6">

                                                    <label for="nagad_num">Nagad Number</label>
                                                    <input class="form-control" autocomplete="off"
                                                        value="@if($paymethod->pay_method =="Nagad"){{$details['nagad_no']}} @endif" required
                                                        name="nagad_number" placeholder="Nagad Number" type="number"
                                                        id="nagad_num"> <br>
                                                </div>

                                                <div class="col-12 col-lg-6">
                                                    <label for="nagad_con">Confirm Nagad Number</label>
                                                    <input type="text" class="form-control"
                                                        value="@if($paymethod->pay_method =="Nagad"){{$details['nagad_no']}} @endif" id="nagad_con"
                                                        placeholder="Confirm Nagad Number" required> <br>
                                                </div>

                                                <div class="col-12 text-center">
                                                <button onclick="otp_show()" type="submit">Confirm <span class="spinner-border spinner-border-sm"></span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row justify-content-center" id="otp_part" style="display: none;">
            <div class="col-md-6 col-12 otp_center" style="margin: 0 auto;">
                <div class="payment_otp">
                     <div class="otp_top">
                         <div class="title">
                            <h2>Authenticate with OTP</h2>
                         </div>
                         <div class="otp_close">
                            <i id="opt_close_icon" class="fas fa-times"></i>
                         </div>

                    </div>
                    <div class="otp_input">
                        <input class="form-control" type="text" placeholder="Enter OTP" name="otp_name">
                        <p>শপ ওনার এর নাম্বারে আমরা একটি ওয়ান টাইম পাসওয়ার্ড (ওটিপি) এসএমএস করেছি.</p>
                    </div>
                    <div class="otp_btn">
                        <button type="submit">Authenticate</button>
                    </div>
                    <div class="otp_time">
                        <p>Reset OTP in 00 : <span id="time"></span></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="row mt-5" style="display: none;">
    <div class="col-12 mt-5">
        <h4>পেমেন্ট ডিটেইলস আপডেট করুন</h4>
        <p>Raftcourier থেকে পেমেন্টের জন্য আপনার পছন্দসই ব্যাঙ্ক/বিকাশ অ্যাকাউন্ট যুক্ত করুন</p>
        <div class=" display-inline" style="display: inline !important;margin-right:30px;">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="bank">
            <label class="form-check-label" for="exampleRadios1">
                Bank
            </label>
        </div>
        <div class=" display-inline" style="display: inline !important">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="mobile">
            <label class="form-check-label" for="exampleRadios1">
                Mobile Banking
            </label>
        </div>

        <div class="row text-left">
            <div class="col-md-2"></div>
            <div class="col-md-4">

                <div class="form-group">
                    <label for="email">Bank Name:</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Account Number:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Account Type:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>


                <button type="submit" class="btn btn-default">Submit</button>

            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Account Name:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>

                <div class="form-group">
                    <label for="pwd">Account Number:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Routing address:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
            </div>
        </div>


    </div>
</div>
@endsection


@section('custom-js')
<script>

function otp_show() {
  document.getElementById("otp_part").style.display = "block";

  $("#payment_hide").hide();
  $("#payment_hide1").hide();

  $("#bkash_prevent").click(function(event){
    event.preventDefault();
  });
  $("#nagad_prevent").click(function(event){
    event.preventDefault();
  });
  $("#bank_prevent").click(function(event){
    event.preventDefault();
  });
}

document.getElementById("opt_close_icon").addEventListener("click", otp_close);
           function otp_close(){ 
               $("#otp_part").hide();
               $("#payment_hide").show();
               $("#payment_hide1").show();
              
           };


function startTimer(duration, display) {
    var timer = duration, seconds;
    setInterval(function () {
 
        seconds = parseInt(timer % 60, 10);

        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent =  seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 5,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};

    $(document).ready(function () {

        $('#bank').click(function () {
            $("#bank_show").removeClass('d-none');
            $("#bkash_show").addClass('d-none');
            $("#nagad_show").addClass('d-none');
        });

        $('#bkash').click(function () {
            $("#bkash_show").removeClass('d-none');
            $("#bank_show").addClass('d-none');
            $("#nagad_show").addClass('d-none');
        });

        $('#nagad').click(function () {
            $("#nagad_show").removeClass('d-none');
            $("#bank_show").addClass('d-none');
            $("#bkash_show").addClass('d-none');
        });



    });

</script>
@endsection
