@extends('front.raftmaster')
@section('main')
@php
!empty($exist_method->details)?$details = json_decode($exist_method->details,true):"";
@endphp

<div id="payment_index">
    <div class="container">
        @if($exist_method_count>0)
        <div class="row">
            <div class="col">
                <div class="pay_heading">
                    <h1>Manage your Payment Method</h1> <a href="{{route('paymethods.edit',$exist_method->id)}}">Change Payment Method</a>
                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col">
                <div class="pay_body">
                    <div class="pay_top">
                        <p>Your default method to receive payments is through @if($exist_method_count>0){{$exist_method->pay_method}}@endif</p>
                    </div>
                    <div class="pay_item">
                        <ul>
                            @if($exist_method_count>0)
                            <li class="common">
                                @if($exist_method->pay_method=="Bank")
                                <p>
                                  <span class="first_span">Bank Name:</span>
                                  <span class="second_span">{{$details['bn']}}</span>
                                </p>
                                <p>
                                  <span class="first_span">Account Holder Name:</span>
                                  <span class="second_span">{{$details['an']}}</span>
                                </p>
                                <p>
                                  <span class="first_span">Branch Name:</span>
                                  <span class="second_span">{{$details['br_n']}}</span>
                                </p>
                                <p>
                                  <span class="first_span">Account Number:</span>
                                  <span class="second_span">{{$details['a_nm']}}</span>
                                </p>
                                
                                    
                                @elseif($exist_method->pay_method=="Bkash")
                                  <p>
                                    <span class="first_span">Account Number:</span>
                                    <span class="second_span">{{$details['bkash_no']}}</span>
                                  </p>
                                @elseif($exist_method->pay_method=="Nagad")
                                  <p>
                                    <span class="first_span">Account Number:</span>
                                    <span class="second_span">{{$details['nagad_no']}}</span>
                                  </p>
                                
                                @endif
                              </li>

                            
                            @else
                            <li class="first">
                                <a href="{{ route('paymethods.create')}}">
                                    <div class="add_account">
                                        <div class="top">
                                            <i class="fas fa-plus-circle"></i>
                                        </div>
                                        <div class="bottom">
                                            <p>একটি নতুন Account যুক্ত করুন</p>
                                        </div>
                                    </div>
                                </a>

                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row mt-5 p-5" style="display:none;">
    {{-- @foreach($shops as $key=>$shop)
    <div class="col-sm-3">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title c-b" >{{$shop->shop_name}}</h5>
    <p class="card-text c-b">{{$shop->shop_address}}</p>
    @if($shop->status =='1')
    <a href="{{route('shops.activate',$shop->id)}}" class="btn-sm btn-success">Working..</a>
    @else
    <a href="{{route('shops.activate',$shop->id)}}" class="btn-sm btn-primary">Activate</a>
    @endif
    <a href="{{route('shops.edit',$shop->id)}}" class="btn-sm btn-warning">edit</a>
</div>
</div>
</div>
@endforeach --}}
<div class="col-sm-3">
    <div class="card">
        <div class="card-body">

            <p class="card-text c-b">Add new Account to grow your business</p>
            <p class="card-text c-b"></p>
            <a href="{{ route('paymethods.create')}}" class="btn-lg btn-danger">Add Account</a>
        </div>
    </div>
</div>
</div>
@endsection
