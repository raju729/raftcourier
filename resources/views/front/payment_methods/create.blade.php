@extends('front.raftmaster')
@section('main')

<div id="payment_create">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="top text-center">
                    <h1>পেমেন্ট ডিটেইলস আপডেট করুন</h1>
                    <p>রেডএক্স থেকে পেমেন্টের জন্য আপনার পছন্দসই ব্যাঙ্ক/বিকাশ অ্যাকাউন্ট যুক্ত করুন</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="create_body">
                    <div class="top"></div>
                    <div class="bottom">

                        <div class="row justify-content-center">
                            <div class="col-sm-10 col-12">
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-12 text-center mb-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="bank" value="Bank" checked>
                                            <label class="form-check-label" for="bank">
                                                Bank Account
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="bkash" value="Bkash">
                                            <label class="form-check-label" for="bkash">
                                                Bkash
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" required name="paymethod"
                                                id="nagad" value="Nagad">
                                            <label class="form-check-label" for="nagad">
                                                Nagad
                                            </label>
                                        </div>
                                    </div>
                                </div>




                                <div class="row justify-content-center" id="bank_show">
                                    <div class="col-10">
                                        <form action="{{route('paymethods.store')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="Bank" name="paymethod">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-lg-6">
                                                    <label for="bank">Bank Name</label>
                                                    <input class="form-control" id='bank' list="browsers"
                                                        name="bank_name" placeholder="Select Bank Name">
                                                    <datalist id="browsers">
                                                        <option value="Internet Explorer">
                                                        <option value="Firefox">
                                                        <option value="Chrome">
                                                        <option value="Opera">
                                                        <option value="Safari">
                                                    </datalist><br>



                                                    <label for="acc_name">Account Name</label>
                                                    <input class="form-control" name="acc_name" autocomplete="off"
                                                        type="text" id="acc_name"> <br>

                                                    <label for="acc_num">Account Number</label>
                                                    <input class="form-control" name="acc_num" autocomplete="off"
                                                        type="number" id="acc_num"> <br>
                                                </div>

                                                <div class="col-12 col-lg-6">
                                                    <label for="branch_name">Branch Name</label>
                                                    <input type="text" class="form-control" id="branch_name"
                                                        placeholder="Branch Name" name="branch_name" required> <br>

                                                    <label for="acc_type">Account Type</label>


                                                    <input class="form-control" id='acc_type' name="acc_type"
                                                        list="browsers1" value="" placeholder="-- Current --">
                                                    <datalist id="browsers1">
                                                        <option value="Internet Explorer">
                                                        <option value="Firefox">
                                                        <option value="Chrome">
                                                        <option value="Opera">
                                                        <option value="Safari">
                                                    </datalist>
                                                    <br>



                                                    <label for="rot_num">Routing Number</label>
                                                    <input type="text" autocomplete="off" name="routing"
                                                        class="form-control" id="rot_num" value="" required> <br>
                                                </div>
                                                <div class="col-12 text-center">
                                                    <button type="submit">Confirm</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                                <div class="row justify-content-center" id="bkash_show" style="display: none;">
                                    <div class="col-10">
                                        <form action="{{route('paymethods.store')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="Bkash" name="paymethod">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-lg-6">

                                                    <label for="bkash_num">Bkash Number</label>
                                                    <input class="form-control" autocomplete="off" name="bkash_number"
                                                        placeholder="Bkash Number" type="number" id="bkash_num"> <br>
                                                </div>

                                                <div class="col-12 col-lg-6">
                                                    <label for="bkash_con">Confirm Bkash Number</label>
                                                    <input type="text" class="form-control" id="bkash_con"
                                                        placeholder="Confirm Bkash Number" required> <br>
                                                </div>
                                                <div class="col-12 text-center">
                                                    <button>Confirm</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                                <div class="row justify-content-center" id="nagad_show" style="display: none;">
                                    <div class="col-10">
                                        <form action="{{route('paymethods.store')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="Nagad" name="paymethod">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-lg-6">

                                                    <label for="nagad_num">Nagad Number</label>
                                                    <input class="form-control" autocomplete="off" required
                                                        name="nagad_number" placeholder="Nagad Number" type="number"
                                                        id="nagad_num"> <br>
                                                </div>

                                                <div class="col-12 col-lg-6">
                                                    <label for="nagad_con">Confirm Nagad Number</label>
                                                    <input type="text" class="form-control" id="nagad_con"
                                                        placeholder="Confirm Nagad Number" required> <br>
                                                </div>

                                                <div class="col-12 text-center">
                                                    <button type="submit">Confirm</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>


                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    

</div>



<div class="row mt-5" style="display: none;">
    <div class="col-12 mt-5">
        <h4>পেমেন্ট ডিটেইলস আপডেট করুন</h4>
        <p>Raftcourier থেকে পেমেন্টের জন্য আপনার পছন্দসই ব্যাঙ্ক/বিকাশ অ্যাকাউন্ট যুক্ত করুন</p>
        <div class=" display-inline" style="display: inline !important;margin-right:30px;">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="bank">
            <label class="form-check-label" for="exampleRadios1">
                Bank
            </label>
        </div>
        <div class=" display-inline" style="display: inline !important">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="mobile">
            <label class="form-check-label" for="exampleRadios1">
                Mobile Banking
            </label>
        </div>

        <div class="row text-left">
            <div class="col-md-2"></div>
            <div class="col-md-4">

                <div class="form-group">
                    <label for="email">Bank Name:</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Account Number:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Account Type:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>


                <button type="submit" class="btn btn-default">Submit</button>

            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Account Name:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>

                <div class="form-group">
                    <label for="pwd">Account Number:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Routing address:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
            </div>
        </div>


    </div>
</div>
@endsection


@section('custom-js')
<script>
    $(document).ready(function () {

        $('#bank').click(function () {
            $("#bank_show").show();
            $("#bkash_show").hide();
            $("#nagad_show").hide();
        });

        $('#bkash').click(function () {
            $("#bkash_show").show();
            $("#bank_show").hide();
            $("#nagad_show").hide();
        });

        $('#nagad').click(function () {
            $("#nagad_show").show();
            $("#bank_show").hide();
            $("#bkash_show").hide();
        });



    });

</script>
@endsection
