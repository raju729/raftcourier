@extends('front.raftmaster')
@section('main')


<div id="dashboard">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="dash_top">
                    <div class="dash_left">
                        <h1>Welcome, RaftCourier</h1>
                        <p>Overview of your order summary</p>
                    </div>
                    <div class="dash_right">
                        <div class="right_select">
                            <select name="" id=""> 
                                <option value="location">All Pickup Location</option>
                                <option value="dhaka">Dhaka</option>
                                <option value="Dhanmondi">Dhanmondi</option>
                            </select>
                        </div>
                        <div class="right_date">
                        
                            <label for="date"> Start Date ~ End Date :
                            <input type="text" name="daterange"  placeholder="start Date - End Date" id="date" />
                            <i class="far fa-calendar-alt"></i>
                            </label>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dash_item">

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/credit.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>0</h6>
                                <p>RaftCourier Credit</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Offering RaftCourier Credits during promotional offers as rewards</span></i>
                                </div>
                            </div>
                        </div>
                        <div class="items_right">
                            <a href="#">Details</a>
                        </div>
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_placed.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$allorders}}</h6>
                                <p>Orders placed</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total sum of parcels that have been created and picked up by RaftCourier</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_delivered.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$completed}}</h6>
                                <p>Orders delivered</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total sum of parcels that have been delivered</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_in_transit.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$ontransit}}</h6>
                                <p>Orders in transit</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total number of parcels that are going to be delivered soon</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_returned.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$returned}}</h6>
                                <p>Orders returned</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total number of parcels that have been returned</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/successful_delivery.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$pending}}</h6>
                                <p>Pending Order</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> The percentage of successful deliveries</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/orders_to_be_returned.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>0</h6>
                                <p>Orders to be returned</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total number of parcels that are going to be returned.</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                </div>
            </div>
        </div>
        

        <div class="row justify-content-center">
            <div class="col-12 dash_top">
                <p>Overview of your payment summary</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="dash_item">

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/total_sales.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>Tk. {{$total_collection+$delivery_charge}}</h6>
                                <p>Total sales using RaftCourier</p>
                                <small>128 successful delivery</small>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Sum of all the cash collection amount of the parcels that have been picked up by RaftCourier</span></i>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/delivery_fees_collected.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>Tk. {{$delivery_charge}}</h6>
                                <p>Total delivery fees paid</p>
                                <small>{{$completed}} successful delivery</small>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext">Total delivery charge of all the parcels that have been picked up by RaftCourier </span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/unpaid_amount.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>Tk. {{$unsettled}}</h6>
                                <p>Total Unsettled amount</p>
                                <small>0 orders delivered but unpaid</small>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext">Total amount due for invoice creation after delivery completion</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/payment_processing.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>Tk. {{$processing}}</h6>
                                <p>Payment Processing</p>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext">Total invoiced amount after delivery completion</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <div class="items">
                        <div class="items_left">
                            <div class="icon">
                                <img src="{{asset('public/front/images/dashboard_img/paid_amount.svg')}}" alt="credit">
                            </div>
                            <div class="text">
                                <h6>{{$paid}}</h6>
                                <p>Paid amount</p>
                                <small>23 invoices created</small>
                                <div class="icon_part">
                                    <i class="fas fa-info-circle"><span class="tooltiptext"> Total amount that have been disbursed to the merchant</span></i>
                                </div>
                            </div>
                        </div>                      
                    </div>                  

                </div>
            </div>
        </div>
    </div>
</div>






<div class="row mt-3 p-5 " style="display:none;" >
    <div class="col-md-10  offset-md-1 mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <p>Overview of your order summary</p>
        <div class="row">
           
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>{{$allorders}}</h4>
                <p>All Orders</p>
            </div>
            
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>{{$pending}}</h4>
                <p>pending Orders</p>
            </div>
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>{{$ontransit}}</h4>
                <p>Orders on Transit</p>
            </div>
            
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>{{$completed}}</h4>
                <p>Completed Orders</p>
            </div>

            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>{{$returned}}</h4>
                <p>Order to be Returned</p>
            </div>

        </div>

        <p>Overview of your Payment summary</p>
        <div class="row">
           
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>Tk. &nbsp; {{$total_collection}}</h4>
                <p>Total sales using RaftCourier</p>
            </div>
            
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>Tk. &nbsp;{{$delivery_charge}}</h4>
                <p>Total delivery fees paid</p>
            </div>
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>Tk. &nbsp;{{$unsettled}}</h4>
                <p>Total Unsettled amount</p>
            </div>
            
            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>Tk. &nbsp;{{$processing}}</h4>
                <p>Payment Processing
                </p>
            </div>

            <div class="col-md-3 m-4" style="background-color: cornflowerblue">
                <h4>Tk. &nbsp;</h4>
                <p>Paid amount</p>
            </div>

        </div>
         @if($allorders == 0)
         <h4 style="text-transform:none;text-decoration:underline"> Shop creation successfull</h3>
            <a class="btn btn-danger" href="{{route('parcels.create')}}">নতুন পার্সেল রিক্যুয়েস্ট তৈরি করুন</a>
         @endif
        
        
        
    </div>
</div>
@endsection

@section('custom-js')
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
@endsection