@extends('front.raftmaster')
@section('main')
<!-- ============  Start Banner Section  =============-->
<section class="banner">
    <div class="container-fluid">
        <div class="row item_part">
            <div class="col-lg-7 col-12 text_part float-left">
                <h1>
                    Parcel Delivered <br />
                    On Time with no Hassle
                </h1>
                <p>
                    Easily track your courier, Get courier within hours. Efficient &
                    safe delivery.
                </p>

            </div>
            <div class="col-lg-5 col-12 form_part float-left">

                <div class="middle">

                    <form id="basic-form" action="" method="post">
                        <div class="top">
                            <p>বিনামুল্যে সাইন-আপ করুন মাত্র দুই মিনিটে</p>
                        </div>
                        <div class="input_part">
                            <div class="left">
                                <span><img src="{{asset('public/front/images/bangladesh-flag.png')}}" alt="bd" />
                                    +88</span>
                            </div>
                            <div class="right">
                                <input type="number" autocomplete="false" required id="phone_no" name="phone"
                                    placeholder="ফোন নম্বর" aria-label="Search" />

                            </div>

                        </div>


                        <div class="middle_input">
                            <div class="first">
                                <input type="text" id="signupotp" class="d-none" name="signupotp"
                                    placeholder="Write OTP">
                            </div>


                            <div class="form-label-group input-group last d-none" id="pass_btn">
                                <input type="password" id="password" name="password"
                                    placeholder="Please Enter Your Password">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i id="eye" class="far fa-eye-slash" onclick="showHidePwd();"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="btn">
                            <button type="submit" id="btn-continue" class="sign_btn">সাইন-আপ</button>
                            <button type="submit" class="d-none" id="btn-submit" class="sign_btn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ============  End Banner Section  =============-->

<!--======= sTART Parcel Center =============-->
<section id="parcel">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-9 text-center">
                <div class="row parcel_items">
                    <div class="col-12 col-sm-9">
                        <form class="form_part" id="basic-form2" action="" method="post">
                            <i class="fas fa-globe-europe"></i>
                            <input type="text" placeholder="Type your tracking number" name="tracking" required />
                            <button class="par_btn">
                                <span>TRACK PARCEL</span>
                            </button>
                        </form>
                    </div>
                    <div class="col-12 col-sm-3 book">
                        <a href="#">
                            <span><i class="fas fa-truck-moving"></i> BOOK P2P</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--======= End Parcel Center =============-->

<!--==================  Start Service Part ===================-->
<div id="service">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col">
                <h2>সার্ভিস সমূহ</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/1.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>পার্সেল ডেলিভারি</h3>
                        <p>
                            আমরা খুব সীমিত পরিমাণ চার্জে পাইকারী বিক্রেতা, খুচরা বিক্রেতা, এজেন্ট, ফেসবুক শপ এবং
                            কর্পোরেট অফিস থেকে পণ্য সংগ্রহ এবং বিতরণ যথা সময়ে করে থাকি।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/2.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>বুলেট পরিসেবা</h3>
                        <p>
                            আমরা স্বল্পতম সময়ে চিঠি, নথি, পণ্য, উপহারের জন্য দ্রুত পরিষেবা দিয়ে থাকি। গ্রাহকের থেকে
                            পার্সেল সংগ্রহ করা হবে এবং যথা সময়ের মধ্যে গন্তব্যে পৌঁছে দেওয়া হবে।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/3.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>একই দিনে ডেলিভারি</h3>
                        <p>
                            আমরা আপনার মূল্যবান সময়ের গুরুত্ব বুঝি। অতএব, আমরা একটি স্বল্প মূল্যে, সময়মত এবং নির্ভর
                            যোগ্য ডেলিভারি প্রদান করে থাকি।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/4.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>রিটার্ন সার্ভিস</h3>
                        <p>
                            গ্রাহকের নথি এবং পার্সেলগুলি কেবল বিতরণই নয়, প্রাপ্তির প্রমাণ হিসাবে প্রেরিত ব্যক্তির কাছ
                            থেকে যথাযথভাবে পূর্নাঙ্গ ডকুমেন্ট প্রদান করা হয়।
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center bottom_heading">
                <h2>কেন রাফ্ট কুরিয়ার কে পার্টনার হিসেবে বেছে নিবেন?</h2>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/5.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>প্রতিশ্রুতিবদ্ধ দল</h3>
                        <p>
                            আমাদের কাছে বন্ধুত্বপূর্ণ এবং দক্ষ গ্রাহক পরিষেবা প্রতিনিধিদের একটি নিবেদিত দল রয়েছে যারা
                            সার্বক্ষণিক পরিসেবা দিয়ে থাকে । তারা সম্ভাব্য সব উপায়ে সহায়তা প্রদান করবে।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/6.png')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>সুলভ মূল্যে পরিসেবা</h3>
                        <p>
                            রাফ্ট কুরিয়ার গুণমান এবং স্বচ্ছতায় বিশ্বাস করে। আমরা আপনাকে একটি নিরাপদ, সাশ্রয়ী মূল্যের,
                            নির্ভরযোগ্য এবং যথাসময়ে বিতরণ পরিসেবার নিশ্চয়তা দিচ্ছি।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/7.png')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>দ্রুত ডেলিভারি</h3>
                        <p>
                            দ্রুত এবং সময়মত ডেলিভারি র‍্যাফ্ট কুরিয়ার সার্ভিসের সর্বোচ্চ অগ্রাধিকার। অতএব, আপনি আমাদের
                            পরিসেবা নেয়ার ফলে কখনোই হতাশ হবেন না।
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="service_item">
                    <div class="ser_img">
                        <img src="{{asset('public/front/images/service/8.jpg')}}" alt="service" class="img-fluid" />
                    </div>
                    <div class="ser_text">
                        <h3>আইটি পরিকাঠামো</h3>
                        <p>
                            আমাদের আইটি দলগুলি আমাদের ক্লায়েন্টদের জন্য রিয়েল-টাইম ভিত্তিতে পার্সেল, ডেলিভারি ম্যান
                            এবং তাদের দক্ষতা ট্র্যাক করার নিশ্চয়তা প্রদান করে।
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==================  End Service Part ===================-->

<!-- ============= Start Partner Section =================-->
<div id="partner">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <h2>Choose RaftCourier as your logistics partner</h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="far fa-clock"></i>
                    </div>
                    <div class="part_text">
                        <h3>৩ দিনে ডেলিভারি গ্যারান্টিড</h3>
                        <p>
                            মাত্র ৩ দিনে বাংলাদেশের যেকোনো প্রান্তে পার্সেল ডেলিভারির
                            নিশ্চয়তা
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="fas fa-door-closed"></i>
                    </div>
                    <div class="part_text">
                        <h3>ডোরস্টেপ পিকআপ ও ডেলিভারি</h3>
                        <p>
                            আপনার দরজা থেকে কাঙ্ক্ষিত গন্তব্যে পার্সেল পৌঁছে যাবে
                            নির্বিঘ্নে
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="fas fa-mobile-alt"></i>
                    </div>
                    <div class="part_text">
                        <h3>এসএমএস আপডেট</h3>
                        <p>
                            পার্সেল বুকিং এবং ডেলিভারির সময় পাবেন আপনার নিবন্ধিত মোবাইল
                            নম্বরে এসএমএস আপডেট
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="fas fa-money-check-alt"></i>
                    </div>
                    <div class="part_text">
                        <h3>পরের দিনই পেমেন্ট</h3>
                        <p>ডেলিভারি সম্পূর্ণ হলে পরের দিনই পেমেন্ট পেয়ে যাবেন</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="fas fa-money-bill-alt"></i>
                    </div>
                    <div class="part_text">
                        <h3>সেরা ক্যাশ অন ডেলিভারি রেট</h3>
                        <p>ঢাকার ভিতর ক্যাশ অন ডেলিভারি চার্জ ০%, ঢাকার বাইরে ১%</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="part_item">
                    <div class="part_icon">
                        <i class="fas fa-user-shield"></i>
                    </div>
                    <div class="part_text">
                        <h3>সিকিউর হ্যান্ডলিং</h3>
                        <p>সর্বোচ্চ নিরাপদে শিপমেন্টের নিশ্চয়তা ও ক্ষতিপূরণ সুবিধা</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============= End Partner Section =================-->

<!-- =========== Start District Section ===============-->
<div id="district">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-12">
                <div class="map">
                    <img src="{{asset('public/front/images/bangladash-map.svg')}}" alt="map" class="img-fluid" />
                </div>
            </div>
            <div class="col-lg-7 col-12">
                <div class="dis_text">
                    <div class="upper_text">
                        <h2>
                            RaftCourier লজিস্টিকস সেবা সারাদেশের ৬৪ জেলা ৪৯০+ উপজেলা জুড়ে
                            বিস্তৃত।
                        </h2>
                        <p>
                            আপনার যাই প্রয়োজন আমরা নিশ্চিত করি সারাদেশ জুড়ে সবচেয়ে
                            দ্রুতগতির সেবা।
                        </p>
                    </div>
                    <div class="dis_btn">
                        <button type="button"><a href="{{route('user.coverage')}}"> কভারেজ এলাকা দেখুন</a></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========== End District Section ===============-->

<!-- ============ Start Calculator Section =============-->
<div id="calculator">
    <div class="container">
        <div class="row text-center align-items-center">
            <div class="col">
                <div class="cal_text">
                    <h2>ডেলিভারি ক্যালকুলেটর</h2>
                    <p>
                        আপনার শিপমেন্টের সুবিধার জন্য আগে থেকেই আপনার ডেলিভারী চার্জ
                        সম্পর্কে ধারণা নিন
                    </p>
                </div>
                <div class="cal_form">
                    <form>
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-sm-6 col-12 text-left">
                                <label for="kg">পণ্যের ওজন ( সর্বোচ্চ 10 কেজি )</label>
                                <input type="text" class="form-control" id="kg" placeholder="পণ্যের ওজন লিখুন" />
                            </div>
                            <div class="col-lg-5 col-sm-6 col-12 text-left">
                                <label for="sell">পণ্যের বিক্রয়মূল্য</label>
                                <input type="text" id="sell" class="form-control"
                                    placeholder="পণ্যের বিক্রয়মূল্য লিখুন" />
                            </div>
                            <div class="col-lg-5 col-sm-6 col-12 text-left mt-3">
                                <label>পিক-আপ এলাকা</label>

                                <div class="nav_drop">
                                    <input type="checkbox" id="menu" class="form-control" />
                                    <img src="{{asset('public/front/images/arrow.png')}}" class="arrow"><label
                                        class="same" for="menu">পিক-আপের এলাকা নির্বাচন করুন</label>


                                    <div class="multi-level">


                                        <div class="item">
                                            <input type="checkbox" id="A" />
                                            <img src="{{asset('public/front/images/arrow.png')}}" class="arrow"><label
                                                for="A">Dhaka <span>13</span></label>

                                            <ul>
                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="A-1" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="A-1">Dhaka
                                                            <span>527</span></label>
                                                        <ul>
                                                            <li> <a href="#">Mohammadpur</a> </li>
                                                            <li> <a href="#">Dhanmondi</a> </li>
                                                            <li> <a href="#">Paltan</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="A-2" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="A-2">Gazipur
                                                            <span>47</span></label>
                                                        <ul>
                                                            <li> <a href="#">Tongi</a> </li>
                                                            <li> <a href="#">Chowrasta</a> </li>
                                                            <li> <a href="#">Kaliakoir</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>

                                        <div class="item">
                                            <input type="checkbox" id="B" />
                                            <img src="{{asset('public/front/images/arrow.png')}}" class="arrow"><label
                                                for="B">Mymensingh <span>4</span></label>

                                            <ul>
                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="B-1" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="B-1">Jamalpur
                                                            <span>7</span></label>
                                                        <ul>
                                                            <li> <a href="#">Bakshiganj</a> </li>
                                                            <li> <a href="#">Dewanganj</a> </li>
                                                            <li> <a href="#">Islampur</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="B-2" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="B-2">Sherpur
                                                            <span>47</span></label>
                                                        <ul>
                                                            <li> <a href="#">Nakla</a> </li>
                                                            <li> <a href="#">Nalitabari</a> </li>
                                                            <li> <a href="#">Sribordi</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-sm-6 col-12 text-left mt-3">
                                <label>ডেলিভারি এলাকা</label>

                                <div class="nav_drop">
                                    <input type="checkbox" id="menu2" class="form-control" />
                                    <img src="{{asset('public/front/images/arrow.png')}}" class="arrow"><label
                                        class="same" for="menu2">পিক-আপের এলাকা নির্বাচন করুন</label>


                                    <div class="multi-level">

                                        @foreach(find_district() as $data)
                                        <div class="item">
                                            <input type="checkbox" id="AA" />
                                            <img src="{{asset('public/front/images/arrow.png')}}" class="arrow"><label
                                                for="AA">{{$data->name}} <span>13</span></label>

                                            <ul>
                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="AA-1" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="AA-1">Dhaka
                                                            <span>527</span></label>
                                                        <ul>
                                                            <li> <a href="#">Mohammadpur</a> </li>
                                                            <li> <a href="#">Dhanmondi</a> </li>
                                                            <li> <a href="#">Paltan</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="sub-item">
                                                        <input type="checkbox" id="AA-2" />
                                                        <img src="{{asset('public/front/images/arrow.png')}}"
                                                            class="arrow"><label for="AA-2">Gazipur
                                                            <span>47</span></label>
                                                        <ul>
                                                            <li> <a href="#">Tongi</a> </li>
                                                            <li> <a href="#">Chowrasta</a> </li>
                                                            <li> <a href="#">Kaliakoir</a> </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                        @endforeach

                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center cal_btn">
                                <button type="button">ডেলিভারি চার্জ দেখুন</button>
                                <p>
                                    আপনি কি জানতে চান, যেকোনো লোকেশনে আপনার পার্সেল পাঠাতে কত
                                    খরচ হতে পারে? <br />
                                    আমাদের বিস্তারিত <a id="charge_list_show" href="javascript:void 0;"> লিস্টটি
                                        দেখুন।</a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="charge_hide" style="display: none;">
            <div class="col-12">
                <div class="charge_list">
                    <div class="charge_icon ">
                        <i class="far fa-times-circle " id="close_btn_icon"></i>
                    </div>
                    <div class="charge_title text-center">
                        <h1>ডেলিভারি চার্জ দেখুন</h1>
                        <p>প্রতিটি পার্সেল পাঠাতে কত খরচ হবে দেখে নিন</p>
                    </div>
                    <div class="charge_link">
                        <ul>
                            <li class="active" id="inside"> শহরের ভিতর</li>
                            <li id="local">শহরের নিকটবর্তী</li>
                            <li id="outside">শহরের বাইরে</li>
                        </ul>
                    </div>

                    <div class="charge_area" id="inside_item">
                        <ul>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳৬০</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১-২ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳৭৫</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>২-৩ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳৯০</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৩-৪ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১০৫</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৪-৫ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১২০</h2>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="charge_area" id="local_item" style="display: none;">
                        <ul>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১০০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১-২ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১১৫ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>২-৩ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৩০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৩-৪ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৪৫ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৪-৫ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৬০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="charge_area" id="outside_item" style="display: none;">
                        <ul>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৩০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>১-২ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৬০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>২-৩ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳১৯০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৩-৪ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳২২০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="items">
                                    <div class="kg">
                                        <p>৪-৫ কেজি পর্যন্ত</p>
                                    </div>
                                    <div class="tk">
                                        <h2>৳২৫০ + ১% COD</h2>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <div class="bottom text-center">
                        <p>এন্টারপ্রাইজ প্রাইসিং সম্পর্কে জানতে যোগাযোগ করুন আমাদের এজেন্টের সাথে</p>
                        <a href="#">সম্ভাব্য খরচ সম্পর্কে জানুন</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!-- ============ End Calculator Section =============-->

<!-- ============= Start Slider Part =============-->
<section class="slider" id="slider">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <h2>RaftCourier প্রতি গ্রাহকদের ভালোবাসা</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid carousel">
        <div class="owl-carousel owl-theme">
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/1.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/2.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/3.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/4.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/5.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row slide_all_item">
                <div class="col client_item">
                    <div class="head float-right">
                        <h4 class="float-right">SMEs</h4>
                    </div>
                    <div class="text">
                        <p>
                            আমরা RaftCourier দ্রুতগতির ডেলিভারির সমাধান নিয়ে খুবই সন্তুষ্ট।
                            ট্র্যাকিং সুবিধার ফলে আমরা ডেলিভারি সম্পর্কে রিয়েল টাইমে অবগত
                            থাকি এবং ক্রেতাদের বিভিন্ন প্রশ্নের দ্রুত উত্তর দিতে পারি।
                        </p>
                    </div>
                    <div class="bottom">
                        <div class="slide_img">
                            <img class="img-fluid" src="{{asset('public/front/images/slider/6.jpg')}}" alt="one" />
                        </div>
                        <div class="slide_info">
                            <h6>রুবাবা আক্তার</h6>
                            <p>সেনা লুব্রিকেন্টস</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ============= End Slider Part =============-->

<!-- ============= Start Question Section =============-->
<div id="question">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="que_head">
                    <h2>আপনার সকল জিজ্ঞাসা</h2>
                    <p>
                        সর্বাধিক জিজ্ঞাসিত প্রশ্নগুলি দেখুন, যেকোনো প্রয়োজনে আমরা আপনার
                        পাশে আছি
                    </p>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-9">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left " type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            <span><i class="fas fa-hand-point-right"></i></span>
                                            RaftCourier কি?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        RaftCourier একটি প্রযুক্তিনির্ভর লজিস্টিকস কোম্পানি যা
                                        দেশজুড়ে এসএমই প্রোডাক্ট ডেলিভারি সেবা থেকে শুরু করে
                                        ব্যাক্তিগত ডকুমেন্ট, পার্সেল ডেলিভারি, এবং বড় কর্পোরেট
                                        কোম্পানিগুলোকে ইন্ডাস্ট্রিয়াল লজিস্ট্রিকস সেবা প্রদান
                                        করে থাকে।
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            <span><i class="fas fa-hand-point-right"></i></span>
                                            RaftCourier কি কি ধরনের সেবা দিয়ে থাকে?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        যে কোন ব্যবসা বা ব্যক্তিগত প্রয়োজনে ডেলিভারি সংক্রান্ত
                                        সেবা প্রদানের একটি পূর্ণাঙ্গ প্লাটফর্মRaftCourier। পার্সেল
                                        ডেলিভারি, বাল্ক শিপমেন্ট, লাইন হল, ট্রাক ভাড়া,
                                        লোডিং-আনলোডিং, ওয়্যারহাউজ, সার্ভিস হিসেবে লজিস্টিকস
                                        সল্যুশন এবং প্রয়োজন অনুযায়ী কাস্টমাইজড সল্যুশনও দিয়ে
                                        থাকেRaftCourier।
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            <span><i class="fas fa-hand-point-right"></i></span>
                                            RaftCourier ডেলিভারির কভারেজ এরিয়া কি?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        RaftCourier বাংলাদেশের ৬৪ টি জেলায় ডেলিভারি করে। আমাদের
                                        ডেলিভারি এরিয়া জানতে লিঙ্কে ক্লিক করুন
                                        <a href="#">https://raftcourier.com.bd/coverage-area</a>
                                        আপনার নিকটস্থRaftCourier হাবে যোগাযোগ করে আজই ডেলিভারি বুক
                                        করুন।RaftCourier হাবের সম্পূর্ণ লিস্ট দেখতে এখানে ক্লিক
                                        করুন
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
                                            aria-controls="collapseFour">
                                            <span><i class="fas fa-hand-point-right"></i></span>
                                            আপনাদের লজিস্টিকস চার্জ সম্পর্কে জানতে চাই?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        মার্চেন্টদের পার্সেল ডেলিভারিতে আমরা প্রতি কেজি অনুযায়ী
                                        চার্জ করে থাকি l ঢাকার ভেতরে প্রতি কেজি ৬০ টাকা, ঢাকার
                                        আশেপাশে ১০০ এবং ঢাকার বাইরে ১৩০ টাকা থেকে চার্জ শুরু হয়।
                                        ঢাকার আশেপাশে এবং বাইরে প্রতি ডেলিভারিতে ১% ক্যাশ অন
                                        ডেলিভারি চার্জ প্রযোজ্য। ব্যক্তিগত পার্সেল পাঠানোর চার্জ
                                        সম্পর্কে জানতে আমাদের হোম ডেলিভারি এবং হাব ডেলিভারির
                                        প্রাইস চার্টটি দেখুন। এন্টারপ্রাইজ লজিস্টিকসের জন্য
                                        আমাদের দক্ষ সেলস টিম রয়েছে। ০৯১০০০৭৩৩৯ নাম্বারে কল দিয়ে
                                        এ সংক্রান্ত সকল তথ্য পাবেন। অনুগ্রহপূর্বক মনে রাখবেন ওজন
                                        এবং অবস্থানের ভিত্তিতে ডেলিভারি চার্জ পরিবর্তন হতে পারে
                                        l এ বিষয়ে আরও বিস্তারিত জানতে লিঙ্কে ক্লিক করুন এবং
                                        আমাদের প্রাইস সম্পর্কে আরো বিস্তারিত জানুন l
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 text-center ques_faq">
                        <a href="#">
                            <span>See all our FAQs</span>
                            <span> <i class="far fa-arrow-alt-circle-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============= End Question Section =============-->

<!-- ============ Start Partner ===============-->
<div id="sign">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="part_item">
                    <div class="part_left">
                        <h2>RaftCourier পার্টনার হোন আজই</h2>
                        <p>সাইনিং করুন মাত্র দুই মিনিটে</p>
                    </div>
                    <div class="part_right">
                        <a href="#">সাইন-আপ করুন</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ End Partner ===============-->
@endsection
@section('custom-js')
<script type="text/javascript">

/* ******** Cacculator charge list part********* */
    $(document).ready(function () {

        $('#inside').click(function () {
            $("#inside_item").show();
            $("#local_item").hide();
            $("#outside_item").hide();
        });

        $('#local').click(function () {
            $("#inside_item").hide();
            $("#local_item").show();
            $("#outside_item").hide();
        });
        $('#outside').click(function () {
            $("#inside_item").hide();
            $("#local_item").hide();
            $("#outside_item").show();
        });
        $('#close_btn_icon').click(function () {
            $("#charge_hide").hide();
        });

        $('#charge_list_show').click(function () {
            $("#charge_hide").show();
        });
    });
    $(document).on('click', 'ul li', function () {
        $(this).addClass('active').siblings().removeClass('active')
    });
    /* ******** Cacculator charge list part********* */


    function showHidePwd() {
        var input = document.getElementById("password");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye").className = "far fa-eye-slash";
        }
    }


    $(document).ready(function () {
        $('.dropdown a.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });


    jQuery(document).ready(function ($) {
        $("#btn-continue").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();
            var formData = {
                phone: jQuery('#phone_no').val(),

            };
            var state = jQuery('#btn-save').val();
            var type = "POST";
            var todo_id = jQuery('#todo_id').val();
            var ajaxurl = 'signupotp';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data.msg);

                    if (data.msg == 'ok') {
                        $('#signupotp').removeClass('d-none');
                        $('#password').removeClass('d-none');
                        $('#pass_btn').removeClass('d-none');
                        $("#btn-continue").addClass('d-none');
                        $("#btn-submit").removeClass('d-none');
                    } else if (data.msg == 'taken') {
                        showFrontendAlert('error', 'Number already Used');
                    } else {

                    }


                    // jQuery('#myForm').trigger("reset");
                    // jQuery('#formModal').modal('hide')
                },
                error: function (data) {
                    console.log(data.msg);
                }
            });
        });

        //button submit/////////////////////////////////////////////////////////////////////////

        $("#btn-submit").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();
            var formData = {
                phone: jQuery('#phone_no').val(),
                otp: jQuery('#signupotp').val(),
                password: jQuery('#password').val(),

            };
            var state = jQuery('#btn-save').val();
            var type = "POST";
            var todo_id = jQuery('#todo_id').val();
            var ajaxurl = 'checkotp';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data.msg);

                    if (data.msg == 'required') {
                        showFrontendAlert('error', 'all fields are required');
                    } else if (data.msg == 'otpproblem') {
                        showFrontendAlert('error', 'OTP Not Matched');
                    } else if (data.msg == 'saved') {
                        window.location.href = "{{ route('user.login')}}";
                        showFrontendAlert('success', 'Id Created Successfully');
                    } else if (data.msg == 'unsaved') {
                        showFrontendAlert('error', 'password not saved.please recreate');
                    } else {

                    }


                    // jQuery('#myForm').trigger("reset");
                    // jQuery('#formModal').modal('hide')
                },
                error: function (data) {
                    console.log(data.msg);
                }
            });
        });


    });

</script>
@endsection
