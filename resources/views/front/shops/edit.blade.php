@extends('front.raftmaster')
@section('main')


<div id="shop_edit">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="edit_top">
                    <h1>Edit Shop</h1> <span> <a href="{{route('shops.index')}}"><i class="far fa-times-circle"></i></a> </span>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10 col-12">
                <div class="row">
                    <div class="col-12">
                        <form id="basic-form" method="post" action="{{route('shops.update',$shop->id)}}">

                            <input name="_method" type="hidden" value="PATCH">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="shopEmail">Shop Email</label>
                                    <input type="email" required class="form-control" value="{{$shop->shop_email}}" name="email" placeholder="Shop Email" id="shopEmail">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="address">Shop Address</label>
                                    <input type="text" required class="form-control" value="{{$shop->shop_address}}" name="shop_address" placeholder="Shop Address" id="address">
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="shopName">Shop Name</label>
                                    <input type="text" required value="{{$shop->shop_name}}" class="form-control" id="shopName" name="shop_name" placeholder="Shop Name">
                                </div>                                                       
                            </div>    
                            <p><i class="fas fa-exclamation-circle"></i> Pickup locations for this shop cannot be edited here <a href="#">Go to Pickup Location</a> </p>    
                            <div class="form-row justify-content-center">
                                <div class="form-group col-md-6 text-center">
                                <button class="text-center justify-content-center" type="submit">শপ এডিট করুন</button>
                                </div>                                                       
                            </div>         
                           
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>














<div class="row mt-3 p-5 " style="display: none;">
    <div class="col-md-8 offset-md-2 mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <h4 style="text-transform:none">Add Your Business Information</h3>
            <p>If you have more than one business, You can create multiple shop later.</p>
            <form role="form" id="quickForm" method="post" action="{{route('shops.store')}}">
                @csrf
                <div class="row">
                    <div class="col-12">

                    </div>



                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Reason for opening an account?
                                </label>
                                <select class="form-control" id="exampleFormControlSelect1" name="s_type" required>
                                    <option disabled selected value>choose an option</option>
                                    <option value="business">Business Use</option>
                                    <option value="personal">Personal use</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlInput1">Shop name</label>
                                <input type="text" class="form-control" value="shop_name" name="s_name" required id=""
                                    placeholder="shop name here..">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlInput1">Shop address</label>
                                <input type="text" class="form-control" name="s_address" required id=""
                                    placeholder="shop address here..">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Pickup address</label>
                                <input type="text" class="form-control" name="p_address" required id=""
                                    placeholder="detail pick up address">
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Shop email</label>
                                <input type="email" class="form-control" id="s_email" name="s_email" required
                                    placeholder="name@example.com">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlInput1">Product type</label>
                                <select name="p_cat" class="form-control" id="p_cat" style="width: 100%;" required>
                                    <option disabled selected value>choose an option</option>
                                    @foreach($cats as $key=> $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Product subcategory type</label>
                                <select name="p_subcat" class="form-control " id="p_subcat" style="width: 100%;"
                                    required>
                                    <option disabled selected value>choose an option</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Pickup Phone</label>
                                <input type="text" class="form-control" name="p_phone" required id=""
                                    placeholder="pick up mobile">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Pickup Area
                        </label>
                    </div>
                    <div class="row">

                        <div class="col">
                            <select class="form-control" id="p_dist" name="p_dist" required>
                                <option disabled selected value>--select District--</option>
                                @foreach($dist as $key=> $dist)
                                <option value="{{$dist->id}}">{{$dist->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control" id="p_subcity" name="p_subcity" required>
                                <option disabled selected value>--select City--</option>

                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control" id="p_subarea" name="p_subarea" required>
                                <option disabled selected value>--Select Area--</option>

                            </select>
                        </div>
                    </div>
                </div>

                <input type="hidden" class="form-control" id="p_full_area" name="p_full_area" placeholder="">

                <button type="submit" class="btn btn-primary">create Shop</button>
            </form>

    </div>

</div>

@endsection

@section('custom-js')
<script type="text/javascript">
    $('form').on('submit', function (e) {
        // validation code here

        $distvalue = $("#p_dist option:selected").text();
        $cityvalue = $("#p_subcity option:selected").text();
        $areavalue = $("#p_subarea option:selected").text();
        $('#p_full_area').val($distvalue + '>' + $cityvalue + '>' + $areavalue);

    });

    $('#p_cat').on('change', function () {
        $id = this.value;
        show_subcat($id);
    });
    $('#p_dist').on('change', function () {
        $id = this.value;
        show_subcity($id);
    });
    $('#p_subcity').on('change', function () {
        $id = this.value;
        show_subarea($id);
    });


    function show_subcat($id) {
        $.get("{{URL::to('shopsetup/subcat')}}/" + $id, function (data) {
            $('#p_subcat').html(data);

        });
    }

    function show_subcity($id) {
        $.get("{{URL::to('shopsetup/subcity')}}/" + $id, function (data) {
            $('#p_subcity').html(data);

        });
    }

    function show_subarea($id) {
        $.get("{{URL::to('shopsetup/subarea')}}/" + $id, function (data) {
            $('#p_subarea').html(data);

        });
    }

</script>
@endsection
