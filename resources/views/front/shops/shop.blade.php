@extends('front.raftmaster') @section('main')


<div id="shop_part">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 text-center head">
                <h1>Add your information</h1>
                <p>Please tell a bit about you and your business</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-12">
                <form method="post" id="basic-form" action="{{route('shops.store')}}">@csrf
                    <div class="top">
                        <div class="form-row">
                            <div class="col-12 justify-content-start">
                                <h3>Personal Information</h3>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label for="inputFullName">Full Name</label>
                                <input type="text" name="name" required class="form-control" id="inputFullName"
                                    placeholder="Full Name"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label for="inputState">Reason for opening an
                                    account</label><select id="inputState" required name="s_type" class="form-control">
                                    <option selected>Choose account type...</option>
                                    <option value="business">Business use</option>
                                    <option value="personal">Personal use</option>
                                </select></div>
                            <div class="form-group col-md-6"><label for="Referred">Referred By (Optional)</label><input
                                    type="text" name="full_name" class="form-control" id="Referred"
                                    placeholder="Referred By"></div>
                        </div>
                    </div>
                    <div class="middle">
                        <div class="form-row">
                            <div class="col-12 justify-content-start">
                                <h3>Shop Information</h3>
                                <p>If you have more than one business,
                                    you can create multiple shops later</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label for="shopName">Shop Name</label>
                                <input type="text" name="s_name" class="form-control" id="shopName"
                                    placeholder="Shop Name"></div>
                            <div class="form-group col-md-6"><label for="shopEmail">Shop Email</label><input
                                    type="email" required name="s_email" class="form-control" id="shopEmail"
                                    placeholder="Email Address"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label for="shopAddress">Shop Address</label><input
                                    type="text" required name="s_address" class="form-control" id="shopAddress"
                                    placeholder="Shop Address"></div>
                            <div class="form-group col-md-6">
                                <label for="productType">Product type</label>
                                <select id="p_cat" name="p_cat" class="form-control">
                                    <option disabled selected value>choose an option</option>
                                    @foreach($cats as $key=>$cat)
                                    <option value="{{$cat->id}}"> {{$cat->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label for="productCategory">Product sub category
                                    type</label><select id="p_subcat" name="p_subcat" class="form-control">
                                    <option selected>Choose sub category type...</option>

                                </select></div>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="form-row">
                            <div class="col-12 justify-content-start">
                                <h3>Pickup Information</h3>
                                <p>If you have more than one pickup location,
                                    additional pickup can be created later</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pickupAddress">Pickup Address</label>
                                <input type="text" name="p_address" required class="form-control" id="pickupAddress"
                                    placeholder="Pickup Address">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pickupPhone">Pickup phone number</label>
                                <input type="number" name="p_phone" required class="form-control" id="pickupPhone"
                                    placeholder="Pickup phone number">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="Division">Pickup Division</label>
                                <select id="p_dist" name="p_dist" required class="form-control">
                                    <option disabled selected value>--select District--</option>
                                    @foreach ($dist as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Dristrict">Pickup City</label>
                                <select id="p_subcity" name="p_subcity" required class="form-control">
                                    <option selected>Choose city...</option>

                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Thana">Pickup Area</label>
                                <select id="p_subarea" class="form-control">
                                    <option selected>Choose Area...</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-12 text-center">
                                <input type="hidden" class="form-control" id="p_full_area" name="p_full_area"
                                    placeholder="">
                                <button type="submit">Next</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3 p-5 raju_dada" style="display: none;">
    <div class="col-md-8 offset-md-2 mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <h4 style="text-transform:none">Add Your Business Information</h3>
            <p>If you have more than one business,
                You can create multiple shop later.</p>
            <form role="form" id="quickForm" method="post" action="{{route('shops.store')}}">@csrf <div class="row">
                    <div class="col-12"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group"><label for="exampleInputPassword1">Reason for opening an account?
                                </label><select class="form-control" id="exampleFormControlSelect1" name="s_type"
                                    required>
                                    <option disabled selected value>choose an option</option>
                                    <option value="business">Business Use</option>
                                    <option value="personal">Personal use</option>
                                </select></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Shop name</label><input
                                    type="text" class="form-control" name="s_name" required id=""
                                    placeholder="shop name here.."></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Shop address</label><input
                                    type="text" class="form-control" name="s_address" required id=""
                                    placeholder="shop address here.."></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Pickup address</label><input
                                    type="text" class="form-control" name="p_address" required id=""
                                    placeholder="detail pick up address"></div>
                        </div>
                        <div class="col-6">
                            <div class="form-group"><label for="exampleFormControlInput1">Shop email</label><input
                                    type="email" class="form-control" id="s_email" name="s_email" required
                                    placeholder="name@example.com"></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Product type</label><select
                                    name="p_cat" class="form-control" id="p_cat" style="width: 100%;" required>
                                    <option disabled selected value>choose an option</option>
                                    @foreach($cats as $key=>$cat)
                                    <option value="{{$cat->id}}"> {{$cat->name}}
                                    </option>
                                    @endforeach
                                </select></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Product subcategory
                                    type</label><select name="p_subcat" class="form-control " id="p_subcat"
                                    style="width: 100%;" required>
                                    <option disabled selected value>choose an option</option>
                                </select></div>
                            <div class="form-group"><label for="exampleFormControlInput1">Pickup Phone</label><input
                                    type="text" class="form-control" name="p_phone" required id=""
                                    placeholder="pick up mobile"></div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group"><label for="exampleInputPassword1">Select Pickup Area </label></div>
                    <div class="row">
                        <div class="col"><select class="form-control" id="p_dist" name="p_dist" required>
                                <option disabled selected value>--select District--</option>@foreach($dist as $key=>
                                $dist) <option value="{{$dist->id}}"> {
                                    {
                                    $dist->name
                                    }
                                    }

                                </option>@endforeach
                            </select></div>
                        <div class="col"><select class="form-control" id="p_subcity" name="p_subcity" required>
                                <option disabled selected value>--select City--</option>
                            </select></div>
                        <div class="col"><select class="form-control" id="p_subarea" name="p_subarea" required>
                                <option disabled selected value>--Select Area--</option>
                            </select></div>
                    </div>
                </div>
                <input type="hidden" class="form-control" id="p_full_area" name="p_full_area" placeholder="">
                <button type="submit" class="btn btn-primary">create Shop</button>
            </form>
    </div>
</div>



@endsection @section('custom-js')



<script type="text/javascript">
    $('form').on('submit', function (e) {
            // validation code here

            $distvalue = $("#p_dist option:selected").text();
            $cityvalue = $("#p_subcity option:selected").text();
            $areavalue = $("#p_subarea option:selected").text();
            $('#p_full_area').val($distvalue + '>' + $cityvalue + '>' + $areavalue);

        }

    );

    $('#p_cat').on('change', function () {
            $id = this.value;
            show_subcat($id);
        }

    );

    $('#p_dist').on('change', function () {
            $id = this.value;
            show_subcity($id);
        }

    );

    $('#p_subcity').on('change', function () {
            $id = this.value;
            show_subarea($id);
        }

    );


    function show_subcat($id) {
        $.get("{{URL::to('shopsetup/subcat')}}/" + $id, function (data) {
                $('#p_subcat').html(data);

            }

        );
    }

    function show_subcity($id) {
        $.get("{{URL::to('shopsetup/subcity')}}/" + $id, function (data) {
                $('#p_subcity').html(data);

            }

        );
    }

    function show_subarea($id) {
        $.get("{{URL::to('shopsetup/subarea')}}/" + $id, function (data) {
                $('#p_subarea').html(data);

            }

        );
    }

</script>@endsection
