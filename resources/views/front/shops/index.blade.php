@extends('front.raftmaster')
@section('main')
{{-- your code here --}}






{{-- your code here --}}

<div id="shop_index">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul>
          <li class="first">
            <a href="{{ route('user.shopsetup')}}">
            <div class="add_shop">
              <div class="top">
              <i class="fas fa-plus-circle"></i>
              </div>
              <div class="bottom">
                <p>একটি নতুন শপ যুক্ত করুন</p>
              </div>
            </div>
            </a>
            
          </li>
          @foreach($shops as $key=>$shop)

          <li>
            <a href="{{route('shops.activate',$shop->id)}}">
              <div class="shop_item">
                <div class="item_top">
                  <h4>{{$shop->shop_name}}</h4>
                  <p>{{$shop->shop_address}}</p>
                  <p>{{$shop->pickup_phone}}</p>
                </div>
                <div class="item_bottom">
                  @if($shop->status =='1')
                  <div class="left">
                  <i class="fas fa-check"></i>
                  </div>
                  @endif
                  <div class="right">
                  <a  href="{{route('shops.edit',$shop->id)}}"><i class="fas fa-edit"></i></a>
                  </div>
                </div>
              </div>
            </a>
              
          </li>
          @endforeach
         
        </ul>
      </div>
    </div>
  </div>
</div>















<div class="row mt-5 p-5 " style="display:none;">
    @foreach($shops as $key=>$shop)
    <div class="col-sm-3">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title c-b" >{{$shop->shop_name}}</h5>
          <p class="card-text c-b">{{$shop->shop_address}}</p>
          @if($shop->status =='1')
          <a href="{{route('shops.activate',$shop->id)}}" class="btn-sm btn-success">Working..</a>
          @else
          <a href="{{route('shops.activate',$shop->id)}}" class="btn-sm btn-primary">Activate</a>
          @endif
          <a href="{{route('shops.edit',$shop->id)}}" class="btn-sm btn-warning">edit</a>
        </div>
      </div>
    </div>
    @endforeach
      <div class="col-sm-3">
        <div class="card">
          <div class="card-body">
           
            <p class="card-text c-b">Add new shop to grow your business</p>
            <p class="card-text c-b"></p>
            <a href="{{ route('user.shopsetup')}}"  class="btn-lg btn-danger">Add More</a> 
          </div>
        </div>
      </div>
  </div> 
@endsection