@extends('front.raftmaster')
@section('main')

<div id="payment_pdf">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="pdf">
                    <div class="pdf_top">
                        <div class="logo">
                            <img src="{{asset('public/front/images/logo.png')}}" alt="logo"> <span class="logo"> RaftCourier</span>
                        </div>
                        <div class="bd">
                            <p>গণজাতী বাংলােদশ সরকার</p>
                           
                            <p>নবি1ত বি2র নাম <span>: REDX Logistics Limited</span> </p>
                            
                            <p>চালানপ3 ইসুর কানা <span>: 50, Lake Circus, Kalabagan, Dhaka, PO : 1205, Bangladesh</span></p>
                        </div>
                        <div class="musok">
                            <p><span>মূসক ৬.৩</span></p>
                        </div>
                    </div>
                    <div class="pdf_customer">
                        <div class="cus_left">
                            <p>তার নাম <span>: E-Raft</span> </p>
                            <p>তার িবআইএন <span>: 8801841337523</span> </p>
                            <p>তার কানা <span>: Mahtab Center (6th Flr), 177 Shahid Syed Nazrul Islam Sarani, Bijoy Nagar,
                                Dhaka.</span> </p>
                            <p>সরবরােহর গবল <span> : N/A</span></p>
                            <p>যানবাহেনর কৃিত ও নাার <span>: N/A</span> </p>
                        </div>
                        <div class="cus_right">
                            <p>চালানপ3 নর <span>: 1</span> </p>
                            <p>ইসুর তািরখ <span> : October 12th</span></p>
                            <p>ইসুর সময় <span>: 12:58 pm</span> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="pdf_table_top">
                    <div class="left">
                        <p><span>Customer Name : E-Raft</span> </p>
                        <p><span> Phone : 8801841337523</span></p>
                    </div>
                    <div class="middle">
                        <p><span>Created At : October 12th 2021</span> </p>
                    </div>
                    <div class="right">
                        <h5>#222795</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="pdf_table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Parcel Id</th>
                                <th scope="col">Merchant Invoice Id</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Customer Phone</th>
                                <th scope="col">Area</th>
                                <th scope="col">Status</th>
                                <th scope="col">Cash Collected</th>
                                <th scope="col">Delivery Charge</th>
                                <th scope="col">COD Charge</th>
                                <th scope="col">Return Charge</th>
                                <th scope="col">Promo Discount</th>
                                <th scope="col">Total Delivery Charge</th>
                                <th scope="col">Vat</th>
                                <th scope="col">Value Without Vat</th>
                                <th scope="col">Vat Amount</th>
                                <th scope="col">Adjustment Amount</th>
                                <th scope="col">Cash Paid</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>20A7A4SAZGQD</td>
                                <td>04/07/2020-010</td>
                                <td>Sumona Jannat</td>
                                <td>01956654471</td>
                                <td>Khulna Sadar</td>
                                <td>delivered</td>
                                <td>1070</td>
                                <td>130</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>130.00</td>
                                <td>15%</td>
                                <td>113.04</td>
                                <td>16.96</td>
                                <td>0</td>
                                <td>940.00</td>

                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>690</td>
                                <td>190</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>190</td>
                                <td>15%</td>
                                <td>165.21</td>
                                <td>24.79</td>
                                <td></td>
                                <td>880.00</td>
                            </tr>
                            <tr>
                                <td>Credit Used</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Total Cash Paid</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>880.00</td>
                            </tr>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="pdf_bottom">
                    <p>িত>ান কতৃপে6র দািয়?া@ বি2র নামঃ <span>Kamrul Hassan Siddique</span> </p>
                    <p>পদবীঃ <span>Executive</span> </p>
                    <p>* সকল কার কর বতীত মূল</p>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
