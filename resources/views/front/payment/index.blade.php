@extends('front.raftmaster')
@section('main')


<div id="payment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="payment_top">
                    <div class="left">
                        <h1>Payments Summary</h1>
                    </div>
                    <div class="right">
                        <ul>
                            <li>
                                <input type="text" class="form-control" name="invoice" placeholder="Invoice No.">
                            </li>
                            <li>
                                <label for="date"> Start Date ~ End Date :
                                    <input type="text" name="daterange" placeholder="start Date - End Date" id="date" />
                                    <i class="far fa-calendar-alt"></i>
                                </label>
                            </li>
                            <li>
                                <a href="#">Search</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="payment_table" style="overflow-x:auto;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Invoice ID</th>
                                <th scope="col">Invoice Date</th>
                                <th scope="col">Cash Collection (Tk.)</th>
                                <th scope="col">Delivery Charge (Tk.)</th>
                                <th scope="col">Raft Credits Used (Tk.)</th>
                                <th scope="col">COD Charge (Tk.)</th>
                                <th scope="col">Return Charge (Tk.)</th>
                                <th scope="col">Advance payment repayment 0% (Tk.)</th>
                                <th scope="col">Total Adjustment Amount (Tk.)</th>
                                <th scope="col">Amount Paid Out (Tk.)</th>
                                <th scope="col">Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $key=>$payment )
                            <tr>
                                <td>{{$payment->id}}</td>
                                <td>{{$payment->created_at}}</td>
                                <td>{{$payment->cash_collection}}</td>
                                <td>{{$payment->delivery_charge}}</td>
                                <td>{{$payment->cod_charge}}</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>
                                    <span>{{$payment->total_payable}}</span>
                                </td>
                                <td>
                                    <a href="#" title="Download Invoice"><i class="far fa-file-pdf"></i></a>
                                    <a href="#" title="Download Musok"><i class="far fa-file-pdf"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>









@endsection

@section('custom-js')
<script>
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
    });

</script>
@endsection
