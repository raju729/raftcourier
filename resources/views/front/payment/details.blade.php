@extends('front.raftmaster')
@section('main')


<div id="payment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="payment_top">
                    <div class="left">
                        <h1>Payment Details</h1>
                    </div>
                </div>
                <div class="pay_id">
                    <div class="left">
                        <p>Invoice ID: <span> 222795</span></p>
                        <p>Jul 16, 2020</p>
                    </div>
                    <div class="right">
                        <a href="#" title="Download Invoice"><i class="far fa-file-pdf"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="payment_table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Parcel ID(s)</th>
                                <th scope="col">Date</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Status</th>
                                <th scope="col">Cash Collection (Tk.)</th>
                                <th scope="col">Delivery Charge (Tk.)</th>
                                <th scope="col">COD Charge (Tk.)</th>
                                <th scope="col">Return Charge (Tk.)</th>
                                <th scope="col">Amount Paid (Tk.)</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>20A7A2THYY0B</td>
                                <td>07/02/2020</td>
                                <td>Siftah Islam Tanisha</td>
                                <td>01923611456</td>
                                <td class="returned">Returned</td>
                                <td>0</td>
                                <td>60</td>
                                <td>0</td>
                                <td>0</td>
                                <td>
                                    <span>880.0</span>
                                </td>
                            </tr>
                            <tr>
                                <td>20A7A2THYY0B</td>
                                <td>07/02/2020</td>
                                <td>Siftah Islam Tanisha</td>
                                <td>01923611456</td>
                                <td class="deliver">Delivered</td>
                                <td>0</td>
                                <td>60</td>
                                <td>0</td>
                                <td>0</td>
                                <td>
                                    <span>880.0</span>
                                </td>
                            </tr>
                            <tr class="bold_border">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="bold_txt">Total Paid Out (Tk.)</td>
                                <td class="bold_txt">880.0</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
