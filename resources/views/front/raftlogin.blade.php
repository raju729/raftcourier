@extends('front.raftmaster')
@section('main')

<!-- ============ Start Signin Section ================-->
<div id="signin">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-7 col-12">
                <form id="basic-form" action="{{ route('user.authenticate') }}" class="needs-validation" method="post" autocomplete="off">
                    @csrf
                    <div id="sign_hide" class="sign_form">
                        <div class="sign_top">
                            <h1>Welcome!</h1>
                            <p>Enter your phone number to login or create new <br> account and hit “Continue” to get an OTP.
                            </p>
                        </div>
                       
                        <div class="sign_form_part">
                           
                            <div class="form-label-group input-group num_design">
                                <div class="input-group-append ">
                                    <span><img src="{{asset('public/front/images/bangladesh-flag.png')}}" alt="bd" />
                                        +88</span>
                                </div>
                                <input type="text" autocomplete="false" required name="phone" placeholder="ফোন নম্বর #EX:01710000000"/>
                            </div>
                            <div class="left">
                            </div>
                            
                            <div class="right">
                            </div>
                        </div>
                        <div class="middle">

                            <div class="form-label-group input-group eye_design">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password"
                                    required>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i id="eye" class="far fa-eye-slash" onclick="showHidePwd();"></i>
                                    </span>
                                </div>
                            </div>


                        </div>
                        <div>
                            <p class="signin_hide"><a href="#">Forgot Password</a></p>
                        </div>
                        <div class="sign_btn">
                            <button type="submit" class="sign_btn">সাইন-In</button>

                        </div>
                        <div class="sign_bottom pb-5">
                            <p class="first">By logging in you agree to our <a
                                    href="https://www.youtube.com/results?search_query=javascript+button+onclick+add+div+and+remove+div">
                                    Terms & Conditions </a> </p>
                        </div>
                    </div>
                </form>

                <div id="forgot" class="sign_form" style="display: none;">
                    <div class="sign_top">
                        <h1>Forgot password</h1>
                        <p>Enter your phone number you have registered with <br> and we will send you an OTP to reset it
                        </p>
                    </div>
                    <div class="sign_form_part">
                        <div class="left">
                            <span><img src="{{asset('public/front/images/bangladesh-flag.png')}}" alt="bd" />
                                +880</span>
                        </div>
                        <div class="right">
                            <input type="text" placeholder="ফোন নম্বর" name="phone" />
                        </div>
                    </div>

                    <div class="sign_btn">
                        <button type="button" class="sign_btn">Continue</button>

                    </div>
                    <div class="sign_bottom pb-5">
                        <p class="first">By logging in you agree to our <a
                                href="https://www.youtube.com/results?search_query=javascript+button+onclick+add+div+and+remove+div">
                                Terms & Conditions </a> </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-12">
                <div class="sign_slider">
                    <div class="container carousel sign_slider">
                        <div class="owl-carousel owl-theme">
                            <div class="row justify-content-center">
                                <div class="col-12 text-center">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/parcel-tracking.png')}}"
                                            alt="one" />
                                    </div>

                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/payment.png')}}"
                                            alt="one" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/issue-raise.png')}}"
                                            alt="one" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12 align-items-center">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/parcel-tracking.png')}}"
                                            alt="one" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/payment.png')}}"
                                            alt="one" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="slide_img">
                                        <img class="img-fluid"
                                            src="{{asset('public/front/images/signin_slider/issue-raise.png')}}"
                                            alt="one" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-js')
<script>

    function showHidePwd() {
        var input = document.getElementById("password");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye").className = "far fa-eye-slash";
        }
    }


    $('document').ready(function () {
        $('.signin_hide').click(function () {
            $("#forgot").show();
            $("#sign_hide").hide();
        });
    });

</script>

<!-- ============ End Signin Section ================-->
@endsection
