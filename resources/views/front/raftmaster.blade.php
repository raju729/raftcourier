<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">

    <title>RaftCourier</title>
<!--
Elegance Template
https://templatemo.com/tm-528-elegance
-->
    <!-- Additional CSS Files -->

    @include('front.includes.css')
    
    </head>
    
    <body>

            <!-- ========= Start Navbar Section =========-->
            @include('front.includes.raftheader')
      <!-- ========= End Navbar Section =========-->
       <!-- ========= start main body Section =========--> 
            @yield('main')
      <!-- ========= end main body Section =========--> 
  
      <!-- ============= Start Footer Section =================-->
      @include('front.includes.raftfooter')
      <!-- ============= End Footer Section =================-->
  
      <!--====== Start Top to Bootom Scroll button=====-->
      <i class="fas fa-arrow-circle-up" id="bottom-to-top"></i>
      <!--====== End Top to Bootom Scroll button=====-->










      @include('front.includes.js')
    <script type="text/javascript">
        function showFrontendAlert(type, message){
            // if(type == 'danger'){
            //     type = 'error';
            // }
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: type,
            title: message
            })
        }

</script>
@yield('custom-js')
    @include('sweetalert::alert')


  </body>
</html>