<footer>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-4 col-sm-6 col-12 text-sm-center small">
          <div class="one">
            <a href="#">
              <span>
                <img
                  class="img-fluid logo"
                  src="{{asset('public/front/images/logo.png')}}"
                  alt="logo"
              /></span>
              <span class="foot_text"> RaftCourier</span>
            </a>

            <p>দ্রুততম সল্যুশনের জন্য RaftCourier অ্যাপটি ডাউনলোড করুন</p>
            <a href="#">
              <img
                class="img-fluid"
                src="{{asset('public/front/images/google-play-badge.svg')}}"
                alt="app"
              />
            </a>
            

          </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-12 text-sm-center small phn_hide">
          <div class="foot_link">
            <h4>গুরুত্বপূর্ণ লিংক</h4>
            <div class="link_list">
              <p><a href="#">সি২সি</a></p>
              <p><a href="#">এন্টারপ্রাইজ</a></p>
              <p><a href="#"> কভারেজ এরিয়া</a></p>
              <p><a href="#"> প্রাইভেসী পলিসি</a></p>
              <p><a href="#"> FAQs</a></p>
              
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-12 text-sm-center small phn_hide">
          <h4>যোগাযোগ</h4>
          <div class="add_list">
            <p>
              Mahtab Center 177 Shahid Syed Nazrul Islam Sharani, Bijoy Nagar
              Road, Dhaka 1000
            </p>
            <p>02-8391778</p>
            <p>E-mail : info@deskbd.com</p>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-12 text-sm-center small">
          <h4>সংযুক্ত হন</h4>
          <div class="foot_social">
            <ul>
              <li>
                <a href="https://www.facebook.com/raftcourier"><i class="fab fa-facebook-f"></i></a>
              </li>
              <li>
                <a href="https://www.linkedin.com/company/raft-courier"><i class="fab fa-linkedin-in"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-youtube"></i></a>
              </li>
            </ul>
          </div>
          <div class="rider_btn">
          <a href="{{route('rider.login')}}">Login as a Rider</a>
          </div>
        </div>
      </div>
    </div>
  </footer>