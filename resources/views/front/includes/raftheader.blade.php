<section class="navbar_sec">

    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{route('user.home')}}">
            <img class="img-fluid" src="{{asset('public/front/images/raft_logo.jpg')}}" alt="logo" />
        </a>
        <form class="form-inline my-2 my-lg-0 lg_track_hide">
            <div class="small_track">
                <input class="form-control" type="search" placeholder="পার্সেল ID লিখুন" aria-label="Search">
                <button type="submit">Track</button>
            </div>

        </form>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="toggler-icon top-bar"></span>
            <span class="toggler-icon middle-bar"></span>
            <span class="toggler-icon bottom-bar"></span>
        </button>
        <div class="collapse navbar-collapse left_part" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @if(Auth::check() && (Auth::user()->user_type == 'User'))
                <form class="form-inline my-2 my-lg-0 phone_hide_track" method="post" action="{{route('user.track.submit')}}">
                    @csrf
                    <input class="form-control" name="track" type="search" placeholder="পার্সেল ID লিখুন" aria-label="Search">
                    <button type="submit">Track Parcel</button>
                </form>
                @else
                <li class="nav-item" style="">
                    <a class="nav-link" href="#">এন্টারপ্রাইজ</a>
                </li>
                <li class="nav-item" style="">
                    <a class="nav-link" href="#">সি২সি</a>
                </li>
                @endif


            </ul>
            <div class="collapse navbar-collapse right_part " id="navbarSupportedContent">
                @if(Auth::check() && (Auth::user()->user_type == 'User'))
                @if(shop_count()>0)

                <ul class="navbar-nav ">
                    <li class="nav-item small_hide">
                        <a class="nav-link" href="{{route('merchant.dashboard')}}">Dashboard</a>
                    </li>
                    <li class="nav-item small_hide">
                        <a class="nav-link" href="{{route('parcels.index')}}">Parcel</a>
                    </li>
                    <li class="nav-item small_hide">
                        <a class="nav-link" href="{{route('payments.index')}}">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Coupon</a>
                    </li>
                    <li class="nav-item btn_link small_hide">
                        <a class="nav-link" href="{{route('parcels.create')}}"> Create Parcel </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{check_shop()}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('shops.index')}}">My Shops</a>
                            <a class="dropdown-item" href="{{route('pickups.index')}}">Pickup Location</a>
                            <a class="dropdown-item" href="{{route('paymethods.index')}}">My Payment Details</a>
                            <a class="dropdown-item" href="{{route('user.notifications')}}">Notification Settings</a>
                            <a class="dropdown-item" href="{{route('user.coverage')}}">Coverage Area</a>
                            <a class="dropdown-item" href="{{route('user.changepass')}}">Change Password</a>
                            <a class="dropdown-item" href="{{route('user.faq')}}">Faq</a>
                            <a class="dropdown-item" href="{{route('user.logout')}}">Logout</a>
                        </div>
                    </li>

                </ul>
                @endif

                @else
                <ul class="navbar-nav ">
                    <li class="nav-item phone">
                        <a class="nav-link" href="#">
                            <i class="fas fa-phone-alt"></i>
                            <span class="call"> কল করুন</span>
                            <span class="num">০৯৬১০০০৭৩৩৯</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('user.login')}}">
                            <button type="submit">লগ ইন</button>
                        </a>
                    </li>
                    <li class="nav-item lang">
                        <a class="nav-link" href="#">
                            <i class="fas fa-globe"></i>
                            <span> EN </span>
                        </a>
                    </li>
                </ul>
                @endif

            </div>
        </div>

    </nav>
    @if(Auth::check() && (Auth::user()->user_type == 'User'))
    @if( shop_count()>1 && count_pay_method()<1) <div class="banner_bor">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_item">
                        <div class="left">
                            <p><i class="fas fa-bullhorn"></i> পেমেন্ট পেতে আপনার ব্যাংক অথবা বিকাশ আকাউন্ট এর তথ্য
                                অ্যাড
                                করুন</p>
                        </div>
                        <div class="right">
                            <a href="{{route('paymethods.create')}}">Add Payment Method</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        @endif
        @endif


        <div class="fixed_footer">
            <ul>
                <li><a href="{{route('merchant.dashboard')}}">Dashboard</a></li>
                <li><a href="{{route('parcels.create')}}">Create Parcel</a></li>
                <li><a href="{{route('parcels.index')}}">Parcel</a></li>
                <li><a href="{{route('payments.index')}}">Payments</a></li>
            </ul>
        </div>


</section>
