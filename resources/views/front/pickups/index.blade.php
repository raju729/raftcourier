@extends('front.raftmaster')
@section('main')



<div id="shop_index">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul>
          <li class="first">
            <a href="javascript:void(0)" id="basic-form" onclick="show_add_category_modal('{{ route('pickups.create')}} ')">
            <div class="add_shop">
              <div class="top">
              <i class="fas fa-plus-circle"></i>
              </div>
              <div class="bottom">
                <p>একটি নতুন পিকআপ লোকেশান যুক্ত করুন</p>
              </div>
            </div>
            </a>   
          </li>
        
         @foreach ($pickups as $pickup)
         <li>
          <a href="#">
            <div class="shop_item">
              <div class="item_top">
                <h4>{{$pickup->name}}</h4>
                <p>{{$pickup->area_id}}</p>
                <p>{{$pickup->address}}</p>
              </div>
              <div class="item_bottom">             
                <div class="left">
                
                </div>         
                <div class="right">
                <a href="javascript:void(0)" onclick="show_add_category_modal('{{ route('pickups.edit',1)}} ')"><i class="fas fa-edit"></i></a>
                </div>
              </div>
            </div>
          </a>        
        </li> 
         @endforeach
           
        </ul>
      </div>
    </div>
  </div>
</div>


  <!-- Modal -->
  <div class="modal fade" id="raft_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div id="modal_content" class="modal-content">
     
      </div>
    </div>
  </div>
@endsection


@section('custom-js')
<script type="text/javascript">
    function show_add_category_modal($url){
       // $('#raft_modal').modal('show',{backdrop:'static'});
           $.get($url, function(data){

               $('#modal_content').html(data);
               $('#raft_modal').modal('show',{backdrop:'static'});
            //    alert('takids');
           });
       }

  
</script>


@endsection