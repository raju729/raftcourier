<div id="pickup_edit">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-sm-8 col-12">
                <div class="top">
                    <div class="left">
                        <p>Add new pickup location</p>
                    </div>
                    <div class="right">
                        <i class="fas fa-times" ></i>
                    </div>
                </div>
                <div class="bottom">
                    <form id="basic-form" action="{{route('pickups.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="pickup_name">Pickup name</label>
                            <input type="text" required class="form-control" id="pickup_name" name="name"
                                placeholder="Example input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_add">Pickup address</label>
                            <input type="text" required class="form-control" id="pickup_add" name="address"
                                placeholder="Another input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_area">Pickup Area</label>
                            <select class="form-control" name="area" id="">
                                <option value="1">chokbajar</option>
                                <option value="2">Mohammadpur</option>
                            </select>
                         
                        </div>
                        <div class="form-group">
                            <label for="pickup_phn">Pickup Phone</label>
                            <input type="number" required class="form-control" id="pickup_phn" name="phone"
                                placeholder="Another input placeholder">
                        </div>
                        <div class="form-group text-center">
                            <button type="reset">Back</button>
                            <button type="submit" onclick="pickupValidation()">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@section('custom-js')
<script>
    function pickupValidation() {

        $("#basic-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                phone: {
                    required: true,
                    number: true,
                    maxlength: 11,
                    minlength: 11
                },
                age: {
                    required: true,
                    number: true,
                    min: 18
                },
                email: {
                    required: true,
                    email: true
                },
                weight: {
                    required: {
                        depends: function (elem) {
                            return $("#age").val() > 50
                        }
                    },
                    number: true,
                    min: 0
                }
            },
            messages: {
                name: {
                    minlength: "Name should be at least 3 characters"
                },
                phone: {
                    required: "Please enter your number",
                    number: "Please enter a numerical value",
                    min: "At least 11 digit"
                },
                age: {
                    required: "Please enter your age",
                    number: "Please enter a numerical value",
                    min: "You must be at least 18 years old"
                },
                email: {
                    email: "The email should be in the format: abc@domain.tld"
                },
                weight: {
                    required: "People with age over 50 have to enter their weight",
                    number: "Please enter your weight as a numerical value"
                }
            }
        });

    }





</script>
@endsection
