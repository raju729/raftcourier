


<div id="pickup_edit">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-10">
                <div class="top">
                    <div class="left">
                        <p>Edit pickup location</p>
                    </div>
                    <div class="right">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <div class="bottom">
                    <form id="basic-form" action="" method="post">
                        <div class="form-group">
                            <label for="pickup_name">Pickup name</label>
                            <input type="text" required class="form-control" id="pickup_name"
                               name="name" placeholder="Example input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_add">Pickup address</label>
                            <input type="text" required class="form-control" id="pickup_add"
                               name="address" placeholder="Another input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_area">Pickup Area</label>
                            <input type="text" required class="form-control" id="pickup_area"
                               name="address" placeholder="Example input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_phn">Pickup Phone</label>
                            <input type="number" required class="form-control" id="pickup_phn"
                               name="phone" placeholder="Another input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="pickup_loc">Pickup Location Status</label>
                            <input list="browsers" class="form-control" name="browser" id="browser">
                            <datalist id="browsers">
                                <option value="Active">
                                <option value="Inactive">                         
                            </datalist>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit">Back</button>
                            <button type="submit">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



