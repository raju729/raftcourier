@extends('front.raftmaster')
@section('main')

{{-- <div class="modal fade show" id="rafts_modal"  role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div id="modal_content" class="modal-content">
       
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div> --}}
<div id="parcel_table">
<div class="heading">
                    <h1>Search By</h1>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <select id="inputState" class="form-control">
                                <option selected>All Pickup Location</option>
                                <option>...</option>
                            </select>
                        </li>
                        <li>
                            <input class="form-control" type="text" name="tracking_id" placeholder="Trackind ID">
                        </li>
                        <li>
                            <input class="form-control" type="text" name="invoice_id" placeholder="Shop Invoice ID">
                        </li>
                        <li>
                            <input class="form-control" type="number" name="phone" placeholder="Phone Number">
                        </li>
                        <li>
                            <input id="datepicker" placeholder="Date from" />
                        </li>
                        <li>
                            <input id="datepicker2" placeholder="Date to" />
                        </li>
                        <li>
                            <select id="inputState" class="form-control">
                                <option selected>Date Filter Type</option>
                                <option>...</option>
                            </select>
                        </li>
                        <li>

                            <a href="#">Search</a>

                        </li>

                    </ul>
                </div>

    <div class="order">
                    <div class="left">
                        <h1>আপনার সব অর্ডার</h1>
                        <small>Total: 146</small>
                    </div>
                    <div class="right">
                        <span> Print Labels <a class="ml-3" href="#">Download Parcel History</a></span>
                    </div>
                </div>
    <div class="table_part">
                    <div class="left">
                        <table class="table scrollbox">
                            <thead>
                                <tr>
                                    <th scope="col"><input type="checkbox"></th>
                                    <th scope="col">Creation Date</th>
                                    <th scope="col">Pickup Name</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Shop</th>
                                    <th scope="col">Customer Details</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Payment Info</th>
                                    <th scope="col">Promocode Discount</th>
                                    <th scope="col">More Info</th>
                                    <th scope="col">Last Updated</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($parcels as $key=>$parcel)
                                <tr>
                                    <th scope="row"><input type="checkbox"></th>
                                    <td>{{$parcel->created_at}}</td>
                                    <td>{{$parcel->pickup_address}}</td>
                                    <td>
                                        <div>
                                            <p><a href="{{route('user.track',$parcel->track_id)}}">ID: {{$parcel->track_id}}</a> </p>
                                            <p>Invoice ID: {{$parcel->merchant_invoice}}</p>
                                        </div>

                                    </td>
                                    <td>{{check_shop()}}</td>
                                    <td>
                                        <div>
                                            <p>{{$parcel->customer_name}}</p>
                                            <p>{{$parcel->customer_phone}}</p>
                                            <p>{{$parcel->customer_address}}</p>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div class="status_btn">
                                            <a href="#">Deleted</a>
                                            <a href="#">{{$parcel->overall_status}}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <p>Tk. {{cash_collection($parcel->track_id)}} Cash Collection</p>
                                            <p>Tk. {{total_charge($parcel->track_id)}} Charge</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <p>TK. 0</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <p>Regular Delivery</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <p>{{$parcel->updated_at}}</p>
                                        </div>
                                    </td>

                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="right">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($parcels as $key=>$parcel)
                                <tr>
                                    <td>
                                        <div class="top_btn">
                                            <a href="javascript:void(0)" onclick="show_add_category_modal('{{ route('parcels.edit',$parcel->id)}} ')">Edit</a>
                                            <a href="#">Exchange</a>
                                            <a href="#">Delete</a>
                                        </div>
                                        <div class="bottom_btn">
                                            <a href="javascript:void(0)" onclick="show_add_category_modal('{{ route('user.issue',$parcel->track_id)}} ')">Raise an issue</a>
                                        </div>
                                    </td>

                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
</div>



<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="raft_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div id="modal_content" class="modal-content">
     
      </div>
    </div>
  </div>

{{-- <div class="row mt-3 p-5 text-left" style="display:none;">
    <div class="col-md-12  mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Track ID</th>
                    <th scope="col-2">Customer Info</th>
                    <th scope="col">Pickup Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">Payment Info</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($parcels as $key=>$parcel)
                <tr>
                    <th scope="row">{{$key}}</th>
                    <td> <a href="{{route('user.track',$parcel->track_id)}}">{{$parcel->track_id}}</a> </td>
                    <td>
                        <span>Customer Name:</span> <span>{{$parcel->customer_name}}</span> <br>
                        <span>phone:</span> <span>{{$parcel->customer_phone}}</span> <br>
                        <span>Address</span> <span>{{$parcel->customer_address}}</span>
                    </td>
                    <td>{{$parcel->pickup_address}}</td>
                    <td>
                        <marquee behavior="" direction=""><span style="color:greenyellow"> Pending</span></marquee>
                    </td>
                    <td>COD</td>
                    <td>
                        <a href="" class="btn-sm btn-info">edit</a>
                        <a href="" class="btn-sm btn-danger">delete</a>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            {{$parcels->links()}}
        </ul>
    </nav>
</div> --}}

@endsection

@section('custom-js')


<script type="text/javascript">
   

    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
     });

     $('#datepicker2').datepicker({
         uiLibrary: 'bootstrap4'
     });



    function show_add_category_modal($url){
       // $('#raft_modal').modal('show',{backdrop:'static'});
           $.get($url, function(data){

               $('#modal_content').html(data);
               $('#raft_modal').modal('show',{backdrop:'static'});
            //    alert('takids');
           });
       }
     
</script>


@endsection
