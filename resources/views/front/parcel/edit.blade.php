
<div id="issues">
    <div class="main">
   <div class="heading">
    <div class="left">
        <h2>Parcel edit for <span>{{$parcel->track_id}}</span> </h2>
    </div>
    <div class="right">
        <i class="fas fa-times"></i>
    </div>
   </div>
    </div>
</div>

<div id="parcel_edit">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
               
                <form id="basic-form" method="post" action="{{route('parcels.update',$parcel->id)}}">

                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class=" col-12 col-form-label">Customer Name</label>
                        <div class=" col-12">
                            <input type="text" value="{{$parcel->customer_name}}" class="form-control" name="c_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class=" col-12 col-form-label">Customer Phone</label>
                        <div class=" col-12">
                            <input type="number" class="form-control" value="{{$parcel->customer_phone}}" name="c_phone">
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="area" class=" col-12 col-form-label">Area</label>
                        <div class=" col-12">
                            <input type="text" value="{{$parcel->deli}}"class="form-control" id="area">
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="address" class=" col-12 col-form-label">Address</label>
                        <div class=" col-12">
                            <textarea name="c_address" value="" class="form-control" id="c_address" cols="30" rows="2">{{$parcel->customer_address}}</textarea>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="amount" class=" col-12 col-form-label">Amount</label>
                        <div class=" col-12">
                            <input type="number" class="form-control" id="amount">
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="price" class=" col-12 col-form-label">Selling Price</label>
                        <div class=" col-12">
                            <input type="number" value="{{$parcel->sell_price}}" class="form-control" name="sell_price">
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="weight" class=" col-12  col-form-label">Weight</label>
                        <div class=" col-12">
                            <input type="number" class="form-control" id="weight">
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="category" class=" col-12 col-form-label">Product Category</label>
                        <div class=" col-12">
                            <select name="p_cat" id="" class="form-control">
                                @foreach ($cats as $cat)
                                  <option {{ $parcel->category == $cat->id ? 'selected':""}} value="{{$cat->id}}">{{$cat->name}}</option> 
                                @endforeach
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="instruction" class=" col-12 col-form-label">Instruction</label>
                        <div class=" col-12">
                            <textarea name="notes" value="" class="form-control" id="instruction" cols="30" rows="2">{{$parcel->notes}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row justify-content-center mt-5">
                        <div class="col-sm-10 text-center">
                            <button type="submit">Update</button>
                             <button type="submit">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>







