@extends('front.raftmaster')
@section('main')
@php
!empty($parcel_status->pickup_pending)?$pickup_pending=json_decode($parcel_status->pickup_pending,true):"";
!empty($parcel_status->pickup_compeleted)?$pickup_compeleted=json_decode($parcel_status->pickup_compeleted,true):"";
!empty($parcel_status->send_shub)?$send_shub=json_decode($parcel_status->send_shub,true):"";
!empty($parcel_status->received_shub)?$received_shub=json_decode($parcel_status->received_shub,true):"";
!empty($parcel_status->send_dhub)?$send_dhub=json_decode($parcel_status->send_dhub,true):"";
!empty($parcel_status->received_dhub)?$received_dhub=json_decode($parcel_status->received_dhub,true):"";
!empty($parcel_status->delivery_onway)?$delivery_onway=json_decode($parcel_status->delivery_onway,true):"";
!empty($parcel_status->return)?$return=json_decode($parcel_status->return,true):"";
!empty($parcel_status->delivery_delay)?$delivery_delay=json_decode($parcel_status->delivery_delay,true):"";
!empty($parcel_status->delivery_done)?$delivery_done=json_decode($parcel_status->delivery_done,true):"";
!empty($parcel_status->cash_collect)?$cash_collect=json_decode($parcel_status->cash_collect,true):"";

   


@endphp


<div id="track">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="heading">
                            <div class="left">
                                <h1>ট্র্যাক পার্সেল</h1>
                            </div>
                            <div class="right_head">
                                <a href="#"><i class="fab fa-font-awesome-flag"></i> Raise an issue </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-12">
                        <div class="track_id">
                            <p>Tracking ID</p>
                            <h4>{{$parcel->track_id}}</h4>
                        </div>
                    </div>
                    <div class="col-sm-9 col-12">

                        <div class="check_body">
                            @if($cash_collect['status']==1)
                            <div class="check_box">
                                <div class="icon_part ">
                                    <i class="fas fa-check active"></i>
                                </div>
                                <div class="text_part">
                                    <h4>পার্সেলের লেনদেনটি সম্পূর্ণ হয়েছে</h4>
                                    <p>{{$cash_collect['time']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($delivery_done['status']==1)
                            <div class="check_box">
                                <div class="icon_part">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="text_part">
                                    <h4>পার্সেল ডেলিভারি সম্পন্ন হয়েছে</h4>
                                    <p>{{$delivery_done['time']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($delivery_onway['status']== 1)
                            <div class="check_box">
                                <div class="icon_part">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="text_part">
                                    <h4>ডেলিভারি এজেন্ট  {{!empty($delivery_onway['rider'])?$delivery_onway['rider']:'--'}} ডেলিভারির জন্যে বের হয়েছে
                                    </h4>
                                    <p>{{$delivery_onway['time']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($received_dhub['status']==1)
                            <div class="check_box">
                                <div class="icon_part">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="text_part">
                                    <h4>পার্সেলটি {{isset($received_dhub['hub'])?$received_dhub['hub']:'--'}} এ রিসিভ করা হয়েছে</h4>
                                    <p>{{$received_dhub['time']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($send_dhub['status']==1)
                            <div class="check_box">
                                <div class="icon_part">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="text_part">
                                    <h4>পার্সেলটি {{isset($send_dhub['hub'])?$send_dhub['hub']:'--'}} এ পাঠানো হচ্ছে</h4>
                                    <p>{{$send_dhub['time']}}</p>
                                </div>
                            </div>
                            @endif
                         
                            @if($received_shub['status']==1 )
                            <div class="check_box">
                                <div class="icon_part">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="text_part">
                                    <h4>পার্সেলটি {{isset($received_shub['hub'])?$received_shub['hub']:'--'}}  এ রিসিভ করা হয়েছে</h4>
                                    <p>{{$received_shub['time']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($send_shub['status']==1 )
                                <div class="check_box">
                                    <div class="icon_part">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="text_part">
                                        <h4>পার্সেলটি {{isset($send_shub['hub'])?$send_shub['hub']:'--'}}  এ পাঠানো হচ্ছে</h4>
                                        <p>{{$send_shub['time']}}</p>
                                    </div>
                                </div>
                             @endif   
                            @if($pickup_compeleted['status']==1 )
                                <div class="check_box">
                                    <div class="icon_part">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="text_part">
                                        <h4>পার্সেল পিকাপ সম্পন্ন হয়েছে</h4>
                                        <p>{{$pickup_compeleted['time']}}</p>
                                    </div>
                                </div>
                             @endif   
                            @if($pickup_pending['status']==1 && !empty($parcel_status->pickup_pending))
                                <div class="check_box">
                                    <div class="icon_part">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="text_part">
                                        <h4>পার্সেলটি পিকআপের জন্য মার্চেন্ট অনুরোধ করেছেন</h4>
                                        <p>{{$pickup_pending['time']}}</p>
                                    </div>
                                </div>
                            @endif    
                        </div>



                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-12">
                <div class="right">
                    <div class="top">
                        <h1>কাস্টমার ও অর্ডারের বিস্তারিত তথ্য</h1>
                    </div>
                    <div class="bottom">
                        <div class="items">
                            <p>Parcel ID</p>
                            <h4>{{$parcel->track_id}}</h4>
                        </div>
                        <div class="items">
                            <p>Customer Address</p>
                            <h4>{{$parcel->customer_address}}</h4>
                        </div>
                        <div class="items">
                            <p>Customer Phone</p>
                            <h4>{{$parcel->customer_phone}}</h4>
                        </div>
                        <div class="items">
                            <p>Customer Name</p>
                            <h4>{{$parcel->customer_name}}</h4>
                        </div>
                        <div class="items">
                            <p>Area</p>
                            <h4>Khulna Sadar</h4>
                        </div>
                        <div class="items">
                            <p>Weight</p>
                            <h4>500</h4>
                        </div>
                        <div class="items">
                            <p>Price</p>
                            <h4>{{$parcel->sell_price}}</h4>
                        </div>
                        <div class="items">
                            <p>Delivery Charge</p>
                            <h4>{{$delivery_details->delivery_charge}}</h4>
                        </div>
                        <div class="items">
                            <p>COD Charge</p>
                            <h4>{{$delivery_details->cod_charge}}</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class="row mt-3 p-5 " style="display:none;">
    <div class="col-md-12 text-left  mt-5 py-5" style="background-color:rgba(255, 255, 255,0.3); border-radius:5px">
        <h4 style="text-transform:none ;text-decoration:underline">Track Parcel</h4>
        <div class="row">
            <div class="col-md-7">
                {{-- @if($pickup_compeleted['status']==1 && !empty($parcel_status->pickup_completed))
                <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
                    <div class="card-header">পার্সেলটি পিকআপের জন্য মার্চেন্ট অনুরোধ করেছেন <br>
                        <span>{{$pickup_pending['time']}}</span>
            </div>

        </div>
        @endif --}}
        @if($pickup_pending['status']==1 && !empty($parcel_status->pickup_pending))
        <div class="card text-white bg-primary mb-3" style="max-width:25rem;">
            <div class="card-header">পার্সেলটি পিকআপের জন্য মার্চেন্ট অনুরোধ করেছেন <br>
                <span>{{$pickup_pending['time']}}</span>
            </div>

        </div>
        @endif


    </div>
    <div class="col-md-5"></div>
</div>
</div>
</div>

@endsection
