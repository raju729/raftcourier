@extends('front.raftmaster')
@section('main')

<div id="parcel_view">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-12">
                <div class="view_left">

                    <div class="view_heading">
                        <h1><span><i class="fas fa-check-circle"></i></span> Your parcel request successfully placed
                        </h1>
                        <p>Parcel ID <span>{{$parcel_view->track_id}}</span> will be picked up by our deliveryman by October 10, 2021
                        </p>
                    </div>


                    <div class="view_download">
                        <div class="down_text">
                            <p>Download the label below and paste on the top of your parcel</p>
                        </div>
                        <div class="down_link">
                            <a target="_blank" href="{{route('user.label.download',$parcel_view->id)}}"><i class="fas fa-download"></i> Download Print Label</a>
                        </div>
                    </div>


                    <div class="view_body">
                        <div class="view_marchant">
                            <div class="left">
                                <h3>MARCHANT:</h3>
                            </div>
                            <div class="right">
                                <h5>{{$parcel_shop->shop_name}}</h5>
                                <p>{{$parcel_shop->shop_address}}</p>
                                <p>{{$parcel_shop->pickup_phone}}</p>
                            </div>
                        </div>

                        <div class="view_customer">
                            <div class="left">
                                <div class="heading">
                                    <h3>CUSTOMER:</h3>
                                </div>
                                <div class="right">
                                    <h5>{{$parcel_view->customer_name}}</h5>
                                    <p>{{$parcel_view->customer_address}}</p>
                                    <p>{{$parcel_view->customer_phone}}</p>
                                </div>
                            </div>

                            <div class="amount">
                                <h6>{{$parcel_view->sell_price}}</h6>
                            </div>
                        </div>

                        <div class="view_invoice">
                            <div class="left">
                                <p>INVOICE#: {{$parcel_view->merchant_invoice}}</p>
                            </div>
                            <div class="right">
                                <p><span>AREA:</span> BijoyNagar</p>
                                <p><span> HUB:</span> BijoyNagar Hub</p>
                            </div>
                        </div>

                        <div class="view_status">
                            <p>no</p>
                        </div>

                        <div class="view_bar">
                            <div class="left">
                                <img class="img-fluid" src="{{asset('public/qrcodes/'.$parcel_view->track_id.'.png')}}" alt="scan">
                            </div>
                            <div class="right">
                                <div class="top">
                                    <img class="img-fluid" src="{{asset('public/front/images/parcel_view/scan-2.jpg')}}" alt="scan-2">
                                </div>
                                <div class="bottom">                      
                                    <p><span> PARCEL ID:</span> {{$parcel_view->track_id}}</p>
                                    <p><span> PARCEL CREATED:</span> {{$parcel_view->created_at}}</p>                                
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="view_btn">
                    <div class="left">
                        <a href="{{route('parcels.index')}}">Go To Parcels Page</a>
                    </div>
                    <div class="right">
                        <a href="{{route('parcels.create')}}">Create New Parcels</a>
                    </div>
                </div>

                </div>
            </div>


            <div class="col-lg-4 col-12">
                <div class="view_right">
                    <div class="know">
                        <div class="top">
                            <h4><span><i class="far fa-lightbulb"></i></span> Did you know?</h4>
                        </div>
                        <div class="middle">
                            <h2>16%</h2>
                            <p>Reduction in the chances of parcel loss by pasting the sticker on a parcel before pickup
                            </p>
                        </div>
                        <div class="bottom">
                            <h2>16%</h2>
                            <p>Reduction in the chances of parcel loss by pasting the sticker on a parcel before pickup
                            </p>
                        </div>
                    </div>
                    <div class="view_guide">
                        <h3><span><i class="fab fa-glide"></i></span> Guidelines</h3>
                        <p><span><i class="fas fa-arrow-alt-circle-right"></i></span> Avoid tissue packaging</p>
                        <p><span><i class="fas fa-arrow-alt-circle-right"></i></span> Write last 4 digits of customer phone number on the parcel so that it is easier to segregate
                            the parcel</p>
                    </div>

                </div>
            </div>

        </div>


       
    </div>
</div>


@endsection
