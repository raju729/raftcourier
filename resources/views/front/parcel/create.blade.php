@extends('front.raftmaster')
@section('main')

<div id="create_part">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-12">

                <div class="left_part">

                    <div class="ship_part">
                        <span>
                            <p>Ship From &nbsp; {{$shop->shop_name}} <a class="pl-4" href="{{ route('shops.index')}}">Choose a different shop</a></p>
                        </span>
                        <span>
                            <p>Pickup address: &nbsp; {{$shop->pickup_address}} </p>
                        </span>
                        <select name="location" id="">
                            <option value="location">All Pickup Location</option>
                            <option value="dhaka">Dhaka</option>
                        </select>
                    </div>


                    <div class="button_part">
                        <div class="heading">
                            <h1>নতুন পার্সেল রিক্যুয়েস্ট তৈরি করুন</h1>
                        </div>
                        <div class="button">
                            <div class="one">
                                <a href="#">Create Bulk Parcels</a>
                            </div>
                            {{-- <div class="two">
                                <a href="#">Import From ShopUp</a>
                            </div> --}}
                        </div>
                    </div>

                    <div class="form_part" >

                        <form method="post" id="basic-form" action="{{route('parcels.store')}}">@csrf
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group col-lg-12 col-12">
                                        <input type="text" class="form-control" value="" name="customer_name" required id=""
                                            placeholder="কাস্টমারের নাম">
                                    </div>

                                    <div class="form-group col-lg-12 col-12">
                                        <input type="text" class="form-control" value="" name="customer_phone" required
                                            id="" placeholder="কাস্টমারের ফোন নম্বর">
                                    </div>



                                    <div class="form-group col-lg-12 col-12 info_part">
                                        <label for="exampleInputEmail1">কাস্টমারের ঠিকানা</label>
                                        <textarea type="text" class="form-control" required name="customer_address" rows="6"
                                            placeholder="কাস্টমারের ঠিকানা" id="exampleInputEmail1"></textarea>
                                         <div class="icon_part">
                                         <i class="fas fa-info-circle"><span class="tooltiptext"> Address Format: Address, Area, District, Code Sample Address: 35, Eastern Paradise, Flat-4B, Shegun Bagicha, Dhaka-1000</span></i>
                                         </div>   
                                    </div>



                                    <div class="form-group col-lg-12 col-12">
                                        <input type="text" class="form-control" value="" name="merchant_invoice" required
                                            id="" placeholder="মার্চেন্ট ইনভয়েস আইডি">
                                    </div>

                                    <div class="form-group col-lg-12 col-12">
                                    
                                            <select name="product_weight" id="p_weight" class="form-control">
                                                <option disabled selected value> সিলেক্ট করুন</option>
                                                @foreach ($weight as $item)
                                                <option class="back-color" value="{{$item->id}}">{{$item->weight}}</option>
                                                @endforeach
                                            </select>
                                    </div>

                                    <div class="form-group col-lg-12 col-12">
                                        <select class="form-control " id="exampleFormControlSelect1" name="product_category"
                                            required>
                                            <option disabled selected value> প্রোডাক্ট ক্যাটেগরি সিলেক্ট করুন</option>
                                            @foreach ($cats as $item) <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group col-lg-12 col-12">
                                        <select id="p_area_crg" name="delivery_area" required  class="form-control">
                                            <option selected>ডেলিভারি এরিয়া নির্বাচন করুন...</option>
                                            @foreach ($area as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-12 col-12 chek_item">
                                            <div class="form-check">
                                                <label for="">আপনার পার্সেল এর ক্ষেত্রে যেগুলো প্রযোজ্য টিক দিন</label> <br>
                                                <span class="ml-3">
                                                    <input class="form-check-input" type="checkbox" name="fragile"
                                                        id="gridCheck">
                                                    <label class="form-check-label" for="gridCheck">
                                                    Fragile 
                                                    </label>
                                                </span>
                                                <span class="ml-5"> 
                                                    <input class="form-check-input" type="checkbox"name="liquid"
                                                        id="gridCheck1">
                                                    <label class="form-check-label" for="gridCheck1">
                                                    Liquid
                                                    </label>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-lg-12 col-12">
                                        <input type="number" class="form-control" onchange="delivery_charge()" value=""
                                            name="cash_collection" required id="cash_collection"
                                            placeholder="ক্যাশ কালেকশন অ্যামাউন্ট">
                                    </div>

                                    <div class="form-group col-lg-12 col-12">
                                        <input type="number" class="form-control" value="" name="sell_price" required id=""
                                            placeholder="পণ্যের বিক্রয় মূল্য">
                                    </div>

                                    <div class="form-group col-lg-12 col-12">
                                        <label for="exampleInputEmail1">জরুরি: দয়া করে আপনার পণ্যটির বিক্রয় মূল্য উল্লেখ
                                            করুন। আপনার পার্সেলটি হারিয়ে গেলে বা ক্ষতিগ্রস্থ হলে এই অর্থ পরিমাণ ক্ষতিপূরণ
                                            দেয়া হবে।</label>
                                        <textarea type="text" class="form-control" name="notes" rows="5"
                                            placeholder="কোন বিশেষ নির্দেশনা"></textarea>
                                    </div>

                                </div>
                            </div>


                            <div class="form-row justify-content-center">
                                <div class="form-group col-md-12 text-center">
                                    <button type="submit">পার্সেল তৈরি করুন</button>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="right_part">
                    <div class="charge_part">
                        <div class="heading mb-4">
                            <h1>ডেলিভারি চার্জের বিস্তারিত </h1>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Cash Collection</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <td scope="col"> <b><span id="show_cash_collection">0</span></b><span
                                            style="font-size:12px">tk</span> </td>
                                </tr>
                                <tr>
                                    <th scope="col">Delivery Charge</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <td scope="col"> <b><span id="delivery_charge_total">0</span></b> <span
                                            style="font-size:12px">tk</span></td>
                                </tr>
                                <tr>
                                    <th scope="col">COD Charge</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <td scope="col"><b><span id="show_cod">0</span></b> <span
                                            style="font-size:12px">tk</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Total payable amount</th>
                                    <td></td>
                                    <td></td>
                                    <th><span id="show_total_payable">0</span> <span style="font-size:12px">tk</span>
                                    </th>
                                </tr>

                            </tbody>
                        </table>
                        <div class="bottom text-center">
                            <p>দুপুর ৩ টার পরের পিকাপ রিকুয়েস্ট আগামী কালকে পিকাপ হবে।</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class="row mt-3 p-5 " style="display:none;">
    <div class="col-md-12 text-left  mt-5 py-5">
        <div class="card mb-5">
            <div class="card-body">

                <p class="card-text c-b">Ship from : <b> {{$shop->shop_name}}</b> </p>
                <p style="font-size: 12px;" class=" c-b">Pickup Address:{{$shop->pickup_address}}</p>
                <a href="{{ route('shops.index')}}" class="btn-sm btn-danger">choose different shop</a>
            </div>
        </div>
        <h4 style="text-transform:none ;text-decoration:underline">নতুন পার্সেল রিক্যুয়েস্ট তৈরি করুন</h4>
        <div class="row">

            <div class="col-md-7 text-left">
                <form role="form" id="quickForm" method="post" action="{{route('parcels.store')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">

                                <input type="text" class="form-control" value="" name="customer_name" required id=""
                                    placeholder="কাস্টমারের নাম">
                            </div>
                            <div class="form-group">

                                <input type="text" class="form-control" value="" name="customer_phone" required id=""
                                    placeholder="কাস্টমারের ফোন নম্বর">
                            </div>
                            <div class="form-group">

                                <textarea type="text" class="form-control" name="customer_address" rows="6"
                                    placeholder="কাস্টমারের ঠিকানা"></textarea>
                            </div>
                            <div class="form-group">

                                <input type="text" class="form-control" value="" name="merchant_invoice" required id=""
                                    placeholder="মার্চেন্ট ইনভয়েস আইডি">
                            </div>
                            <div class="form-group">
                                <select class="form-control " id="p_weight" name="product_weight" required>
                                    <option disabled selected value> সিলেক্ট করুন</option>
                                    @foreach ($weight as $item)
                                    <option class="back-color" value="{{$item->id}}">{{$item->weight}}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="form-group">
                                <select class="form-control " id="exampleFormControlSelect1" name="product_category"
                                    required>
                                    <option disabled selected value> প্রোডাক্ট ক্যাটেগরি সিলেক্ট করুন</option>
                                    @foreach ($cats as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control js-example-basic-single" id="p_area_crg"
                                    name="delivery_area" required>
                                    <option disabled selected value> সিলেক্ট করুন</option>
                                    @foreach ($area as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">আপনার পার্সেল এর ক্ষেত্রে যেগুলো প্রযোজ্য টিক
                                    দিন</label>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="fragile">
                                    <label class="" for="exampleCheck1">Fragile</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="liquid">
                                    <label class="" for="exampleCheck1">Liquid</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" onchange="delivery_charge()" value=""
                                    name="cash_collection" required id="cash_collection"
                                    placeholder="ক্যাশ কালেকশন অ্যামাউন্ট">
                            </div>

                            <div class="form-group">
                                <input type="number" class="form-control" value="" name="sell_price" required id=""
                                    placeholder="পণ্যের বিক্রয় মূল্য">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">জরুরি: দয়া করে আপনার পণ্যটির বিক্রয় মূল্য উল্লেখ করুন।
                                    আপনার পার্সেলটি হারিয়ে
                                    গেলে বা ক্ষতিগ্রস্থ হলে এই অর্থ পরিমাণ ক্ষতিপূরণ দেয়া হবে।</label>
                                <textarea type="text" class="form-control" name="notes" rows="6"
                                    placeholder="কোন বিশেষ নির্দেশনা"></textarea> </div>


                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">পার্সেল তৈরি করুন</button>
                </form>
            </div>


            <div class="col-md-4" style="border-left: 1px solid white">
                <h4 style="text-transform:none ;text-decoration:underline"><b>ডেলিভারি চার্জের বিস্তারিত</b> </h4>
                <div>
                    <table class="table w-100">
                        <thead>
                            <tr>
                                <th scope="col">Cash Collection</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <td scope="col"> <b><span id="show_cash_collection">0</span></b><span
                                        style="font-size:12px">tk</span> </td>
                            </tr>
                            <tr>
                                <th scope="col">Delivery Charge</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <td scope="col"> <b><span id="delivery_charge_total">0</span></b> <span
                                        style="font-size:12px">tk</span></td>
                            </tr>
                            <tr>
                                <th scope="col">COD Charge</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <td scope="col"><b><span id="show_cod">0</span></b> <span
                                        style="font-size:12px">tk</span></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Total payable amount</th>
                                <td></td>
                                <td></td>
                                <th><span id="show_total_payable">0</span> <span style="font-size:12px">tk</span></th>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('custom-js')
<script>



    $(document).ready(function () {
        localStorage.setItem("area_charge", 0);
        localStorage.setItem("weight_charge", 0);
    });

    // In your Javascript (external .js resource or <script> tag)
    // $('.js-example-basic-single').select2({
    //     placeholder: 'Select an option'
    // });


    $('#p_weight').on('change', function () {
        $id = this.value;
        get_weight($id);
    });

    $('#p_area_crg').on('change', function () {
        $id = this.value;
        get_area_charge($id);
    });



    function get_weight($id) {
        $.get("{{URL::to('parcels/weightcharge')}}/" + $id, function (data) {
            localStorage.setItem("weight_charge", data.weight);
            // $('#p_subcat').html(data);
            delivery_charge();
        });
    }

    function get_area_charge($id) {
        $.get("{{URL::to('parcels/areacharge')}}/" + $id, function (data) {
            localStorage.setItem("area_charge", data.area);
            delivery_charge();

            // $('#p_subcat').html(data);
        });
    }

    function delivery_charge() {
        $cash_col = $('#cash_collection').val()
        if ($cash_col) {
            $('#show_cash_collection').text($cash_col);
            $cod_charge = parseInt($cash_col) / 100;
            $('#show_cod').text($cod_charge);
        } else {
            $cash_col = 0;
            $cod_charge = 0;
            $('#show_cash_collection').text('0tk');
        }
        $total_charge = parseInt(localStorage.getItem("area_charge")) + parseInt(localStorage.getItem("weight_charge"));
        $('#delivery_charge_total').text($total_charge);
        $total_payable = ($total_charge + parseInt($cod_charge)) - parseInt($cash_col);
        $('#show_total_payable').text($total_payable);


    }

</script>
@endsection
