<div id="issues" >
    <div class="main" id="pic_first">
        <div class="heading">
            <div class="left">
                <h2>Issue creation for <span>211010SU9QX3I</span> </h2>
            </div>
            <div class="right">
                <i id="modal_close" class="fas fa-times" ></i>
            </div>
        </div>
        <div class="body" >
            <div class="title" >
                <h4>Please select an issue type</h4>
            </div>
            <div class="items" >
                <ul> 
                    <li id="picup">Pickup  <i class="fas fa-chevron-right float-right"></i></li>
                    <li>Delivery  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Info Update  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Return  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Exchange  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Exchange  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Payment  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Fake Return  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Delivery Agent  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Parcel Damaged  <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Parcel Lost  <i class="fas fa-chevron-right float-right"></i></li> 
                    
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="issues" class="pic_one" style="display: none;">
    <div class="main">
        <div class="heading">
            <div class="left">
                <h2>Issue creation for <span>211010SU9QX3I</span> </h2>
            </div>
            <div class="right">
                <i class="fas fa-times"></i>
            </div>
        </div>
        <div class="body">
            <div class="title">
                <h4>Pickup</h4>
            </div>
            <div class="items">
                <ul>
                    <li id="d_picup">Delay in Pickup <i class="fas fa-chevron-right float-right"></i></li>
                    <li>Pickup man did not arrive <i class="fas fa-chevron-right float-right"></i></li> 
                    <li>Pickup is showing as Pending <i class="fas fa-chevron-right float-right"></i></li>
                </ul>
            </div>
        </div>
        <div class="pic_back">
            <a id="back" href="#">Back</a>
        </div>
    </div>
</div>

<div id="issues" class="pic_one_one" style="display: none;">
    <div class="main">
        <div class="heading">
            <div class="left">
                <h2>Issue creation for <span>211010SU9QX3I</span> </h2>
            </div>
            <div class="right">
                <i class="fas fa-times"></i>
            </div>
        </div>
        <div class="body">
            <div class="title">
                <h4>Delay in Pickup</h4>
            </div>
            <div class="items">
                <ul>
                    <li id="delay_last">I placed a pickup request but no one came yet <i class="fas fa-chevron-right float-right"></i></li>
                </ul>
            </div>
        </div>
        <div class="pic_back">
            <a id="back1" href="#">Back</a>
        </div>
    </div>
</div>

<div id="issues" class="pic_one_two" style="display: none;">
    <div class="main">
        <div class="heading">
            <div class="left">
                <h2>Issue creation for <span>211010SU9QX3I</span> </h2>
            </div>
            <div class="right">
                <i class="fas fa-times"></i>
            </div>
        </div>
        <div class="body">
            <div class="title">
                <h4><span style="font-size:16px; font-weight:bold;">Issue:</span>  I placed a pickup request but no one came yet</h4>
            </div>
            <div class="items">
                <ul>
                    <li><textarea class="form-control" name="" id="" cols="30" rows="10"></textarea></li>
                </ul>
            </div>
        </div>
        <div class="pic_back">
            <a id="back2" href="#">Back</a>
            <a href="#">Confirm</a>
        </div>
    </div>
</div>


<script>


     document.getElementById("modal_close").addEventListener("click", modal_hide_all);
           function modal_hide_all(){ 
               $("#issues").hide();
              

           };

        document.getElementById("picup").addEventListener("click", modal_hide);
           function modal_hide(){ 
               $("#pic_first").hide();
               $(".pic_one").show();

           };

           document.getElementById("d_picup").addEventListener("click", modal_hide2);
           function modal_hide2(){ 
               $(".pic_one").hide();
               $(".pic_one_one").show();
               $("#pic_first").hide();
           };

           document.getElementById("delay_last").addEventListener("click", modal_hide3);
           function modal_hide3(){ 
               $(".pic_one").hide();
               $(".pic_one_one").hide();
               $("#pic_first").hide();
               $(".pic_one_two").show();
           };



           document.getElementById("back").addEventListener("click", modal_back);
           function modal_back(){ 
              $("#pic_first").show();
               $(".pic_one").hide();
           };
           document.getElementById("back1").addEventListener("click", modal_back1);
           function modal_back1(){ 
              $("#pic_first").hide();
               $(".pic_one").show();
               $(".pic_one_one").hide();
           };
           document.getElementById("back2").addEventListener("click", modal_back2);
           function modal_back2(){ 
              $("#pic_first").hide();
               $(".pic_one").hide();
               $(".pic_one_one").show();
               $(".pic_one_two").hide();
           };
</script>



@section('custom-js')

@endsection