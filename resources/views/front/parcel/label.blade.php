@extends('front.raftemptymaster')
@section('main')


<div id="parcel_view">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-12">
                <div class="view_left">

                  



                    <div class="view_body">
                        <div class="view_marchant">
                            <div class="left">
                                <h3>MARCHANT:</h3>
                            </div>
                            <div class="right">
                                <h5>{{$parcel_shop->shop_name}}</h5>
                                <p>{{$parcel_shop->shop_address}}</p>
                                <p>{{$parcel_shop->pickup_phone}}</p>
                            </div>
                        </div>

                        <div class="view_customer">
                            <div class="left">
                                <div class="heading">
                                    <h3>CUSTOMER:</h3>
                                </div>
                                <div class="right">
                                    <h5>{{$parcel_view->customer_name}}</h5>
                                    <p>{{$parcel_view->customer_address}}</p>
                                    <p>{{$parcel_view->customer_phone}}</p>
                                </div>
                            </div>

                            <div class="amount">
                                <h6>{{$parcel_view->sell_price}}</h6>
                            </div>
                        </div>

                        <div class="view_invoice">
                            <div class="left">
                                <p>INVOICE#: {{$parcel_view->merchant_invoice}}</p>
                            </div>
                            <div class="right">
                                <p><span>AREA:</span> BijoyNagar</p>
                                <p><span> HUB:</span> BijoyNagar Hub</p>
                            </div>
                        </div>

                        <div class="view_status">
                            <p>no</p>
                        </div>

                        <div class="view_bar">
                            <div class="left">
                                <img class="img-fluid" src="{{asset('public/qrcodes/'.$parcel_view->track_id.'.png')}}" alt="scan">
                            </div>
                            <div class="right">
                                <div class="top">
                                    <img class="img-fluid" src="{{asset('public/front/images/parcel_view/scan-2.jpg')}}" alt="scan-2">
                                </div>
                                <div class="bottom">                      
                                    <p><span> PARCEL ID:</span> {{$parcel_view->track_id}}</p>
                                    <p><span> PARCEL CREATED:</span> {{$parcel_view->created_at}}</p>                                
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>

                </div>
            </div>


            <div class="col-lg-4 col-12">
               
            </div>

        </div>


       
    </div>
</div>

@endsection
@section('custom-js')
    <script>
        $( document ).ready(function() {
         window.print();
});
    </script>
@endsection

