@extends('front.raftmaster')
@section('main')

<div id="faq">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="faq_heading">
                    <h1>Read our commonly asked questions</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="faq_left">
                    <ul>
                        <li class="active">
                            <a class="items" id="general_id" href="#">
                                <h3>General</h3>
                                <p>Your general queries</p>
                            </a>
                        </li>
                        <li>
                            <a class="items" id="payment_id" href="#">
                                <h3>Payment/Rates</h3>
                                <p>Queries regarding payment & delivery rates</p>
                            </a>
                        </li>
                        <li>
                            <a class="items" id="return_id" href="#">
                                <h3>Return/Damage/Issues</h3>
                                <p>Info on issue resolution</p>
                            </a>
                        </li>
                        <li>
                            <a class="items" id="coverage_id" href="#">
                                <h3>Coverage area and delivery time</h3>
                                <p>Info on coverage area & delivery time</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-12">
                <div class="faq_right">

                    <div class="general">
                        <div class="top_title">
                            <h2>Small business</h2>
                        </div>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            What is RaftCourier Delivery?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        RaftCourier Delivery is a logistics company that aims to aid Bangladesh’s
                                        expanding e-commerce sector by providing tech-first delivery support. With
                                        exclusive features and a talented workforce, RaftCourier Delivery gives the
                                        delivery service industry of Bangladesh a brand new pace. <br> RaftCourier
                                        delivers for both businesses and individuals. You can use our services to
                                        deliver everything from products to your customer’s doorstep to important
                                        personal documents and parcels to your friends and family.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            How do I contact you?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        You can call us at 01844-488983 or you can email us at
                                        contact@RaftCourier.com.bd. Additionally you can also reach out to us over live
                                        chat from our app or from our web portal. You can also connect with us on <a
                                            href="#">Facebook.</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            How can a merchant track their parcel delivery?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        You can track your parcel through our website. Simply enter your parcel ID on
                                        our website here to see the latest update on your package.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseFour" aria-expanded="true"
                                            aria-controls="headingFour">
                                            How do I send a product/ courier via RaftCourier Delivery?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        We do home delivery for products and parcels of multiple sizes and weights all
                                        over Bangladesh. To avail our services, please register with us on our website
                                        <a href="#">here</a> , or download the RaftCourier app from Google Play Store.
                                        You can also contact us at 01844-488983 or reach out through our <a
                                            href="#">Facebook page</a> .
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                            aria-controls="collapseFive">
                                            I want to hold a parcel for more than 3 days before home delivery. Is it
                                            possible?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Currently we do not hold any parcel for more than 3 days once a parcel reaches
                                        its last mile hub. For more information call us at 01844-488983.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseSix" aria-expanded="false"
                                            aria-controls="collapseSix">
                                            Can you do product exchange from customers?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes, we do provide reverse delivery. If your customer wants to return a product,
                                        we can collect it from them and deliver it back to you. For more information
                                        please contact us.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true"
                                            aria-controls="collapseSeven">
                                            Can you deliver to addresses inside Cantonment or other restricted areas?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Although RaftCourier provides doorstep home delivery service in Dhaka along with
                                        all other districts, if security in such areas prohibit outside entry, our
                                        delivery men will wait at the gate and you can come collect your package from
                                        there.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseEight" aria-expanded="false"
                                            aria-controls="collapseEight">
                                            I do not have a Facebook page, can I register as a merchant?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes, you can. All you need is a registered phone number.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseNine" aria-expanded="false"
                                            aria-controls="collapseNine">
                                            What kind of products does RaftCourier deliver?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        We deliver all valid portable products. RaftCourier is a courier service and
                                        parcel delivery service that offers the flexibility merchants and customers
                                        need.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title">
                            <h2>Enterprise</h2>
                        </div>
                        <div class="accordion" id="accordionExample2">
                            <div class="card">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseTen" aria-expanded="true"
                                            aria-controls="collapseTen">
                                            Do you have any web version or mobile application for cargo requisition?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample2">
                                    <div class="card-body">
                                        Currently we’re collecting requisitions over mail & hotline. However, we’re
                                        working on the web version. Besides, you’ll be able to book a cargo through
                                        RaftCourier android app very soon.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false"
                                            aria-controls="collapseEleven">
                                            Currently What’s the procedure to book a cargo for my shipment?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample2">
                                    <div class="card-body">
                                        Simply call us & let us know your requirements in detail. We’ll get back to you
                                        with the vehicle confirmation & pricing within the next 120 minutes. If you
                                        agree, we’ll simply deploy our cargo to report to your loading point within the
                                        next 60 minutes. <br> For prefixed requirements, simply drop a mail to
                                        cargo@RaftCourier.com.bd & we’ll get back to you as early as possible.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="payment" style="display: none;">
                        <div class="top_title">
                            <h2>Small business</h2>
                        </div>
                        <div class="accordion" id="accordionExample3">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            What is the delivery charge?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                    data-parent="#accordionExample3">
                                    <div class="card-body">
                                        The regular delivery charge for parcel delivery (upto 1 kg) is as follows:
                                        Inside Dhaka - ৳60
                                        Dhaka Suburb - ৳100 + 1% COD
                                        Outside Dhaka- ৳130 + 1% COD
                                        Please note that charges vary depending on weight and location. For more details
                                        visit this link and refer to the price guide.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            What is the payment cycle for merchants?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample3">
                                    <div class="card-body">
                                        Our payment cycle is 5 days a week (Sunday to Thursday). Once a parcel is
                                        delivered, the payment will be sent to you on the next working day.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            How can I update my payment information?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample3">
                                    <div class="card-body">
                                        Log in to your account at RaftCourier.com.bd, and navigate to My Payment Details
                                        and
                                        update your information from there.
                                        But if your payment information has already been set, you will have to email us
                                        at contact@RaftCourier.com.bd or call us at 01844-488983 in order to change or
                                        update
                                        it.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseFour" aria-expanded="true"
                                            aria-controls="headingFour">
                                            How will I receive my merchant payment?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample3">
                                    <div class="card-body">
                                        You will receive the payment for all parcel delivery done within a particular
                                        day either via your bank account or bKash account within the next working day.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title">
                            <h2>Enterprise</h2>
                        </div>
                        <div class="accordion" id="accordionExample4">
                            <div class="card">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseTen" aria-expanded="true"
                                            aria-controls="collapseTen">
                                            Currently In which format are you operating? Is it fixed rate or open rate
                                            policy?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample4">
                                    <div class="card-body">
                                        We’re offering both fixed rate & open rate pricing to ensure convenience for our
                                        valued B2B enterprises.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false"
                                            aria-controls="collapseEleven">
                                            How will you collect the payment after the shipment delivery is done?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample4">
                                    <div class="card-body">
                                        we’re available to collect our payment on both on-spot (Cash) & credit. Separate
                                        invoices will be generated by our Finance team & the due will be collected
                                        within the credit period.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="return" style="display: none;">
                        <div class="top_title">
                            <h2>Small business</h2>
                        </div>
                        <div class="accordion" id="accordionExample5">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            I have an issue related to my delivery. What should I do?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                    data-parent="#accordionExample5">
                                    <div class="card-body">
                                        You can simply write your issues to us by using the live chat option or call us
                                        at 01844-488983.
                                        Additionally you can email us at contact@RaftCourier.com.bd along with your
                                        customer
                                        details and tracking ID with necessary attachments if needed.
                                        Our dedicated team will investigate the issue and get back to you in the
                                        shortest possible time.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            When should I inform you if an item is lost or damaged?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample5">
                                    <div class="card-body">
                                        You must inform us regarding any lost or damaged items immediately upon
                                        discovery. It can be either during delivery or return. The maximum time we allow
                                        for raising an issue is within 3 days.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            What is the compensation/refund process for lost or damaged goods?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample5">
                                    <div class="card-body">
                                        In case of lost and damaged parcels or courier, you must raise the issue with us
                                        immediately. Once we receive your case, we will thoroughly investigate the
                                        matter and follow up as soon as possible. This process will require a minimum of
                                        5 working days.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseFour" aria-expanded="true"
                                            aria-controls="headingFour">
                                            What type of products are not covered by the compensation/ refund system?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample5">
                                    <div class="card-body">
                                        Refund is not applicable if the product is damaged due to packaging
                                        inadequacies. If liquid items, such as ghee, honey, oil or any glass bottles are
                                        damaged, the refund system will not apply to them.
                                        In addition, for home delivery service, if the product being delivered is faulty
                                        to begin with, the customer has to check in front of the rider and return it.
                                        Complaints of any kind after successful home delivery or return are not
                                        acceptable.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title">
                            <h2>Enterprise</h2>
                        </div>
                        <div class="accordion" id="accordionExample6">
                            <div class="card">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseTen" aria-expanded="true"
                                            aria-controls="collapseTen">
                                            What’s the course of action from your organization if any of my products
                                            gets missing, damaged or stolen after handing over the products?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample6">
                                    <div class="card-body">
                                        We’ll take full responsibility & ensure a viable solution as per our
                                        compensation policy.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false"
                                            aria-controls="collapseEleven">
                                            How safe & secure is it for us to work with your organization?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample6">
                                    <div class="card-body">
                                        Our enlisted trucks are verified with documents. We ensure safety but in case of
                                        any unexpected situation, we will provide all necessary information about the
                                        cargo & its vendor.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="coverage" style="display: none;">
                        <div class="top_title">
                            <h2>Small business</h2>
                        </div>
                        <div class="accordion" id="accordionExample8">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            How much time is required to deliver a parcel?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        Our delivery time frame after picking up a parcel is as follows:
                                        Inside Dhaka - Next day
                                        Dhaka Suburb - 48 hours
                                        Outside Dhaka - 72 Hours
                                        Please note that delivery time may vary depending on external conditions or
                                        customer availability.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            What is RaftCourier’s pickup time?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        Our regular pickup time for home delivery service in Dhaka and all other
                                        districts is 3pm to 8pm.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            Does RaftCourier provide parcel delivery in Dhaka only?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        When we say RaftCourier provides home delivery service in Bangladesh, we mean
                                        that we
                                        deliver all across the country, to every doorstep. If the destination is part of
                                        the coverage area, we will promptly deliver it.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseFour" aria-expanded="true"
                                            aria-controls="headingFour">
                                            What is your coverage area?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        We deliver your products in all of the 64 districts of Bangladesh. You can place
                                        an order by contacting your nearest RaftCourier Delivery hub. Click here to see
                                        details
                                        on all hubs.
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseFive" aria-expanded="true"
                                            aria-controls="headingFive">
                                            What is the cut off time for requesting a pickup?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        In order to make sure that your parcels are picked up successfully, you must
                                        input the details of the parcels in either our website or mobile app within 3pm.
                                        Once a request is placed, we will send our pickup agent to your doorstep the
                                        same day to pick up your parcels. Please note that you must place at least one
                                        request within 3pm for our agent to get notified and arrive at your doorstep for
                                        pickup.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseSix" aria-expanded="true"
                                            aria-controls="headingSix">
                                            Do you pick up parcels from outside Dhaka?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        We are not a parcel delivery service in Dhaka only, our home delivery courier
                                        service in Bangladesh extends to all 64 districts. We pick up parcels from all
                                        over Bangladesh. To know more please call us at 01844-488983.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true"
                                            aria-controls="headingSeven">
                                            Where can I find information about my nearest RaftCourier Delivery hub?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample8">
                                    <div class="card-body">
                                        To find your nearest RaftCourier Delivery hub click here.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center faq_law">
            <div class="col-12">
                <h1>আইনগত নীতিমালা সম্পর্কে আরও জানুন</h1>
            </div>
            <div class="col-md-6 col-12">
                <div class="faq_bottom">
                    <div class="left">
                        <i class="far fa-file-alt"></i>
                    </div>
                    <div class="right">
                        <div class="top">
                            <h2>নিয়ম ও শর্তাবলী</h2>
                            <p>অনুগ্রহ করে প্ল্যাটফর্মটি ব্যবহার করার আগে নিয়ম ও <br> শর্তাবলী মনোযোগ দিয়ে পড়ুন</p>
                        </div>
                        <div class="bottom">
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="faq_bottom">
                    <div class="left">
                        <i class="fas fa-user-shield"></i>
                    </div>
                    <div class="right">
                        <div class="top">
                            <h2>প্রাইভেসি পলিসি</h2>
                            <p>আমাদের নীতিমালা সঠিকভাবে পর্যালোচলনা করলে <br> আপনি শান্তিপূর্ণভাবে অ্যাপটি ব্যবহার করতে
                                পারবেন</p>
                        </div>
                        <div class="bottom">
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


@endsection


@section('custom-js')
<script>
    $(document).ready(function () {

        $('#general_id').click(function () {
            $(".general").show();
            $(".payment").hide();
            $(".return").hide();
            $(".coverage").hide();
        });

        $('#payment_id').click(function () {
            $(".general").hide();
            $(".payment").show();
            $(".return").hide();
            $(".coverage").hide();
        });
        $('#return_id').click(function () {
            $(".return").show();
            $(".general").hide();
            $(".payment").hide();
            $(".coverage").hide();
        });
        $('#coverage_id').click(function () {
            $(".coverage").show();
            $(".general").hide();
            $(".return").hide();
            $(".payment").hide();
        });





    });

    $(document).on('click' , 'ul li' , function(){
        $(this).addClass('active').siblings().removeClass('active')
    });

</script>
@endsection
