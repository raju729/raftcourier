@extends('front.raftmaster')
@section('main')


<div id="coverage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="cov_heading">
                    <div class="left">
                        <h1>Inside Dhaka</h1>
                        <select name="" id="">
                            <option value="inside">Inside Dhaka</option>
                            <option value="suburb">Dhaka Suburb</option>
                            <option value="outside">Outside Dhaka</option>
                        </select>
                        <input type="text" placeholder="area or district" name="">
                    </div>
                    <div class="right">
                        <a href="#"><i class="fas fa-download"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="cov_table" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">District</th>
                                <th scope="col">Area</th>
                                <th scope="col">Post Code</th>
                                <th scope="col">Home Delivery</th>
                                <th scope="col">Lockdown</th>
                                <th scope="col">Charge(1kg)</th>
                                <th scope="col">Charge(2kg)</th>
                                <th scope="col">Charge(3kg)</th>
                                <th scope="col">COD Charge</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>    
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>
                            <tr>                              
                                <td>Dhaka</td>
                                <td>Mohammadpur(Dhaka)</td>
                                <td>1207</td>
                                <td>Yes</td>
                                <td>No</td>
                                <td>60</td>
                                <td>75</td>
                                <td>90</td>
                                <td>0%</td>
                            </tr>                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
