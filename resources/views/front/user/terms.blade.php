@extends('front.raftmaster')
@section('main')


<div id="terms">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading">
                    <h1>Terms and conditions</h1>
                </div>
                <div class="bottom">

                    <div class="items">
                        <h4>1. Introduction</h4>
                        <p>RaftCurier is a company incorporated under the laws of Bangladesh, (hereinafter referred to as
                            “RaftCurier”) and offers diversified delivery and pick-up services with a focus on e-commerce
                            transactions. This agreement (“Agreement”) is an electronic contract between You and RaftCurier,
                            and outlines the terms on which RaftCurier will provide services to You.

                            You acknowledge that clicking the “I accept” button, and subsequently accessing and using
                            RaftCurier’s services, constitutes an express acceptance of this electronic contract between RaftCurier
                            and Yourself.

                            You are alternatively referred to as “Client”. Both You and RaftCurier are hereafter individually
                            referred to as a “Party”, and collectively as the “Parties”..</p>
                    </div>

                    <div class="items">
                        <h4>2. SCOPE OF DELIVERY SERVICES</h4>
                        <p>The Client may access and use any of the following services provided by RaftCurier (cumulatively,
                            the Fulfilment Services)</p>
                        <ul>
                            <li>The delivery of goods from the Client to its customer (Forward Leg Deliveries), which
                                may include the logistics for picking up invoiced goods from the Client’s designated
                                premises bringing it to the RaftCurier’s facility, and eventual dispatch to the Client’s
                                customer</li>
                            <li>The pick-up of goods from the Client’s customers and delivery to the Client’s designated
                                address (Reverse Pickups)</li>
                            <li>Cash handling for Forward Leg Deliveries which involve cash collection on delivery of
                                the product to the Client’s customers (CoD Services)</li>
                        </ul>
                        <p>A list of pin codes of the areas within the territory which is serviced by RaftCurier is provided
                            in Schedule 2, along with the timelines within which Fulfilment Services will be completed
                            for these areas. RaftCurier shall provide the Fulfilment Services only for these specified
                            locations. RaftCurier may, at any time and at its sole discretion, update the Serviceable Area
                            List, without prior notice to the Client.</p>
                    </div>

                    <div class="items">
                        <h4>3. CLIENT OBLIGATIONS</h4>
                        <p>The Client shall ensure that the packaging of the Products is safe, proper and adequate to
                            withstand the normal transportation and environmental hazards, along with the necessary
                            invoices and documentation, and in compliance with applicable laws.</p>
                        <p>The Client shall ensure that the goods for delivery are kept ready for pick-up by RaftCurier
                            personnel at the designated pick-up time, failing which the RaftCurier personnel shall not wait at
                            your location, and you may be charged the requisite fee for the delivery.</p>
                        <p>The Client shall ensure that the goods inside the package match the description of the goods
                            on the outer packaging / documentation, and that proper, true, fair, correct and factual
                            declarations are made on the docket regarding description and value of products / goods
                            handed over for the Fulfilment Services.</p>
                        <p>The Client shall not book/handover or allow to be handed over any Product to RaftCurier (or its
                            fulfilment personnel) which is banned, restricted, illegal, prohibited, stolen, infringing
                            of any third-party rights, hazardous or dangerous or in breach of any tax laws currency,
                            bullion, letters and financial and security instruments (“Banned Goods”).</p>
                        <p>Where requested by RaftCurier, the Client shall, at its own cost, provide forecasting reports, in
                            the format and at the frequencies suggested by RaftCurier, which forecast the potential
                            requirement of Fulfilment Services by the Client. In addition, from time to time, RaftCurier may
                            request additional information to be provided by the Client on an ad hoc or periodic basis,
                            which may relate to the nature and volume of the consignments for which the Client shall
                            require Fulfilment Services.</p>
                        <p>Any dispute to be raised by the Client with regard to the Fulfilment Services provided by
                            RaftCurier must be raised within a period of 20 (twenty) days from the conclusion of the Forward
                            Leg Delivery or Return, the Conclusion Point determined in accordance under the section RaftCurier
                            OBLIGATION, paragraph 5, failing which the Client shall be barred from subsequently raising
                            such a dispute.</p>
                    </div>

                    <div class="items">
                        <h4>4. RaftCurier OBLIGATIONS</h4>
                        <p>RaftCurier shall provide the Fulfilment Services with reasonable skill and care, and in accordance
                            with applicable laws.</p>
                        <p>RaftCurier shall be solely responsible for selecting, hiring, assigning, and supervising its
                            fulfilment personnel, in order to ensure the provision of the Fulfilment Services in
                            accordance with the terms of this Agreement.</p>
                        <p>RaftCurier shall be solely responsible for the payment of salaries, wages, and other statutory
                            payments including without limitation provident fund contributions and gratuity, of its
                            fulfilment personnel, and the Client shall have no liability towards this.</p>
                        <p>If the Client raises an objection with regard to the behaviour or performance of any RaftCurier
                            fulfilment personnel, RaftCurier, upon investigating the matter and where it deems fit, at its
                            sole discretion, shall ensure that such fulfilment personnel shall not service the Client
                            thereafter in any manner; provided that there shall be no obligation to take any further
                            action in this regard, except as mandated under applicable law</p>
                        <p>RaftCurier shall ensure that:</p>
                        <ul>
                            <li>Upon successful delivery of a package to the Client’s customer, an SMS notification is
                                sent to the Client’s customer</li>
                        </ul>
                        <p>It is agreed that the time of the delivery of such SMS shall be the point of conclusion of
                            the service (“Conclusion Point”), and that in the absence of any valid dispute raised by the
                            Client within the timelines set out in Para 6 under client obligation, such SMS shall be
                            final and conclusive proof of the completion of the Fulfilment Services with respect to that
                            particular Client consignment.</p>
                        <p>The remittance of cash collected as part of the COD Services to the Client’s designated
                            account shall be conducted by RaftCurier from Sunday to Thursday of each week, through bank
                            account transfer or through BKASH, as may be agreed with the Client.</p>
                    </div>

                    <div class="items">
                        <h4>5.RIGHTS OF RaftCurier</h4>
                        <p>RaftCurier reserves the right, to be exercised by its personnel, to reject any product or package
                            provided by the Client, for any reason to be intimated to the Client, including the
                            following</p>
                        <ul>
                            <li>Inadequate packaging</li>
                            <li>Lack of invoices or necessary documentation</li>
                            <li>Delivery location not within the Serviceable Area List</li>
                            <li>Provision of Banned Goods</li>
                            <li>Value of the product exceeding specified thresholds, and the lack of insurance cover on
                                the product</li>
                        </ul>
                        <p>RaftCurier may exercise a lien and withhold and set off any monies collected from customers of the
                            Client, as part of the CoD Services, against any pending dues from the Client, which remain
                            outstanding beyond the due date of payment as set out under the section FEES AND PAYMENT
                            TERMS para 3</p>
                    </div>

                    <div class="items">
                        <h4>6. REPRESENTATIONS AND WARRANTIES</h4>
                        <p>RaftCurier hereby represents, warrants and assures that:</p>
                        <ul>
                            <li>It has full power, absolute authority and is competent to provide the Fulfilment
                                Services and fulfil its obligations under this Agreement, including necessary licenses,
                                permits and approvals under applicable law; and</li>
                            <li>It employs and maintains sufficient number of adequately trained personnel who will
                                provide the Fulfilment Services.</li>
                        </ul>
                        <p>The Client hereby represents, warrants and assures that:</p>
                        <ul>
                            <li>The following items with regard to the Client are in good order and in compliance with
                                all applicable law and regulations: the organization and standing; conduct of legitimate
                                business activities, corporate power and authorization; governmental approvals;
                                proprietary information agreements with employees; and that there has been no
                                restriction by any regulatory authority in conducting the business.</li>
                            <li>It has all the requisite rights and licenses over the goods or any other content
                                provisioned to RaftCurier for undertaking of the Fulfilment Services, and in furtherance to
                                this the Client hereby assures RaftCurier that such activity undertaken on behalf of the
                                Client by RaftCurier shall not violate any right / licenses of any third party.</li>
                            <li>It shall ensure that the goods or any other content provided to RaftCurier shall not contain
                                any Banned Goods, or material which is offensive / derogatory / explicit / perverse to
                                any specific race, gender or class of persons or degrading to public conscience or
                                morals, and do not breach any applicable law in any manner.</li>
                            <li>It shall ensure that it complies with all the regulatory requirements pertaining to all
                                types of taxes, levies etc. (including but not limited to VAT, service taxes, octroi,
                                local body taxes, etc.) as applicable from time to time. The Client will be solely
                                responsible for any penalties levied by any regulatory authority due to non-compliance
                                with the regulatory requirements by the Client.</li>
                            <li>It shall be solely responsible for any confiscation/seizure/disposal of goods by any
                                regulatory authority on account of any violation of the law in respect of the goods or
                                due to its failure to comply with the applicable regulatory requirements for the
                                stocking, sale, or movement of such goods during the provision of the Fulfilment
                                Services by RaftCurier, even if RaftCurier has been advised of the same.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>7. FEES AND PAYMENT TERMS</h4>
                        <p>The fee payable to RaftCurier for the Fulfilment Services (“Fees”) shall be as set out in the
                            Schedule to this Agreement.</p>
                        <p>The Client shall pay all applicable taxes on the Fees, and it is clarified that the amounts
                            set out in the Schedule to this Agreement, as the Fees for each component of the Fulfilment
                            Services, are exclusive of taxes.</p>
                        <p>RaftCurier shall issue invoice to the Client at end of each calendar month for the Fulfilment
                            Services that have been rendered in previous month, with the terms of this Agreement, which
                            shall be paid by the Client within fifteen (15) days from the date of submission of the
                            invoice.</p>
                        <p>The Client must provide notice of any dispute with regard to an invoice (or part thereof)
                            submitted by RaftCurier, within 7 (seven) days of receipt of the invoice, failing which the
                            invoice shall be deemed to be undisputed and payable in full within the timeline set out in
                            Clause 5.2.</p>
                    </div>

                    <div class="items">
                        <h4>8. RaftCurier SYSTEMS INTEGRATIONS AND TECHNICAL SUPPORT</h4>
                        <p>RaftCurier shall provide an online administrative panel, on its website, for the use of the Client,
                            to organize and communicate its requirements for Fulfilment Services, and oversee the
                            execution of the same.</p>
                        <p>The online tools and panels (“RaftCurier Digital Resources”) that are provided by RaftCurier to the
                            Client are provided on an ‘as is’ basis, without any warranty of any kind including fitness
                            for purpose, and any use or integration of the same made by the Client shall be at its own
                            and sole risk.</p>
                        <p>All rights, title, interest and intellectual property in the RaftCurier Digital Resources shall
                            remain the sole and exclusive property of RaftCurier, and the Client shall have only a limited,
                            terminable, non-exclusive license to use the same for the term of this Agreement.</p>
                    </div>

                    <div class="items">
                        <h4>9. LIMITATION OF LIABILITY</h4>
                        <p>Where a consignment of goods / products of the Client, that is undelivered to Client or the
                            Client’s customer, as the case may be, for 30 days from the date of hand-over to RaftCurier, such
                            consignment shall be treated as “Lost”.</p>
                        <p>In relation to Lost or damaged consignments in a Forward Leg Delivery, where such loss or
                            damage is caused by the fault of RaftCurier personnel after the handover of the consignment to
                            RaftCurier, the Client agrees that the liability of RaftCurier shall be limited to the invoice value or
                            BDT 5000 (Five Thousand), whichever is less.</p>
                        <p>In relation to Lost consignments in a Reverse Pick-up, the Client agrees that the liability
                            of RaftCurier shall be limited the invoice value or BDT 5000 (Five Thousand), whichever is less;
                            it is agreed that such liability shall exist only in the event of ‘Lost’ cases, and there
                            shall be no liability for damaged consignments in the case of Reverse Pick-ups.</p>
                        <p>For Lost or damaged consignments with a value greater than BDT 5000 (Five Thousand), RaftCurier
                            shall provide a scan copy of certificate of facts (“COF”) within 10 (ten) working days of a
                            receipt of a request from the Client; RaftCurier shall have no liability beyond BDT 5,000 (Five
                            Thousand), provided that if the COF provided by RaftCurier is not accepted by the insurance
                            provider of the Client, then RaftCurier shall provide a revised COF within 14 (fourteen) working
                            days.</p>
                        <p>It is agreed that in no event shall the liability of RaftCurier, for any loss or damage or other
                            event that occurs either during a Forward Leg Delivery or a Reverse Pick-up, exceed BDT 5000
                            (Five Thousand).</p>
                        <p>In no event or under any circumstance shall RaftCurier be liable to the Client or to any person
                            claiming under or through it in contract, tort or otherwise for indirect, special,
                            incidental, exemplary, punitive, or consequential damages of any kind whatsoever even if
                            advised of the possibility of such damages.</p>
                    </div>

                    <div class="items">
                        <h4>10. CONFIDENTIALITY</h4>
                        <p>Both Parties shall keep confidential (and to ensure that its officers, employees, agents,
                            affiliates and professional and other advisers keep confidential) any Confidential
                            Information. Both Parties shall not, and shall procure that none of their directors,
                            officers, employees, agents, affiliates or professional advisers shall not, use Confidential
                            Information for any purpose other than for the provision of Delivery Services and for
                            performance under this Agreement.</p>
                        <p>Confidential Information shall mean any and all technical and non-technical information,
                            which either Party may have acquired before or after the date of this Agreement in relation
                            to the business and operations of the other Party, and any other information designated as
                            confidential by a Party from time to time. It is expressly clarified that the RaftCurier Digital
                            Resources are a part of RaftCurier’s Confidential Information, and may not be disclosed to third
                            parties by the Client.</p>
                    </div>

                    <div class="items">
                        <h4>11. INDEMNITY</h4>
                        <p>The Client agrees to indemnify, defend and hold RaftCurier harmless from and against claims,
                            demands, actions, liabilities, costs, interest, damages and expenses of any nature
                            whatsoever (including all legal and other costs, charges and expenses) incurred or suffered
                            by RaftCurier, arising out of any (a) any wrongful or negligent act or omission of the Client or
                            any person engaged by the Client; (b) any breach of the Client’s obligations under this
                            Agreement; (c) any third party action or claim made against the Client; and (d) a violation
                            of applicable law by the Client. The rights, powers, privilege and remedies provided in this
                            indemnity are cumulative and not exclusive of any rights, powers, privileges or remedies
                            provided by law.</p>
                    </div>

                    <div class="items">
                        <h4>12. MISCELLANEOUS</h4>
                        <p>Assignment. Neither Party shall assign this Agreement or any of its rights and obligations
                            hereunder, without the prior written consent of the other Party and any such attempted
                            assignment shall be null and void, provided that RaftCurier shall be permitted to assign any of
                            the rights and obligations under this Agreement to any of its affiliates/group
                            companies/subsidiaries, by providing prior written notice of 24 (twenty-four) hours to the
                            Client.</p>
                        <p>Force Majeure. In the event either Party (“Prevented Party”) is prevented from performing its
                            obligations under this Agreement by force majeure, such as earthquake, typhoon, flood,
                            public commotion, torrential rains, heavy winds, storms or other acts of nature, fire,
                            terrorist acts, threatened terrorists acts, explosion, acts of civil or military authority
                            including the inability to obtain any required approvals or permits, strikes, riots, war,
                            plagues, other epidemics, or other unforeseen events beyond the Prevented Party’s reasonable
                            control (“Event of Force Majeure”), the Prevented Party shall notify the other party without
                            delay and within fifteen (15) days thereafter shall provide detailed information concerning
                            such event and documents evidencing such event, explaining the reasons for its inability to
                            execute, or for its delay in the execution of, all or part of its obligations under this
                            Agreement. If an Event of Force Majeure occurs, neither party shall be responsible for any
                            damage, increased costs or loss which the other party may sustain by reason of such a
                            failure or delay of performance, and such failure or delay shall not be deemed a breach of
                            this Agreement. The Prevented Party shall take reasonable means to minimise or remove the
                            effects of an Event of Force Majeure and, within the shortest reasonable time, attempt to
                            resume performance of the obligations delayed or prevented by the Event of Force Majeure.
                        </p>
                        <p>Law and Jurisdiction. This Agreement, the construction and enforcement of its terms and the
                            interpretation of the rights and duties of the Parties hereto shall be governed by the laws
                            of Bangladesh, and the courts in Dhaka shall have exclusive jurisdiction over any disputes
                            that arise out of or are related to the subject matter in this Agreement.</p>
                        <p>Compliance. Each Party hereto agrees that it shall comply with all applicable local laws,
                            ordinances and codes in performing its obligations hereunder. If at any time during the Term
                            of this Agreement, a Party is informed or information comes to its attention that it is or
                            may be in violation of any law, ordinance, regulation, or code (or if it is so decreed or
                            adjudged by any court, tribunal or other authority having competent jurisdiction), that
                            Party shall immediately take all appropriate steps to remedy such violation and comply with
                            such law, regulation, ordinance or code in all respects. Further, each Party shall establish
                            and maintain all proper records (particularly, but without limitation, accounting records)
                            required by any law, code of practice or corporate policy applicable to it from time to
                            time.</p>
                        <p>Severability. In the event any one or more of the provisions of this Agreement shall, for any
                            reason, be held to be invalid, illegal or unenforceable, the remaining provisions of this
                            Agreement shall be unaffected, and the invalid, illegal or unenforceable provision(s) shall
                            be replaced by a mutually acceptable provision(s), which being valid, legal and enforceable,
                            comes closest to the intention of the Parties underlying the invalid, illegal or
                            unenforceable provision(s).</p>
                    </div>

                    <div class="items">
                        <h4>13. SCHEDULE - 1 Fee Chart for Fulfilment Services</h4>
                        <div class="term_table" style="overflow-x:auto;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th scope="col">Details</th>
                                    <th scope="col">Upto 1kg</th>
                                    <th scope="col">Upto 2kg</th>
                                    <th scope="col">Upto 3kg</th>
                                    <th scope="col">Upto 4kg</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>                                 
                                    <td>Dhaka City</td>
                                    <td>TK. 60</td>
                                    <td>TK. 75</td>
                                    <td>TK. 90</td>
                                    <td>TK. 105</td>
                                </tr>
                                <tr>                              
                                <td>Dhaka Suburb</td>
                                    <td>TK. 100</td>
                                    <td>TK. 115</td>
                                    <td>TK. 130</td>
                                    <td>TK. 145</td>
                                </tr>
                                <tr>                              
                                <td>Outside Dhaka</td>
                                    <td>TK. 130</td>
                                    <td>TK. 160</td>
                                    <td>TK. 190</td>
                                    <td>TK. 220</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                       
                    </div>

                    <div class="items">
                        <h4>14. SCHEDULE - 2 Serviceable Area List</h4>
                        <p>Please click <a href="#">here</a> to get the list of serviceable areas of RaftCurier</p>
                    </div>

                    <div class="terms_btn">
                        <a href="#">Go Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
