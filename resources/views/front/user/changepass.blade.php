@extends('front.raftmaster')
@section('main')

<div id="change_pass">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-11 col-md-7 text-center">
                <div class="top_part">
                    <h1>Change password</h1>
                    <p>A strong password will help prevent someone from guessing it and gaining access to your account.
                        Choose one 10 or more characters longs and that contains a mix of lowercase, uppercase, numeric
                        and symbolic characters.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-md-5 text-center">
                <form id="basic-form" action="{{route('user.changepass.submit')}}" method="post">
                    @csrf

                    <div class="form-label-group input-group eye_design">
                        <input type="password" id="password" name="curr_pass" class="form-control"
                            placeholder="Current Password" required>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i id="eye" class="far fa-eye-slash" onclick="showHidePwd();"></i>
                            </span>
                        </div>
                    </div>

                    <div class="form-label-group input-group eye_design">
                        <input type="password" id="txtNewPassword" name="pass" class="form-control"
                            placeholder="New Password" required>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i id="eye1" class="far fa-eye-slash" onclick="showHidePwd1();"></i>
                            </span>
                        </div>
                    </div>

                    <div class="form-label-group input-group eye_design">
                        <input type="password" id="txtConfirmPassword" name="pass" class="form-control" required
                            name="confpass" placeholder="Retype New Password">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i id="eye2" class="far fa-eye-slash" onclick="showHidePwd2();"></i>
                            </span>
                        </div>
                    </div>
              
                    <p class="registrationFormAlert" style="color:green; margin: 10px 0px;" id="CheckPasswordMatch">
                    </p>
                    <button class="my-4" type="submit">Save</button>
                    <span>পাসওয়ার্ড ভুলে গেছেন ?<a class="ml-2" href="#">রিসেট পাসওয়ার্ড</a></span>
                </form>
            </div>
        </div>
    </div>
</div>




@endsection

@section('custom-js')
<script>
    function showHidePwd() {
        var input = document.getElementById("password");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye").className = "far fa-eye-slash";
        }
    }

    function showHidePwd1() {
        var input = document.getElementById("txtNewPassword");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye1").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye1").className = "far fa-eye-slash";
        }
    }

    function showHidePwd2() {
        var input = document.getElementById("txtConfirmPassword");
        if (input.type === "password") {
            input.type = "text";
            document.getElementById("eye2").className = "far fa-eye";
        } else {
            input.type = "password";
            document.getElementById("eye2").className = "far fa-eye-slash";
        }
    }



    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();
        if (password != confirmPassword)
            $("#CheckPasswordMatch").html("Passwords does not match!");
        else
            $("#CheckPasswordMatch").html("Passwords match.");
    }
    $(document).ready(function () {
        $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
@endsection
