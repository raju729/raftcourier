@extends('front.raftmaster')
@section('main')


<div id="terms">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading">
                    <h1>Privacy policy</h1>
                </div>
                <div class="bottom">

                    <div class="items">
                        <h4>1. Introduction</h4>
                        <p>Welcome to RaftCurier!This Privacy Policy was written to help you better understand how we collect,
                            use and store your information. Since technology and privacy laws are always changing, we
                            may occasionally update this policy. If a significant change is made, we will be sure to
                            post a notice on our home page and in the merchant admin. If you continue to use RaftCurier after
                            these changes are posted, you agree tothe revised policy.By signing up for any of the
                            products or services offered by RaftCurier (together, the “Services”), or dealing with a merchant
                            using RaftCurier Services, you are agreeing to the terms of this Privacy Policy and, as
                            applicable, the RaftCurier Terms of Service. This policy is a legally binding agreement between
                            you (and your client, employer or another entity if you are acting on their behalf) as the
                            user ofthe Services (referred to as “you” or “your”) and RaftCurier Inc. (referred to as “we”,
                            “our”, “us” or “RaftCurier”). If we add any new features or tools to our Services, they will also
                            be subject to this policy.We will keep your Personal Information accurate, complete and
                            up-to-date with the information that you provide to us. If you request access to your
                            Personal Information, we will inform you of the existence, use and disclosure of your
                            Personal Information as allowed by law, and provide you access to that information. When we
                            use the term “Personal Information” in this policy, it means any information related to an
                            identifiable individual, but does not include the name, title, business address, or
                            telephone number of an employee of an organization.</p>
                    </div>

                    <div class="items">
                        <h4>2. Information from merchants</h4>
                        <p>Privacy matters! If you are a merchant, you agree to post a privacy policy on your storefront that complies with the laws applicable to your business. You also agree to obtain consent from your customers for the use and access of their Personal Information by RaftCurier and other third parties. What information do we collect from merchants and why?</p>
                        <ul>
                            <li>We collect your name, company name, address, email address, phone number(s) and credit card details.oWe need this information to provide you with our Services, for example, to confirm your identity, contact you, and invoice you.</li>
                            <li>We collect data about the RaftCurier -hosted webpages that you visit and how and when you access your account, including information about the device and browser you use, your network connection and your IP address.oWe need this information to give you access to and improve our Services.</li>
                            <li>We collect Personal Information about your customers that you share with us orthat customers provide while shopping or during checkout.</li>
                            <li>We use this information to provide you with our Services and so that you canprocess orders and better serve your customers.</li>
                            <li>We will also use Personal Information in other cases where you have given us your express permission. When do we collect this information?</li>
                            <li>We collect Personal Information when you sign up for our Services, when you access our Services or otherwise provide us with the information.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>3. Information from our merchants’ customers</h4>
                        <p>What information do we collect and why?</p>
                        <ul>
                            <li>We collect our merchants’ customers’ name, email, shipping and billing address, payment details, company name, phone number, IP address and devicedata.</li>
                            <li>We need this information to provide merchants with our Services, including supporting and processing orders, authentication, and processing payments. This information is also used to improve our Services.</li>
                            <li>When do we collect this information?</li>
                            <li>Information is collected when a merchant’s customer uses or accesses our Services, such as when a customer visits a merchant’s site, places an order or signs up for an account on a merchant’s site.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>4. Information from Partners</h4>
                        <p>Partners are individuals or businesses that have agreed to the terms of the Partner Program to work with RaftCurier to promote the Services by (a) referring clients to RaftCurier; (b) developing RaftCurier store themes for merchant use; or (c) developing apps using the RaftCurier Application Interface (API) for merchant use. What information do we collect from Partners and why?</p>
                        <ul>
                            <li>We collect your name, company name, website, twitter or other social media handles, phone number(s), address, business type, and email address.</li>
                            <li>We use this information to work with you, confirm your identity, contact you,and pay you.</li>
                            <li>We collect data about the RaftCurier -hosted webpages that you visit and how and when you access your account, including information about the device and browser you use, your network connection and your IP address.oWe use this information to give you access to and improve our Services.</li>
                            <li>We collect Personal Information about your customers that you share with us orthat they provide to us directly.</li>
                            <li>We use this information to work with you and to provide our Services to yourcustomers.</li>
                            <li>We will also use Personal Information in other cases where you have given us express permission. When do we collect this information?</li>
                            <li>We collect this information when you sign up for a Partner Account, when you sign up one of your customers for our Services, or when your customers sign up themselves. We also collect any additional information that you might provide to us.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>5. Information from RaftCurier visitors and support users. What information do we collect and why?</h4>
                        <p>From RaftCurier website visitors, we collect information about the device and browser you use, your network connection and your IP address.</p>
                        <ul>
                            <li>From telephone support users, we collect your phone number and call audio.</li>
                            <li>From chat support users, we collect your name, email address, information about the device and browser you use, your network connection and your IP address.</li>
                            <li>From forum users, we collect your name, email address and website URL. We use this information to service your account, enhance our Services, and answer any questions you may have. When do we collect this information?·We collect this information when you visit RaftCurier -hosted pages or engage with us either by email, web form, instant message, phone, or post content on our website (including forums & blogs). We also collect any additional information that you might provide to us.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>6. Information from cookies</h4>
                        <p>What is a cookie? A cookie is a small amount of data, which may include a unique identifier. Cookies are sent to your browser from a website and stored on your device. Every device that accesses our website is assigned a different cookie by us. Why does RaftCurier use cookies?</p>
                        <ul>
                            <li>We use cookies to recognize your device and provide you with a personalized experience</li>
                            <li>We also use cookies to serve customized ads from Google and other third-partyvendors.</li>
                            <li>Our third-party advertisers use cookies to track your prior visits to our website and elsewhere on the Internet in order to serve you customized ads.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>7. When and why do we share Personal Information with third parties?</h4>
                        <ul>
                           <li>RaftCurier works with third parties to help provide you with our Services and we may share Personal Information with them to support these efforts. In certain limited circumstances, we may also be required by law to share information withthird parties.</li>
                           <li>Personal information may be shared with third parties to prevent, investigate, or take action regarding illegal activities, suspected fraud, situationsinvolving potential threats to the physical safety of any person, violations of our Terms of Service or any other agreement related to the Services, or as otherwiserequired by law.</li>
                           <li>Personal information may also be shared with a company that acquires our business, whether through merger, acquisition, bankruptcy, dissolution, reorganization, or other similar transaction or proceeding. If this happens, we will post a notice on our home page.</li>
                           <li>Except when required by law, RaftCurier will never disclose your Personal Information without obtaining your consent.</li>
                        </ul>
                    </div>

                    <div class="items">
                        <h4>8. What do we do with your Personal Information when you terminate your relationship with us?</h4>
                        <p>We will continue to store archived copies of your Personal Information for legitimate business purposes and to comply with the law.·We will continue to store anonymous or anonymized information, such as website visits, without identifiers, in order to improve our Services.</p>                    
                    </div>

                    <div class="items">
                        <h4>9. What we don’t do with your Personal Information?</h4>
                        <ul>
                            <li>We do not and willnevershare, disclose, sell, rent, or otherwise provide Personal Information to other companies for the marketing of their own productsor services.</li>
                            <li>We do not use the Personal Information we collect from you or your customers to contact or market to your customers or directly compete with you. However, RaftCurier may contact or market to your customers if we obtain their information from another source, such as from the customers themselves.</li>
                        </ul>                    
                    </div>

                    <div class="items">
                        <h4>10. How do we keep your Personal Information secure?</h4>
                        <ul>
                            <li>We follow industry standards on information security management to safeguard sensitive information, such as financial information, intellectual property, employee details and any other Personal Information entrusted to us. Our information security systems apply to people, processes and information technology systems on a risk management basis.</li>
                            <li>No method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee the absolute security of your Personal Information.</li>
                        </ul>                    
                    </div>

                    <div class="items">
                        <h4>11. Access to your personal information</h4>
                        <p>You retain all rights to your Personal Information and can access it anytime. In addition, RaftCurier takes reasonable steps to allow you to correct, amend or delete personal information that is shown to be inaccurate or incomplete.</p>                 
                    </div>
                    <div class="terms_btn">
                        <a href="#">Go Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




@endsection