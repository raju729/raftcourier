@extends('front.raftmaster')
@section('main')


<div id="notification">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 text-center">
                <div class="heading">
                    <h1>My Notification Settings</h1> <br>
                    <h1>যেই ফোন নম্বর ও ই-মেইলে নোটিফিকেশন পেতে চাচ্ছেন</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-md-5 text-center">
                <form id="basic-form" method="post" action="{{route('user.notification.update',$notif->id)}}">

                    @csrf
                    <input type="number" class="form-control" required name="number" value="{{$notif->number}}"  placeholder="Phone Number">
                    <input type="email" class="form-control"  value="{{$notif->email}}" name="email" placeholder="Email Address">
                    <button class="my-4" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

@section('custom-js')

@endsection
