<?php
use App\Models\Area;
use App\Models\City;
use App\Models\Delivery_charge;
use App\Models\District;
use App\Models\Parcel;
use App\Models\Payment_method;
use App\Models\Rider;
use App\Models\Shop;
use http\Env\Request;
function check_shop()
{
    $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
    if ($shop) {
        return $shop->shop_name;
    }

}
function find_shop_details($id)
{
    $shop = Shop::find($id);
    return $shop;
}
function find_district()
{
    $dist = District::all();
    return $dist;
}
function find_city($id)
{
    $city = City::where('district_id', $id)->get();
    return $city;
}
function find_area($id)
{
    $area = Area::where('area_id', $id)->get();
    return $area;
}
function find_riders($id)
{
    $parcel = Parcel::where('track_id', $id)->pluck('delivery_area');
    $rider = Rider::where('area_id', $parcel)->get();
    return $rider;
}
function find_rider_id()
{
    $rider = Rider::where('user_id', Auth::user()->id)->select('id')->first();
    return $rider->id;
}

function check_rider()
{
    $rider = Rider::where('user_id', Auth::user()->id)->first();
    return $rider->name;
}

function find_rider_details($id)
{
    $rider = Rider::find($id);
    return $rider;
}

function get_area_name($id)
{
    $area = Area::where('id', $id)->first();
    return $area->name;
}
function count_pay_method()
{
    $exist_method_count = Payment_method::where('shop_id', get_active_shop()->id)->count();
    return $exist_method_count;
}

function rider_details()
{
    $rider = Rider::where('user_id', Auth::user()->id)->first();
    return $rider;
}

function get_active_shop()
{
    $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
    return $shop;
}

function shop_count()
{
    $shops = Shop::where('user_id', Auth::user()->id)->count();
    return $shops;
}
function cash_collection($id)
{
    $d_crg = Delivery_charge::where('track_id', $id)->select('cash_collection')->first();
    return $d_crg->cash_collection;
}

function total_charge($id)
{
    $t_crg = Delivery_charge::where('track_id', $id)->first();
    $total_charge = $t_crg->delivery_charge + $t_crg->cod_charge;
    return $total_charge;
}

if (!function_exists('areActiveNavs')) {
    function areActiveNavs(array $routes, $output = "nav-item-expanded nav-item-open")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }

        }

    }
}

if (!function_exists('areActiveRoutes')) {
    function areActiveRoutes(array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }

        }

    }
}

if (!function_exists('areActiveTrees')) {
    function areActiveTrees(array $routes, $output = "menu-open")
    {

        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }

        }

    }
}

if (!function_exists('userActiveRoute')) {
    function userActiveRoute(array $routes, $output = "current_page_item")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }

        }

    }
}

if (!function_exists('previous')) {
    function previous()
    {
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        return $referer;
    }
}

if (!function_exists('convert_time')) {
    function convert_time($date)
    {
        $mydate = date("Y-m-d H:i:s");
        $datetime1 = date_create($date);
        $datetime2 = date_create($mydate);
        $interval = date_diff($datetime1, $datetime2);
        $min = $interval->format('%i');
        $sec = $interval->format('%s');
        $hour = $interval->format('%h');
        $mon = $interval->format('%m');
        $day = $interval->format('%d');
        $year = $interval->format('%y');
        if ($interval->format('%i%h%d%m%y') == "00000") {
            return $sec . " Seconds";
        } else if ($interval->format('%h%d%m%y') == "0000") {
            return $min . " Minutes";
        } else if ($interval->format('%d%m%y') == "000") {
            return $hour . " Hours";
        } else if ($interval->format('%m%y') == "00") {
            return $day . " Days";
        } else if ($interval->format('%y') == "0") {
            return $mon . " Months";
        } else {
            return $year . " Years";
        }
    }
}

function getip()
{
    $ip = "103.120.160.170"; //"125.62.213.30";//"13.55.129.115";//"103.120.160.170";//$_SERVER['REMOTE_ADDR'];
    return $ip;
}

if (!function_exists('currencyConvertion')) {
    function currencyConvertion($amount)
    {
        $ip = getip();
        $URL = 'http://www.geoplugin.net/php.gp?ip=' . $ip;
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            $data = unserialize($contents);
            $currency = $data['geoplugin_currencyCode'];
            $country_rate = $currency . ' ' . $amount;

            return $country_rate;
        } else {
            return false;
        }

    }
}

if (!function_exists('currentCountry')) {
    function currentCountry()
    {
        $ip = getip();
        $URL = 'http://ipinfo.io/' . $ip;

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            $data = json_decode($contents);
            /* dd($data,1);*/
            return $data->country;
        } else {
            return false;
        }

    }
}

if (!function_exists('currentLoginDetails')) {
    function currentLoginDetails()
    {

        $ip = getip();
        $URL = 'http://ipinfo.io/' . $ip;

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            $data = json_decode($contents);
            return $data;
            dd($data->country, 1);
        } else {
            return false;
        }

        /* $ip = getip();
    $URL = 'http://www.geoplugin.net/php.gp?ip='.$ip;
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    $contents = curl_exec($c);
    curl_close($c);

    if ($contents) {
    $data = unserialize($contents);
    return  $data;
    }
    else return FALSE;*/
    }
}

if (!function_exists('convertUrl')) {
    function convertUrl($url)
    {
        return preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://www.youtube.com/embed/$1", $url);

    }
}
