<?php

namespace App\Http\Controllers\Rider;

use App\Http\Controllers\Admin\ParcellController;
use App\Http\Controllers\Controller;
use App\Models\Delivery_charge;
use App\Models\Parcel;
use App\Models\Parcel_assign;
use App\Models\Parcel_status;

class ParcelController extends Controller
{
    public function pending()
    {
        $parcel = Parcel_assign::where('rider_id', find_rider_id())->where('status', 'ongoing')->pluck('parcel_id');
        $parcels = Parcel::whereIn('id', $parcel)->get();
        return view('rider.parcel.pending', compact('parcels'));

    }
    public function complete()
    {
        $parcel = Parcel_assign::where('rider_id', find_rider_id())->where('status', 'success')->pluck('parcel_id');
        $parcels = Parcel::whereIn('id', $parcel)->get();
        return view('rider.parcel.completed', compact('parcels'));

    }

    public function track_parcel($id)
    {
        $parcel = Parcel::where('track_id', $id)->first();
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $delivery_details = Delivery_charge::where('track_id', $id)->first();
        return view('rider.parcel.track', compact('parcel', 'parcel_status', 'delivery_details'));

    }

    public function change_status($id = null, $stage = null, $extra_value = null)
    {
        $admin_parcel = new ParcellController();
        $admin_parcel->change_status($id, $stage, $extra_value);
        return redirect()->route('rider.track', $id);
    }
}
