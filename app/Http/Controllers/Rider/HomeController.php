<?php

namespace App\Http\Controllers\Rider;

use App\Http\Controllers\Controller;
use App\Models\Parcel_assign;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{
    public function login()
    {
        return view('rider.auth.login');
    }

    public function dashboard()
    {
        $data['pending'] = Parcel_assign::where('rider_id', rider_details()->id)->where('status', 'ongoing')->count();
        $data['complete'] = Parcel_assign::where('rider_id', rider_details()->id)->where('status', 'success')->count();
        $data['wallet'] = rider_details()->wallet;
        $data['income'] = rider_details()->total_income;

        return view('rider.dashboard', compact('data'));
    }

    public function authenticate(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required',

        ]);

        if ($validator->fails()) {

        }
        $user = User::where('phone', $request->phone)->get();
//    dd(count($user) );
        if (count($user) > 0 && count($user) === 1) {
            $user = User::where('phone', $request->phone)->first();
            if (Hash::check($request->password, $user->password)) {
                Auth::login($user, true);
                toast('login Successfull!', 'success');
                return redirect()->route('rider.dashboard');
            } else {
                toast('Password didn`t match!', 'error');
                return redirect()->back();
            }
        } else {
            toast('Phone didn`t match!', 'error');
            return redirect()->back();

        }
    }
    public function changepass()
    {
        return view('front.user.changepass');
    }
    public function changepass_submit(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'curr_pass' => 'required',
            'new_pass' => 'min:6|required_with:re_pass|same:re_pass',

            're_pass' => 'min:6',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }

        if (Hash::check($request->curr_pass, Auth::user()->password)) {
            $merchant_user = User::find(Auth::user()->id);

            $merchant_user->password = Hash::make($request->new_pass);
            if ($merchant_user->save()) {
                toast('password changed successfully', 'success');
                return redirect()->route('user.home');
            } else {
                toast('password change failed', 'error');
                return redirect()->back()->withInput();

            }

        } else {
            toast('password Didn`t match', 'error');
            return redirect()->back()->withInput();
        }
    }

    public function notifications()
    {
        return view('front.user.notifications');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('rider.login');
    }
}
