<?php

namespace App\Http\Controllers;

use App\Models\Pickup;
use App\Models\Shop;
use Auth;
use Illuminate\Http\Request;

class PickupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        $pickups = Pickup::where('shop_id', $shop->id)->get();

        return view('front.pickups.index', compact('pickups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front.pickups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        $pickup = new Pickup();
        $pickup->name = $request->name;
        $pickup->area_id = $request->area;
        $pickup->address = $request->address;
        $pickup->phone = $request->phone;
        $pickup->shop_id = $shop->id;
        if ($pickup->save()) {
            return redirect('pickups');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('front.pickups.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
