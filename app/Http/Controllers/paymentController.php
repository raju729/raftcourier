<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Models\Voucher;
use Auth;

class PaymentController extends Controller
{
    public function index()
    {
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        $payments = Voucher::where('shop_id', $shop->id)->where('status', 'paid')->orderBy('created_at', 'desc')->paginate(10);

        return view('front.payment.index', compact('payments'));
    }
    public function details()
    {
        return view('front.payment.details');
    }

    public function pdf()
    {
        return view('front.payment.pdf');
    }
}
