<?php

namespace App\Http\Controllers;

use App\Models\Payment_method;
use Auth;
use Illuminate\Http\Request;

class PaymethodController extends Controller
{
    public function index()
    {
        $exist_method = Payment_method::where('shop_id', get_active_shop()->id)->first();
        $exist_method_count = Payment_method::where('shop_id', get_active_shop()->id)->count();

        return view('front.payment_methods.index', compact('exist_method', 'exist_method_count'));
    }

    public function create()
    {
        return view('front.payment_methods.create');
    }

    public function store(Request $request)
    {
        $paymethod = new Payment_method();
        $paymethod->user_id = Auth::user()->id;
        $paymethod->shop_id = get_active_shop()->id;
        $paymethod->pay_method = $request->paymethod;
        if ($request->paymethod == "Bank") {
            $details = ['bn' => $request->bank_name, 'an' => $request->acc_name, 'a_nm' => $request->acc_num, 'at' => $request->acc_type,
                'rt' => $request->routing, 'br_n' => $request->branch_name];

        } elseif ($request->paymethod == "Bkash") {
            $details = ['bkash_no' => $request->bkash_number];
        } elseif ($request->paymethod == "Nagad") {
            $details = ['nagad_no' => $request->bkash_number];
        } else {
            dd('Rocket');
        }
        $details = json_encode($details);
        $paymethod->details = $details;
        $paymethod->status = 1;
        if ($paymethod->save()) {
            return redirect()->route('paymethods.index');
        } else {
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $paymethod = Payment_method::find($id);
        return view('front.payment_methods.edit', compact('paymethod'));

    }

    public function update(Request $request, $id)
    {
        $paymethod = Payment_method::findOrFail($id);
        $paymethod->pay_method = $request->paymethod;
        if ($request->paymethod == "Bank") {
            $details = ['bn' => $request->bank_name, 'an' => $request->acc_name, 'a_nm' => $request->acc_num, 'at' => $request->acc_type,
                'rt' => $request->routing, 'br_n' => $request->branch_name];

        } elseif ($request->paymethod == "Bkash") {
            $details = ['bkash_no' => $request->bkash_number];
        } elseif ($request->paymethod == "Nagad") {
            $details = ['nagad_no' => $request->bkash_number];
        } else {
            dd('Rocket');
        }
        $details = json_encode($details);
        $paymethod->details = $details;
        if ($paymethod->save()) {
            return redirect()->route('paymethods.index');
        } else {
            return redirect()->back();
        }

    }
}
