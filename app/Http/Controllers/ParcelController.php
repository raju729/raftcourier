<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Category;
use App\Models\Delivery_charge;
use App\Models\Parcel;
use App\Models\Parcel_status;
use App\Models\Shop;
use App\Models\Weight;
use Auth;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use QRCode;
use Response;

class ParcelController extends Controller
{

    public function view()
    {
        return view('front.parcel.view');
    }
    public function qrcode($track_id)
    {
        $filename = public_path() . '/qrcodes/' . $track_id . '.png';
        // dd($filename);
        $data = QRCode::text($track_id)->setOutfile($filename)->png();
        return $data;
    }
    public function index()
    {
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        $parcels = Parcel::where('shop_id', $shop->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('front.parcel.index', compact('parcels'));

    }
    public function create()
    {
        $weight = Weight::orderBy('created_at', 'desc')->get();
        $area = Area::all();
        $cats = Category::all();
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        return view('front.parcel.create', compact('shop', 'weight', 'area', 'cats'));

    }
    public function store(Request $request)
    {
        $weight = Weight::where('id', $request->product_weight)->select('charge')->first();
        $area = Area::where('id', $request->delivery_area)->select('charge')->first();
        $shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        $delivery_charge = $area->charge + $weight->charge;
        $cash_collection = $request->cash_collection;
        $cod_charge = $request->cash_collection / 100;
        $total_pay = $request->cash_collection - ($delivery_charge + $cod_charge);
        $pickup_address = $shop->pickup_address;
        $track_id = uniqid('RC_');

        $fragile_val = $request->fragile == 'on' ? 1 : 0;
        $liquid_val = $request->liquid == 'on' ? 1 : 0;
        $parcel_type = ['fragile' => $fragile_val, 'liquid' => $liquid_val];
        $parcel_type = json_encode($parcel_type);
        // dd($total_pay);
        //track id unique check dite hobe...

        $parcel = new Parcel();
        $parcel->user_id = Auth::user()->id;
        $parcel->track_id = $track_id;
        $parcel->shop_id = $shop->id;
        $parcel->overall_status = 'Pending';
        $parcel->customer_name = $request->customer_name;
        $parcel->customer_phone = $request->customer_phone;
        $parcel->customer_address = $request->customer_address;
        $parcel->merchant_invoice = $request->merchant_invoice;
        $parcel->delivery_area = $request->delivery_area;
        $parcel->weight = $request->product_weight;
        $parcel->category = $request->product_category;
        $parcel->merchant_invoice = $request->merchant_invoice;
        $parcel->parcel_type = $parcel_type;
        $parcel->pickup_address = $pickup_address;
        $parcel->notes = $request->notes;
        $parcel->sell_price = $request->sell_price;
        if ($parcel->save()) {
            $this->delivery_charge($parcel->track_id, $cash_collection, $cod_charge, $total_pay, $delivery_charge);
            $this->qrcode($parcel->track_id);
            if ($parcel->id) {
                $parcel_view = Parcel::where('id', $parcel->id)->first();
                $parcel_shop = Shop::where('id', $parcel_view->shop_id)->first();
                return view('front.parcel.view', compact('parcel_view', 'parcel_shop'));
            }
        } else {
            # code...
        }

    }
    public function label_download($id)
    {
        $parcel_view = Parcel::where('id', $id)->first();
        $parcel_shop = Shop::where('id', $parcel_view->shop_id)->first();
        return view('front.parcel.label', compact('parcel_view', 'parcel_shop'));
    }

    public function edit($id)
    {
        $parcel = Parcel::find($id);
        $cats = Category::all();
        return view('front.parcel.edit', compact('parcel', 'cats'));
    }
    public function update(Request $request, $id)
    {
        $parcel = Parcel::find($id);
        $parcel->customer_name = $request->c_name;
        $parcel->customer_address = $request->c_address;
        $parcel->customer_phone = $request->c_phone;
        $parcel->notes = $request->notes;
        $parcel->category = $request->p_cat;
        $parcel->sell_price = $request->sell_price;
        if ($parcel->save()) {
            toast('parcel Updated successfully', 'success');
            return redirect('parcels');

        }

    }

    public function delivery_charge($track_id, $cash_col, $cod, $total_pay, $delivery_crg)
    {
        $delivery = new Delivery_charge();
        $delivery->track_id = $track_id;
        $delivery->delivery_charge = $delivery_crg;
        $delivery->cash_collection = $cash_col;
        $delivery->cod_charge = $cod;
        $delivery->total_payable = $total_pay;
        if ($delivery->save()) {
            $this->parcel_status($delivery->track_id);

        }

    }

    public function parcel_status($track_id)
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $timenow = $dt->format('F j, Y, g:i a');
        $pickup_pending = ['status' => 1, 'time' => $timenow];
        $pickup_compeleted = ['status' => 0, 'time' => 0];
        $pickup_pending = json_encode($pickup_pending);
        $pickup_compeleted = json_encode($pickup_compeleted);

        $parcel_status = new Parcel_status();
        $parcel_status->track_id = $track_id;

        $parcel_status->pickup_pending = $pickup_pending;
        $parcel_status->pickup_compeleted = $pickup_compeleted;
        $parcel_status->send_shub = $pickup_compeleted;
        $parcel_status->received_shub = $pickup_compeleted;
        $parcel_status->send_dhub = $pickup_compeleted;
        $parcel_status->received_dhub = $pickup_compeleted;
        $parcel_status->delivery_onway = $pickup_compeleted;
        $parcel_status->delivery_done = $pickup_compeleted;
        $parcel_status->cash_collect = $pickup_compeleted;
        $parcel_status->delivery_delay = $pickup_compeleted;
        $parcel_status->delivery_again = $pickup_compeleted;
        $parcel_status->pickup_compeleted = $pickup_compeleted;
        $parcel_status->return = $pickup_compeleted;
        $parcel_status->return_del_onway = $pickup_compeleted;
        $parcel_status->return_received = $pickup_compeleted;
        $parcel_status->return_success = $pickup_compeleted;
        $parcel_status->pickup_compeleted = $pickup_compeleted;

        if ($parcel_status->save()) {
            toast('parcel creation success', 'success');
        }
    }

    public function track_parcel($id)
    {
        $parcel = Parcel::where('track_id', $id)->first();
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $delivery_details = Delivery_charge::where('track_id', $id)->first();
        return view('front.parcel.track', compact('parcel', 'parcel_status', 'delivery_details'));

    }
    public function track_search(Request $request)
    {
        return redirect()->route('user.track', $request->track);
    }

    public function weight_charge($id)
    {
        $weight = Weight::where('id', $id)->first();
        $weight = $weight->charge;
        return Response::json(['weight' => $weight], 200);
    }
    public function area_charge($id)
    {
        $area = Area::where('id', $id)->first();
        $area = $area->charge;
        return Response::json(['area' => $area], 200);
    }

    public function issue()
    {
        return view('front.parcel.issues');
    }
}
