<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Shop;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{
    public function home()
    {
        toast('welcome to Raft courier', 'success');
        return view('front.rafthome');
    }

    public function signupotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:users',

        ]);

        if ($validator->fails()) {

            return response()->json(array('msg' => 'taken'), 200);
        }

        $verification_code = rand(100000, 999999);
        $verification_code = strval($verification_code);
        $outputs = $this->sendmySMS($request->phone, '01969811565', 'Raftcourier OTP:' . $verification_code);
        //    dd($outputs);
        if ($outputs == 'Ok') {
            $user = new User();
            $user->phone = $request->phone;
            $user->user_type = "User";
            $user->verification = $verification_code;
            if ($user->save()) {
                return response()->json(array('msg' => 'ok'), 200);
            }

            // return redirect()->route('verification');
        } else {
            return response()->json(array('msg' => 'notok'), 200);
        }
    }
    public function checkotp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'otp' => 'required',
            'password' => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json(array('msg' => 'required'), 200);
        }

        $user = User::where('phone', $request->phone)->first();
        if ($user) {
            if ($user->verification == $request->otp) {
                $passwords = Hash::make($request->password);
                $user->password = $passwords;
                if ($user->save()) {
                    return response()->json(array('msg' => 'saved'), 200);
                } else {
                    return response()->json(array('msg' => 'unsaved'), 200);
                }
            } else {
                return response()->json(array('msg' => 'otpproblem'), 200);
            }
        }
    }
    public function raju()
    {
        return 'raju';
    }

    public function login()
    {
        return view('front.raftlogin');
    }
    public function changepass()
    {
        return view('front.user.changepass');
    }
    public function changepass_submit(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'curr_pass' => 'required',
            'new_pass' => 'min:6|required_with:re_pass|same:re_pass',

            're_pass' => 'min:6',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }

        if (Hash::check($request->curr_pass, Auth::user()->password)) {
            $merchant_user = User::find(Auth::user()->id);

            $merchant_user->password = Hash::make($request->new_pass);
            if ($merchant_user->save()) {
                toast('password changed successfully', 'success');
                return redirect()->route('user.home');
            } else {
                toast('password change failed', 'error');
                return redirect()->back()->withInput();

            }

        } else {
            toast('password Didn`t match', 'error');
            return redirect()->back()->withInput();
        }
    }

    public function notifications()
    {

        $notif = Notification::where('shop_id', get_active_shop()->id)->first();
        return view('front.user.notifications', compact('notif'));
    }

    public function notif_update(Request $request, $id)
    {
        $notif_updt = Notification::where('id', $id)->update(['number' => $request->number, 'email' => $request->email]);
        return redirect()->route('user.notifications');
    }
    public function coverage()
    {
        return view('front.user.coverage');
    }
    public function faq()
    {
        return view('front.user.faq');
    }
    public function policy()
    {
        return view('front.user.policy');
    }
    public function terms()
    {
        return view('front.user.terms');
    }
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required',

        ]);

        if ($validator->fails()) {

        }
        $user = User::where('phone', $request->phone)->get();
//    dd(count($user) );
        if (count($user) > 0 && count($user) === 1) {
            $user = User::where('phone', $request->phone)->first();
            if (Hash::check($request->password, $user->password)) {
                Auth::login($user, true);
                toast('login Successfull!', 'success');
                $priority = Shop::where('user_id', Auth::user()->id)->count();
                switch ($priority) {
                    case 0:
                        return redirect('/shopsetup');
                        break;
                    default:
                        return redirect('/merchants');

                }

            } else {

                toast('Password didn`t match!', 'error');
                return redirect()->back();
            }

        } else {
            toast('Phone didn`t match!', 'error');

            return redirect()->back();

        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('user.login');
    }

    public function sendmySMS($to, $from, $text)
    {
        $url = "http://api.greenweb.com.bd/api.php";
        $token = "95282bd58c334f5615b767ece874ec0f";
        $data = array(
            'to' => "$to",
            'message' => "$text",
            'token' => "$token",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $smsresult = curl_exec($ch);
        $result = mb_substr($smsresult, 0, 2);
        return $result;

    }
}
