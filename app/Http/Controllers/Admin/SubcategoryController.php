<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Session;
use Validator;

class SubcategoryController extends Controller
{

    public function index()
    {
        $cats = Subcategory::orderBy('subcategories.created_at', 'desc')
            ->leftJoin('categories', 'subcategories.cat_id', '=', 'categories.id')
            ->select('categories.name', 'subcategories.id', 'subcategories.sub_cat_name', )->paginate(10);
        return view('admin.subcategory.index', compact('cats'));
    }

    public function create()
    {
        $cats = Category::all();
        return view('admin.subcategory.create', compact('cats'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/subcats/index')
                ->withErrors($validator->errors())
                ->withInput();
        }
        $cat = new Subcategory();
        $cat->cat_id = $request->cat_id;
        $cat->sub_cat_name = $request->name;

        if ($cat->save()) {
            Session::flash('flash_message', 'Successfully Created ');
            Session::flash('flash_type', 'success');
            return Redirect()->route('subcats.index');
        } else {
            Session::flash('flash_message', 'Wrong  details. ');
            Session::flash('flash_type', 'error');
            return Redirect()->back()->withInputes();
        }

    }
    public function edit($id)
    {
        $cat = Subcategory::findOrFail($id);
        return view('admin.subcategory.edit', compact('cat'));
    }
    public function update(Request $request, $id)
    {
        $cat = Category::findOrFail($id);
        $cat->cat_id = $request->cat_id;
        $cat->sub_cat_name = $request->name;

        if ($cat->save()) {
            Session::flash('flash_message', 'Successfully Updated ');
            Session::flash('flash_type', 'success');
            return Redirect()->route('subcats.index');
        } else {
            Session::flash('flash_message', 'Wrong  details. ');
            Session::flash('flash_type', 'error');
            return Redirect()->back()->withInputes();
        }
        return Redirect()->route('subcats.index');
    }

    public function destroy($id)
    {
        if (Subcategory::destroy($id)) {

            Session::flash('flash_message', 'Successfully deleted ');
            Session::flash('flash_type', 'success');
            return redirect()->route('subcats.index');
        } else {
            Session::flash('flash_message', 'Wrong  details. ');
            Session::flash('flash_type', 'error');
            return back();
        }
    }
}
