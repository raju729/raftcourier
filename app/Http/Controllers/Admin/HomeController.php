<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Session;
use Validator;

class HomeController extends Controller
{
    public function login()
    {
        return view('admin.auth.login');
    }

    public function changepass()
    {
        return view('admin.auth.changepass');
    }
    public function checkpass(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'cur_password' => 'required',
            'new_password' => 'min:6|required_with:password_confirmation|same:password_confirmation',

            'password_confirmation' => 'min:6',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }

        if (Hash::check($request->cur_password, Auth::user()->password)) {
            $admin_user = User::find(Auth::user()->id);
            $admin_user->password = Hash::make($request->new_password);
            if ($admin_user->save()) {
                toast('password changed successfully', 'success');
                return redirect()->route('admin.dashboard');
            } else {
                toast('password change failed', 'error');
                return redirect()->route('admin.changepass');

            }

        } else {
            toast('password Didn`t match', 'error');
            return redirect()->route('admin.changepass');
        }

    }

    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'password' => 'required',
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return redirect('admin/login')
                ->withErrors($validator->errors())
                ->withInput();
        }
        $user_data = array(
            'email' => $request->email,
            'password' => $request->password,
        );
        $admin = User::where('email', $request->email)->first();

        if ($admin === null) {
            Session::flash('flash_message', 'login failed ');
            Session::flash('flash_type', 'error');
            return Redirect()->back();

        } else {

            if ($admin && $admin->user_type == 'Admin') {
                //  dd(Hash::make(123456));
                if (Hash::check($request->password, $admin->password)) {

                    // $this->guard()->login($user);
                    // auth()->login($agent);
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
                        Session::flash('flash_message', ' successfully logged in ');
                        Session::flash('flash_type', 'success');
                        return redirect()->route('admin.dashboard');
                    } else {
                        Session::flash('flash_message', 'login failed ');
                        Session::flash('flash_type', 'error');
                        return Redirect()->back();
                    }

                } else {
                    Session::flash('flash_message', 'Password did not match. ');
                    Session::flash('flash_type', 'error');
                    return Redirect()->back();
                }

            } else {
                Session::flash('flash_message', 'Wrong login details. ');
                Session::flash('flash_type', 'error');
                return Redirect()->back()->withInputes();
            }
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }
    public function dashboard()
    {
        return view('admin.dashboard');
    }
}
