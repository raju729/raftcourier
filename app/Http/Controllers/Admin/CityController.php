<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\city;
use App\Models\District;
use Illuminate\Http\Request;
use Validator;

class CityController extends Controller
{
    public function index()
    {$r = new City;
        $city = District::orderBy('districts.created_at', 'desc')
            ->rightJoin('cities', 'cities.District_id', '=', 'districts.id')
            ->select('districts.name as d_name', 'districts.id as d_id', 'cities.name as c_name', 'cities.id as c_id')->paginate(10);
        return view('admin.city.index', compact('city'));
    }

    public function create()
    {
        $dist = District::all();
        return view('admin.city.create', compact('dist'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }
        $city = new City();
        $city->district_id = $request->dist_id;
        $city->name = $request->name;
        if ($city->save()) {
            toast('Successfully Saved', 'success');
            return Redirect()->route('cities.index');
        } else {
            toast('Wrong Details', 'error');
            return Redirect()->back()->withInputes();
        }

    }

    public function edit($id)
    {
        $dist = District::all();
        $city = City::findOrFail($id);
        return view('admin.city.edit', compact('dist', 'city'));
    }
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->district_id = $request->dist_id;
        $city->name = $request->name;
        if ($city->save()) {
            toast('Successfully Updated', 'success');
            return Redirect()->route('cities.index');
        } else {
            toast('Wrong Details', 'error');
            return Redirect()->back()->withInputes();
        }

    }

    public function destroy($id)
    {
        if (city::destroy($id)) {
            toast('Successfully Deleted', 'success');
            return redirect()->route('cities.index');
        } else {
            toast('Wrong Details', 'error');
            return back();
        }
    }
}
