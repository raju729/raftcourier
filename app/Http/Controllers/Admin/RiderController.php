<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\City;
use App\Models\District;
use App\Models\Parcel;
use App\Models\Parcel_assign;
use App\Models\Rider;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rider = Rider::all();
        return view('admin.rider.index', compact('rider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area::all();
        $city = City::all();
        $dist = District::all();
        return view('admin.rider.create', compact('area', 'city', 'dist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $passwords = Hash::make(123456);
        $user->phone = $request->phone;
        $user->password = $passwords;
        $user->user_type = 'Rider';
        if ($user->save()) {
            $rider = new Rider();
            $rider->name = $request->name;
            $rider->user_id = $user->id;
            $rider->phone = $request->phone;
            $rider->address = $request->address;
            $rider->card_number = $request->card_number;
            $rider->district_id = $request->district_id;
            $rider->area_id = $request->area_id;
            $rider->city_id = $request->city_id;
            $rider->status = '1';
            if ($rider->save()) {
                $imageName = $rider->id . '.' . $request->image->extension();
                $request->image->move(public_path('riderImage'), $imageName);
                toast('Successfully Saved', 'success');
                return Redirect()->route('rider.index');
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show_need_assign($id, $area_id)
    {
        $rider_id = $id;

        $parcel = Parcel::where('Overall_status', 'Pending')->where('delivery_area', $area_id)
            ->where('assigned', '1')->get();

        return view('admin.rider.assign', compact('parcel', 'rider_id'));

    }

    public function parcel_assigned(Request $request)
    {
        $data = [];
        foreach ($request->all() as $key => $sku) {
            if ($key == '_token') {
            } else {
                array_push($data, $sku);
            }
        }
        foreach ($data as $key => $item) {
            $assign = new Parcel_assign();
            $assign->rider_id = $request->rider_id;
            $assign->parcel_id = $item;
            $assign->status = 'ongoing';

            if ($assign->save()) {
                $parcel = Parcel::where('id', $item)->update(['assigned' => '2']);

            }
        }

        return redirect()->route('rider.index');

    }
}
