<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Delivery_charge;
use App\Models\Payment;
use App\Models\Shop;
use App\Models\Transaction;
use App\Models\Voucher;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PaymentController extends Controller
{
    public function pending(Request $request)
    {
        $all_shop = Shop::all();

        $pending = DB::table('payments')
            ->leftJoin('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->leftJoin('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->where('payments.status', 'unpaid')
            ->where('parcels.shop_id', $request->shop_id)
            ->get();
        $selected_shop_id = $request->shop_id;
        return view('admin.payment.pending', compact('pending', 'all_shop', 'selected_shop_id'));

    }

    public function processing(Request $request)
    {
        $data = [];
        foreach ($request->all() as $key => $sku) {
            if ($key == '_token' || $key == 'selected_shop') {

            } else {
                array_push($data, $sku);
            }

        }
        $payment = Payment::whereIn('track_id', $data)->update(['status' => "processing"]);
        if ($payment) {
            $d_crg = Delivery_charge::whereIn('track_id', $data)->sum('delivery_charge');
            $total_pay = Delivery_charge::whereIn('track_id', $data)->sum('total_payable');
            $cash_collec = Delivery_charge::whereIn('track_id', $data)->sum('cash_collection');
            $cod_charges = Delivery_charge::whereIn('track_id', $data)->sum('cod_charge');
            // $return_charge = Delivery_charge::whereIn('track_id', $data)->sum('return_charge');
            $voucher = new Voucher();
            $voucher->user_id = Auth::user()->id;
            $voucher->shop_id = $request->selected_shop;
            $voucher->parcel_ids = json_encode($data);
            $voucher->total_payable = $total_pay;
            $voucher->return_charge = 0;
            $voucher->cash_collection = $cash_collec;
            $voucher->cod_charge = $cod_charges;
            $voucher->delivery_charge = $d_crg;
            $voucher->status = 'processing';
            if ($voucher->save()) {
                return redirect()->route('payment.pending');
            }

        }

    }
    public function processing_list(Request $request)
    {
        $all_shop = Shop::all();
        // dd($request->shop_id);
        $processing = Voucher::where('shop_id', $request->shop_id)->get();
        return view('admin.payment.processing', compact('processing', 'all_shop'));

    }
    public function paid_form($id)
    {
        $paid = Voucher::find($id);
        return view('admin.payment.paid_form', compact('paid'));

    }
    public function paid_submit(Request $request)
    {
        $paid = Voucher::find($request->voucher_id);
        $trans = new Transaction();
        $trans->user_id = Auth::user()->id;
        $trans->shop_id = $paid->shop_id;
        $trans->method = $request->method;
        $trans->voucher_id = $request->voucher_id;
        $trans->transaction = $request->transaction;
        $trans->sent_account = $request->sent_account;
        $trans->total_payable = $request->total_payable;
        if ($trans->save()) {
            $payment = Payment::whereIn('track_id', json_decode($paid->parcel_ids, true))->update(['status' => "paid"]);
            $voucher = Voucher::where('id', $request->voucher_id)->update(['status' => "paid"]);
            if ($payment && $voucher) {
                return redirect()->route('payment.processing.list');
            }

        }

    }

    public function voucher_details($id)
    {
        $voucher = Voucher::findOrFail($id);
        $parcel_ids = (json_decode($voucher->parcel_ids, true));
        $parcel_details = Delivery_charge::whereIn('track_id', $parcel_ids)->get();
        return view('admin.payment.voucher_details', compact('parcel_details'));

    }

    public function download_pdf($id)
    {
        $voucher = Voucher::findOrFail($id);
        $parcel_ids = (json_decode($voucher->parcel_ids, true));
        $parcel_details = Delivery_charge::whereIn('track_id', $parcel_ids)->get();
        $pdf = PDF::loadView('admin.payment.voucher_details', compact('parcel_details'));
        return $pdf->download('RC_invoice.pdf');

    }

}
