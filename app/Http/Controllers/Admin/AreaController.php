<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\city;
use Illuminate\Http\Request;
use Validator;

class AreaController extends Controller
{
    public function index()
    {
        $area = Area::orderBy('cities.created_at', 'desc')
            ->leftJoin('cities', 'cities.id', '=', 'areas.city_id')
            ->select('areas.name as ar_name', 'areas.id as ar_id', 'areas.charge', 'cities.name as c_name', 'cities.id as c_id')->paginate(10);
        return view('admin.area.index', compact('area'));
    }

    public function create()
    {
        $city = City::all();
        return view('admin.area.create', compact('city'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [ // <---
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }
        $area = new Area();
        $area->city_id = $request->city_id;
        $area->name = $request->name;
        $area->charge = $request->charge;
        $area->pick_up = $request->pickup == 'on' ? 1 : 0;
        $area->drop_off = $request->dropoff == 'on' ? 1 : 2;
        if ($area->save()) {
            toast('Successfully Saved', 'success');
            return Redirect()->route('areas.index');
        } else {
            toast('Wrong Details', 'error');
            return Redirect()->back()->withInputes();
        }

    }

    public function edit($id)
    {
        $city = City::all();
        $area = Area::findOrFail($id);
        return view('admin.area.edit', compact('area', 'city'));
    }
    public function update(Request $request, $id)
    {
        $area = Area::findOrFail($id);
        $area->city_id = $request->city_id;
        $area->name = $request->name;
        $area->charge = $request->charge;
        if ($area->save()) {
            toast('Successfully Updated', 'success');
            return Redirect()->route('areas.index');
        } else {
            toast('Wrong Details', 'error');
            return Redirect()->back()->withInputes();
        }

    }

    public function destroy($id)
    {
        if (Area::destroy($id)) {
            toast('Successfully Deleted', 'success');
            return redirect()->route('cities.index');
        } else {
            toast('Wrong Details', 'error');
            return back();
        }
    }
}
