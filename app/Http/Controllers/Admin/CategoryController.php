<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Session;
use Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $cats = Category::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.category.index', compact('cats'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/cats/index')
                ->withErrors($validator->errors())
                ->withInput();
        }
        $cat = new Category();
        $cat->name = $request->name;

        if ($cat->save()) {
            Session::flash('flash_message', 'Successfully Created ');
            Session::flash('flash_type', 'success');
            return Redirect()->route('cats.index');
        } else {
            Session::flash('flash_message', 'Wrong login details. ');
            Session::flash('flash_type', 'error');
            return Redirect()->back()->withInputes();
        }

    }
    public function edit($id)
    {
        $cat = Category::findOrFail($id);
        return view('admin.category.edit', compact('cat'));
    }
    public function update(Request $request, $id)
    {
        $cat = Category::findOrFail($id);
        $cat->name = $request->name;

        if ($cat->save()) {
            Session::flash('flash_message', 'Successfully Updated ');
            Session::flash('flash_type', 'success');
            return Redirect()->route('cats.index');
        } else {
            Session::flash('flash_message', 'Wrong  details. ');
            Session::flash('flash_type', 'error');
            return Redirect()->back()->withInputes();
        }
        return Redirect()->route('cats.index');
    }

    public function destroy($id)
    {
        if (Category::destroy($id)) {

            Session::flash('flash_message', 'Successfully deleted ');
            Session::flash('flash_type', 'success');
            return redirect()->route('cats.index');
        } else {
            Session::flash('flash_message', 'Wrong  details. ');
            Session::flash('flash_type', 'error');
            return back();
        }
    }
}
