<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\District;
use Illuminate\Http\Request;
use Validator;

class DistrictController extends Controller
{
    public function index()
    {
        $dist = District::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.district.index', compact('dist'));
    }

    public function create()
    {
        return view('admin.district.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ // <---
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }
        $dist = new District();
        $dist->name = $request->name;

        if ($dist->save()) {
            toast('Successfully Saved', 'success');
            return Redirect()->route('districts.index');
        } else {
            toast('Wrong Details', 'error');
            return Redirect()->back()->withInputes();
        }

    }
    public function edit($id)
    {
        $dist = District::findOrFail(base64_decode($id));
        return view('admin.district.edit', compact('dist'));
    }
    public function update(Request $request, $id)
    {
        $dist = District::findOrFail($id);
        $dist->name = $request->name;

        if ($dist->save()) {
            toast('Successfully Updated', 'success');
            return Redirect()->route('districts.index');
        } else {
            toast('wrong details', 'error');
            return Redirect()->back()->withInputes();
        }
        return Redirect()->route('districts.index');
    }

    public function destroy($id)
    {
        if (District::destroy(base64_decode($id))) {
            toast('Successfully Deleted', 'success');
            return redirect()->route('districts.index');
        } else {
            toast('Wrong Details', 'error');
            return back();
        }
    }
}
