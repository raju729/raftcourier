<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Homecontroller;
use App\Models\Area;
use App\Models\Parcel;
use App\Models\Parcel_assign;
use App\Models\Parcel_status;
use App\Models\Payment;
use App\Models\Rider;
use App\Models\Shop;
use Auth;
use DateTime;
use DateTimeZone;

// use App\Models\Delivery_charge;

class ParcellController extends Controller
{
    public function index()
    {
        $parcels = Parcel::orderBy('created_at', 'desc')->paginate(15);
        return view('admin.parcel.index', compact('parcels'));
    }

    public function show_status($id)
    {
        $area = Area::orderBy('created_at', 'desc')->get();
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        // dd($parcel_status);
        return view('admin.parcel.show_status', compact('parcel_status', 'area'));
    }

    public function change_status($id = null, $stage = null, $extra_value = null)
    {

        switch ($stage) {
            case 'pickup_compeleted':
                $this->change_status_helper('pickup_compeleted', 'send_shub', $id, $extra_value);
                return response()->json(['data' => 'ok'], 200);
                break;
            case 'send_shub':
                $this->change_status_hub_helper('send_shub', 'received_shub', $id, $extra_value);
                return response()->json(['data' => 'ok'], 200);
                break;
            case 'received_shub':
                $this->change_status_hub_helper('received_shub', 'send_dhub', $id, $extra_value);
                return response()->json(['data' => 'ok'], 200);
                break;
            case 'send_dhub':
                $this->change_status_hub_helper('send_dhub', 'received_dhub', $id, $extra_value);

                return response()->json(['data' => 'ok'], 200);
                break;
            case 'received_dhub':
                $this->change_status_hub_helper('received_dhub', 'delivery_onway', $id, $extra_value);
                $parcel_assign = Parcel::where('track_id', $id)->update(['assigned' => '1']);
                return response()->json(['data' => 'ok'], 200);
                break;
            case 'delivery_onway':
                $this->change_status_rider_helper('delivery_onway', $id, $extra_value);
                $this->parcel_assign($id, $extra_value);
                return response()->json(['data' => 'ok'], 200);
                break;
            case 'return_sendotp':
                $result = $this->send_return_otp('return', $id, $extra_value);
                if ($result) {
                    return response()->json(['data' => 'ok'], 200);
                } else {
                    return response()->json(['data' => 'notok'], 200);
                }
                break;
            case 'return_checkotp':
                $result = $this->check_return_otp('return', $id, $extra_value);

            // return response()->json(['data' => 'ok'], 200);
            // break;
            case 'delivery_done':
                $this->change_status_helper('delivery_done', 'cash_collect', $id, $extra_value);

                return response()->json(['data' => 'ok'], 200);
                break;

            case 'cash_collect':
                $this->change_final_status('cash_collect', $id, $extra_value);
                return response()->json(['data' => 'ok'], 200);
                break;

            default:

        }

    }
    public function check_return_otp($col1, $id, $extra_value)
    {
        $extra_value = explode(',', $extra_value);
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $return = json_decode($parcel_status->return, true);
        if ($extra_value[1] == $return['otp']) {

        }

    }

    public function send_return_otp($col1, $id)
    {
        $shop_id = Parcel::where('track_id', $id)->select('shop_id')->first();
        $merchant_phone = Shop::where('id', $shop_id->shop_id)->select('pickup_phone')->first();
        $verification_code = rand(100000, 999999);
        $verification_code = strval($verification_code);
        $home_contl = new HomeController();
        $outputs = $home_contl->sendmySMS($merchant_phone->pickup_phone, '01969811565', 'Raftcourier parcel return OTP:' . $verification_code);
        if ($outputs == "Ok") {
            $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $timenow = $dt->format('F j, Y, g:i a');
            $editor = Auth::user()->id;
            $col_first = ['status' => 2, 'time' => $timenow, 'otp' => $verification_code, 'editor' => $editor];
            $col_first = json_encode($col_first);
            $parcel_status = Parcel_status::where('track_id', $id)->first();
            $parcel_status->$col1 = $col_first;
            if ($parcel_status->save()) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function change_status_helper($col1, $col2, $id, $extra)
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $timenow = $dt->format('F j, Y, g:i a');
        $editor = Auth::user()->id;

        $col_first = ['status' => 1, 'time' => $timenow, 'editor' => $editor];
        $col_second = ['status' => 0, 'time' => '0', 'editor' => 'N/A'];
        $col_first = json_encode($col_first);
        $col_second = json_encode($col_second);
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $parcel_status->$col1 = $col_first;
        $parcel_status->$col2 = $col_second;

        if ($parcel_status->save()) {
            if ($col1 == 'pickup_compeleted') {
                $parcel = Parcel::where('track_id', $id)->first();
                $parcel->overall_status = 'Ontransit';
                $parcel->save();

            } elseif ($col1 == 'delivery_done') {
                $parcel = Parcel::where('track_id', $id)->first();
                $parcel->overall_status = 'Completed';
                $parcel->save();
                if (Auth::user()->user_type == 'Rider') {
                    $rider_src = Rider::where('user_id', Auth::user()->id)->first();
                    $new_wallet = $rider_src->wallet + 30.00;
                    Rider::where('id', $rider_src->id)->update(['wallet' => $new_wallet]);
                }
            } else {

            }
            return true;
        }

    }
    public function change_status_rider_helper($col1, $id, $extra)
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $timenow = $dt->format('F j, Y, g:i a');
        $editor = Auth::user()->id;
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $col_first = ['status' => 1, 'time' => $timenow, 'rider' => $extra, 'editor' => $editor];
        $col_first = json_encode($col_first);
        $parcel_status->$col1 = $col_first;
        if ($parcel_status->save()) {
            return true;
        }

    }

    public function change_final_status($col1, $id, $extra)
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $timenow = $dt->format('F j, Y, g:i a');
        $editor = Auth::user()->id;
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        $col_first = ['status' => 1, 'time' => $timenow, 'editor' => $editor];
        $col_first = json_encode($col_first);
        $parcel_status->$col1 = $col_first;
        if ($parcel_status->save()) {
            $payment = new Payment();
            $payment->user_id = $editor;
            $payment->track_id = $id;
            $payment->status = 'unpaid';
            if ($payment->save()) {
                return true;
            }

        }

    }

    public function change_status_hub_helper($col1, $col2, $id, $extra)
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $timenow = $dt->format('F j, Y, g:i a');
        $editor = Auth::user()->id;
        $parcel_status = Parcel_status::where('track_id', $id)->first();
        if ($col1 == 'send_shub') {
            $col_first = ['status' => 1, 'time' => $timenow, 'hub' => $extra, 'editor' => $editor];
            $col_second = ['status' => 0, 'time' => '0', 'hub' => $extra, 'editor' => 'N/A'];
        } elseif ($col1 == 'send_dhub') {
            $col_first = ['status' => 1, 'time' => $timenow, 'hub' => $extra, 'editor' => $editor];
            $col_second = ['status' => 0, 'time' => '0', 'hub' => $extra, 'editor' => 'N/A'];
        } elseif ($col1 == 'received_shub') {
            $col_first = json_decode($parcel_status->received_shub, true);
            $col_first['status'] = 1;
            $col_first['time'] = $timenow;
            $col_first['editor'] = $editor;
            $col_second = ['status' => 0, 'time' => '0', 'editor' => 'N/A'];
        } elseif ($col1 == 'received_dhub') {
            $col_first = json_decode($parcel_status->received_shub, true);
            $col_first['status'] = 1;
            $col_first['time'] = $timenow;
            $col_first['editor'] = $editor;
            $col_second = ['status' => 0, 'time' => '0', 'rider' => '', 'editor' => 'N/A'];
        }

        $col_first = json_encode($col_first);
        $col_second = json_encode($col_second);

        $parcel_status->$col1 = $col_first;
        $parcel_status->$col2 = $col_second;
        if ($parcel_status->save()) {
            return true;
        }

    }
    public function parcel_assign($parcel_id = null, $rider_id = null)
    {
        $parcel = Parcel::where('track_id', $parcel_id)->select('id')->first();

        $assign = new Parcel_assign();
        $assign->rider_id = $rider_id;
        $assign->parcel_id = $parcel->id;
        $assign->status = 'ongoing';

        if ($assign->save()) {
            $parcel_assign = Parcel::where('track_id', $parcel_id)->update(['assigned' => '2']);
            return true;

        } else {
            return false;
        }

    }

}
