<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Category;
use App\Models\City;
use App\Models\District;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\Subcategory;
use Auth;
use Illuminate\Http\Request;
use Validator;

class ShopController extends Controller
{

    public function index()
    {
        $shops = Shop::where('user_id', Auth::user()->id)->get();
        return view('front.shops.index', compact('shops'));

    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [ // <---
            's_name' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();

        }
        $priority = Shop::where('user_id', Auth::user()->id)->count();
        // dd($priority);
        switch ($priority) {
            case 0:
                $priority = 1;
                $status = '1';
                break;
            default:
                $priority = $priority + 1;
                $status = '0';

        }
        $shop = new Shop();
        $shop->shop_type = $request->s_type;
        $shop->user_id = Auth::user()->id;
        $shop->priority = $priority;
        $shop->status = $status;
        $shop->shop_name = $request->s_name;
        $shop->owner_name = $request->name;
        $shop->shop_email = $request->s_email;
        $shop->shop_address = $request->s_address;
        $shop->pickup_address = $request->p_address;
        $shop->pickup_area = $request->p_full_area;
        $shop->other_pickups = $request->p_full_area;
        $shop->pickup_phone = $request->p_phone;
        $shop->product_cat = $request->p_cat;
        $shop->product_subcat = $request->p_subcat;
        if ($shop->save()) {
            $notif = new Notification();
            $notif->shop_id = $shop->id;
            $notif->number = $request->p_phone;
            $notif->email = $request->s_email;
            if ($notif->save()) {
                toast('Shop creation success', 'success');
                return redirect('merchants');
            }

        } else {
            toast('Wrong details!', 'error');
            return back()->withInput();
        }

    }
    public function edit($id)
    {
        $dist = District::all();
        $cats = Category::all();
        $shop = Shop::findOrFail($id);
        return view('front.shops.edit', compact('shop', 'cats', 'dist'));

    }

    public function update(Request $request, $id)
    {
        $shop = Shop::findOrFail($id);
        if ($shop) {
            $shop->shop_name = $request->shop_name;
            $shop->shop_address = $request->shop_address;
            $shop->shop_email = $request->shop_email;
            if ($shop->save()) {
                toast('Shop Updated successfully', 'success');
                return redirect('shops');
            } else {
                toast('Wrong details!', 'error');
                return redirect('shops');
            }

        }

    }

    public function activate($id)
    {
        //pooor coding here
        $shop_status = Shop::where('user_id', Auth::user()->id)
            ->update(['status' => '0']);
        $shop_status2 = Shop::where('id', $id)
            ->update(['status' => '1']);
        toast('New shop activated', 'success');
        return redirect('merchants');

    }

    public function shopsetup()
    {
        $dist = District::all();
        $cats = Category::all();
        return view('front.shops.shop', compact('cats', 'dist'));
    }

    public function find_subcat($id)
    {
        $datas = Subcategory::where('cat_id', $id)->select('sub_cat_name as name', 'id')->get();
        return view('front.helpers.dropdown', compact('datas'));

    }
    public function find_subcity($id)
    {
        $datas = City::where('district_id', $id)->get();
        return view('front.helpers.dropdown', compact('datas'));

    }
    public function find_subarea($id)
    {
        $datas = Area::where('city_id', $id)->get();
        return view('front.helpers.dropdown', compact('datas'));

    }

}
