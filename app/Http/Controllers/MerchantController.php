<?php

namespace App\Http\Controllers;

use App\Models\Parcel;
// use App\Models\Parcel_status;
use App\Models\Shop;
use Auth;
use Illuminate\Support\Facades\DB;

class MerchantController extends Controller
{
    public function dashboard()
    {
        $active_shop = Shop::where('user_id', Auth::user()->id)->where('status', '1')->first();
        // dd($active_shop);

        $total_collection = DB::table('payments')
            ->leftJoin('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->leftJoin('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->where('parcels.shop_id', $active_shop->id)
            ->sum('delivery_charges.cash_collection');

        $unsettled = DB::table('payments')
            ->join('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->join('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->where('parcels.shop_id', $active_shop->id)
            ->where('payments.status', 'unpaid')
            ->sum('delivery_charges.cash_collection');

        $processing = DB::table('payments')
            ->join('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->join('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->where('parcels.shop_id', $active_shop->id)
            ->where('payments.status', 'processing')
            ->sum('delivery_charges.cash_collection');

        $paid = DB::table('payments')
            ->join('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->join('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->where('parcels.shop_id', $active_shop->id)
            ->where('payments.status', 'Paid')
            ->sum('delivery_charges.cash_collection');

        $delivery_charge = DB::table('payments')
            ->join('delivery_charges', 'payments.track_id', '=', 'delivery_charges.track_id')
            ->join('parcels', 'payments.track_id', '=', 'parcels.track_id')
            ->where('parcels.shop_id', $active_shop->id)
            ->sum('delivery_charges.delivery_charge');

        $allorders = Parcel::where('shop_id', $active_shop->id)->count();
        $pending = Parcel::where('shop_id', $active_shop->id)->where('overall_status', 'pending')->count();
        $ontransit = Parcel::where('shop_id', $active_shop->id)->where('overall_status', 'ontransit')->count();
        $completed = Parcel::where('shop_id', $active_shop->id)->where('overall_status', 'completed')->count();
        $returned = Parcel::where('shop_id', $active_shop->id)->where('overall_status', 'returned')->count();
        return view('front.merchant.dashboard',
            compact('processing', 'total_collection', 'unsettled', 'paid', 'delivery_charge', 'pending', 'ontransit', 'completed', 'allorders', 'returned'));
    }
    public function index(Type $var = null)
    {
        return view('front.merchant.dashboard');
    }
}
