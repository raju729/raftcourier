<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\IsUser as Middleware;
class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->user_type == 'User')) {
            return $next($request);
        }
        else{
            Session::flash('flash_message', 'please logged in as Admin ');
            Session::flash('flash_type', 'error'); 
            return redirect()->route('user.login');
        }
    }
}
